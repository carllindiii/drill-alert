package system.wells;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.well.Well;
import cactig.drillalert.backend.model.well.WellBore;
import cactig.drillalert.wells.WellsAdapter;

public class WellsAdapterTest extends TestCase {
   WellsAdapter wellsAdapter;

   public void setUp() throws Exception {
      super.setUp();

      List<Well> wells = new ArrayList<>();
      wells.add(new Well("11", "name", "location", null));
      wells.add(new Well("12", "name", "location", new ArrayList<WellBore>()));
      wells.add(new Well("13", "name", "location", null));
      wells.add(new Well("14", "name", "location", null));

      wellsAdapter = new WellsAdapter(null,wells);
   }

   public void testGetChildId()
   {
      assertTrue(wellsAdapter.getChildId(0, 0) > -1);
   }

   public void testGetChildrenCount()
   {
      assertTrue(wellsAdapter.getChildrenCount(1) > -1);

      assertEquals(0, wellsAdapter.getChildrenCount(-1));
      assertEquals(0, wellsAdapter.getChildrenCount(0));
   }

   public void testGetGroup()
   {
      assertNotNull(wellsAdapter.getGroup(0));
   }

   public void testHasStableIds()
   {
      assertFalse(wellsAdapter.hasStableIds());
   }

   public void testIsChildSelectable()
   {
      assertTrue(wellsAdapter.isChildSelectable(0, 0));
   }

}