package system.views.display;

import android.test.ActivityInstrumentationTestCase2;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.model.dashboard.ItemSettings;
import cactig.drillalert.backend.model.dashboard.ItemSettingsInternal;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.views.manage.DashboardAdapter;
import cactig.drillalert.views.manage.ManageDashboardsActivity;

/**
 * Created by cjohnson on 5/6/15.
 */
public class ManageDashboardsTest extends ActivityInstrumentationTestCase2<ManageDashboardsActivity>
{
   private ManageDashboardsActivity mActivity;

   @Override
   protected void setUp() throws Exception {
      // Starts the MainActivity of the target application
//      context = new ContextThemeWrapper(getInstrumentation().getTargetContext(), R.style.Theme_AppCompat);
      super.setUp();
   }


   public ManageDashboardsTest() {
      super(ManageDashboardsActivity.class);
   }

   public void testAdapter()
   {
      List<Dashboard> dashboards = new ArrayList<>();
      List<Page> pages = new ArrayList<>();
      List<Item> items = new ArrayList<>();

      Item newItem = new Item();
      newItem.setId(null);
      newItem.setPageId(0);
      newItem.setItemSettings(new ArrayList<ItemSettings>());
      newItem.setJsFile("ManagePanels");
      items.add(newItem);

      Page validPage = new Page();

      pages.add(validPage);
      pages.add(null);
      pages.add(new Page());

      Dashboard validDashboard = new Dashboard();
      validDashboard.setName("Test");
      validDashboard.setPages(pages);
      validDashboard.setWellboreId("00000000-0000-0000-0000-000000000001");

      dashboards.add(validDashboard);
      dashboards.add(null);
      dashboards.add(new Dashboard());

      DashboardAdapter dashboardAdapter = new DashboardAdapter(getActivity(), dashboards);

      assertTrue(dashboardAdapter.getAllItems().contains(validDashboard));
   }
}