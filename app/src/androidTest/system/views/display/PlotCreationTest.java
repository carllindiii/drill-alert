package system.views.display;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import cactig.drillalert.R;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.views.manage.creation.PlotCreationActivity;

/**
 * Created by cjohnson on 5/6/15.
 */
public class PlotCreationTest extends ActivityInstrumentationTestCase2<PlotCreationActivity> {
   private PlotCreationActivity mActivity;

   private String name;
   private int stepSize;
   private int startRange;
   private int endRange;

   private ArrayAdapter<Item> adapter;

   private int divisionSize;
   private boolean linear;

   private Button ivtSettingsButton;
   private Button trackSettingsButton;

   private Button addTrackButton;
   private Button removeTrackButton;

   @Override
   protected void setUp() throws Exception {
      super.setUp();
   }

   public PlotCreationTest() {
      super(PlotCreationActivity.class);
   }

   public void testConstruction() {
      mActivity = this.getActivity();

      loadLayoutData(mActivity);

      validate();
   }

   private void validate()
   {
      assertNotNull(name);
      assertNotNull(stepSize);
      assertNotNull(startRange);
      assertNotNull(endRange);

      assertNotNull(divisionSize);

      if(adapter.getCount() == 1)
      {
         assertFalse(removeTrackButton.isEnabled());
      }
   }

   private void loadLayoutData(Activity activity) {
       name = getText((EditText) activity.findViewById(R.id.plot_name));
       stepSize = getValue((EditText) activity.findViewById(R.id.ivt_step_size));
       startRange = getValue((EditText) activity.findViewById(R.id.ivt_start_range));
       endRange = getValue((EditText) activity.findViewById(R.id.ivt_end_range));;

       adapter = (ArrayAdapter<Item>)((Spinner) activity.findViewById(R.id.track_spinner)).getAdapter();

       divisionSize = getValue((EditText)activity.findViewById(R.id.item_settings_division_size));
       linear = ((RadioButton)activity.findViewById(R.id.linear_radio_button)).isChecked();

      ivtSettingsButton = (Button)activity.findViewById(R.id.ivt_settings_button);
      trackSettingsButton = (Button)activity.findViewById(R.id.item_settings_button);

      addTrackButton = (Button)activity.findViewById(R.id.add_item_settings_button);
      removeTrackButton = (Button)activity.findViewById(R.id.remove_item_settings_button);
   }

   private String getText(EditText editText)
   {
      return editText.getText().toString();
   }

   private int getValue(EditText editText)
   {
      String text = editText.getText().toString();

      return Integer.parseInt(text);
   }
}
