package system.views.display;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import cactig.drillalert.R;
import cactig.drillalert.views.manage.creation.CanvasCreationActivity;

/**
 * Created by cjohnson on 5/6/15.
 */
public class CanvasCreationTest extends ActivityInstrumentationTestCase2<CanvasCreationActivity> {
   private CanvasCreationActivity mActivity;

   private String name;
   private Button addGaugeButton;
   private Button addNumberReadoutButton;

   private ArrayAdapter curvesAdapter;

   @Override
   protected void setUp() throws Exception {
      super.setUp();
   }

   public CanvasCreationTest() {
      super(CanvasCreationActivity.class);
   }

   public void testConstruction() {
      mActivity = this.getActivity();

      loadLayoutData(mActivity);

      validate();
   }

   private void validate()
   {
      assertNotNull(name);
      assertFalse(name.isEmpty());

      assertTrue(addGaugeButton.isEnabled());
      assertTrue(addNumberReadoutButton.isEnabled());
   }

   private void loadLayoutData(Activity activity) {
      name = getText((EditText)activity.findViewById(R.id.canvas_name));
      addGaugeButton = (Button)activity.findViewById(R.id.add_gauge_button);
      addNumberReadoutButton = (Button)activity.findViewById(R.id.add_number_readout_button);

      curvesAdapter = (ArrayAdapter)((Spinner) activity.findViewById(R.id.canvas_spinner)).getAdapter();
   }

   private String getText(EditText editText)
   {
      return editText.getText().toString();
   }
}
