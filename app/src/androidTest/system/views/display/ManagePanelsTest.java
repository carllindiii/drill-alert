package system.views.display;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.R;
import cactig.drillalert.backend.model.dashboard.ItemSettings;
import cactig.drillalert.backend.model.dashboard.ItemSettingsInternal;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.views.manage.ManagePanelsActivity;
import cactig.drillalert.views.manage.PanelAdapter;

/**
 * Created by cjohnson on 5/6/15.
 */
public class ManagePanelsTest extends ActivityInstrumentationTestCase2<ManagePanelsActivity>
{
   private ManagePanelsActivity mActivity;

   private String name;
   private PanelAdapter panelAdapter;

   @Override
   protected void setUp() throws Exception {
      // Starts the MainActivity of the target application
//      context = new ContextThemeWrapper(getInstrumentation().getTargetContext(), R.style.Theme_AppCompat);
      super.setUp();
   }


   public ManagePanelsTest() {
      super(ManagePanelsActivity.class);
   }

   public void testConstruction()
   {
      mActivity = this.getActivity();

      loadLayoutData(mActivity);

      validate();
   }

   public void testAdapter()
   {
      List<Page> pages = new ArrayList<Page>();
      List<Item> items = new ArrayList<>();

      Item newItem = new Item();
      newItem.setId(null);
      newItem.setPageId(0);
      newItem.setItemSettings(new ArrayList<ItemSettings>());
      newItem.setJsFile("ManagePanels");
      items.add(newItem);

      Page validPage = new Page();

      pages.add(validPage);
      pages.add(null);
      pages.add(new Page());

      PanelAdapter panelAdapter = new PanelAdapter(getActivity(), pages);

      assertTrue(panelAdapter.getAllItems().contains(validPage));
   }

   private void loadLayoutData(ManagePanelsActivity activity)
   {
      name = getText((EditText)activity.findViewById(R.id.manage_panels_name));

      panelAdapter = activity.getPanelAdapter();
   }

   private void validate()
   {
      assertNotNull(name);
      assertFalse(name.isEmpty());
   }

   private String getText(EditText editText)
   {
      return editText.getText().toString();
   }

   private int getValue(EditText editText)
   {
      String text = editText.getText().toString();

      return Integer.parseInt(text);
   }
}