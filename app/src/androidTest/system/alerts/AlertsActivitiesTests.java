package system.alerts;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.R;
import cactig.drillalert.alerts.AddAlertActivity;
import cactig.drillalert.alerts.AlertsAdapter;
import cactig.drillalert.backend.model.alert.Alert;

public class AlertsActivitiesTests extends ActivityInstrumentationTestCase2<AddAlertActivity>
{
   private AddAlertActivity mActivity;

   private String name;
   private Integer threshold;
   private boolean parameterRises;
   private String priority;


   @Override
   protected void setUp() throws Exception {
      // Starts the MainActivity of the target application
//      context = new ContextThemeWrapper(getInstrumentation().getTargetContext(), R.style.Theme_AppCompat);
      super.setUp();
   }


   public AlertsActivitiesTests() {
      super(AddAlertActivity.class);
   }

   public void testConstruction()
   {
      mActivity = this.getActivity();

      loadLayoutData();

      assertFalse(name.isEmpty());
      assertEquals(new Integer(0), (Integer) threshold);
      assertTrue(parameterRises);
      assertFalse(priority.isEmpty());
   }

   public void testAdapter()
   {
      List<Alert> alerts = new ArrayList<>();
      Alert validAlert = new Alert("0", "id", true, 1, "Critical", 1000.3);
      Alert replacement = new Alert("1", "id", false, 2, "Warning", 2000.45);
      alerts.add(validAlert);
      alerts.add(new Alert(null, null, null, null, null, null));
      alerts.add(null);

      AlertsAdapter alertsAdapter = new AlertsAdapter(getActivity(), alerts);
      assertTrue(alertsAdapter.getAllAlerts().contains(validAlert));

      assertEquals(1, alertsAdapter.getAllAlerts().size());

      alertsAdapter.setAlert(validAlert, replacement);

      assertFalse(alertsAdapter.getAllAlerts().contains(validAlert));
      assertTrue(alertsAdapter.getAllAlerts().contains(replacement));
   }

   private void loadLayoutData()
   {
      name = ((EditText)mActivity.findViewById(R.id.add_alert_name)).getText().toString();
      threshold = Integer.parseInt(((EditText) mActivity.findViewById(R.id.threshold_field)).getText().toString());
      parameterRises = (((RadioButton) mActivity.findViewById(R.id.rising_radio_button)).isChecked());

      if (((RadioButton) mActivity.findViewById(R.id.critical_radio_button)).isChecked())
      {
         priority = "Critical";
      }
      else if (((RadioButton) mActivity.findViewById(R.id.warning_radio_button)).isChecked())
      {
         priority = "Warning";
      }
      if (((RadioButton) mActivity.findViewById(R.id.information_radio_button)).isChecked())
      {
         priority = "Information";
      }
   }
}