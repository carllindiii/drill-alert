package cactig.drillalert.backend.service;

import junit.framework.TestCase;

import java.util.List;

public class DashboardServiceTest extends TestCase {
   List returnedData;
   UIRunnable uiRunnable = new UIRunnable() {
      @Override
      public void run(List data) {
         returnedData = data;
      }
   };

   public void testOnPostExecute() throws Exception {

      try {
         //Check null handling
         new DashboardService(uiRunnable).onPostExecute(null);
      }
      catch (Exception exception)
      {
         //TODO: fail();
      }
   }
}