package cactig.drillalert.backend.model.alert;

import junit.framework.TestCase;

public class AlertHistoryTest extends TestCase {

   private String message = "hello world";
   private Integer notificationSeverity = 1; // 0 - information, 1 - warning, 2 - critical
   private String notificationDate = "01/01/1222";
   private Integer triggeringAlert = 1; // ID of the original alert - get from: /alert/{id}
   private Boolean acknowledged = true;
   private AlertHistory alertHistory;

   protected void setUp() throws Exception {
      alertHistory = new AlertHistory(message, notificationSeverity, notificationDate, triggeringAlert, acknowledged);

      alertHistory.setMessage(message);
      alertHistory.setNotificationSeverity(notificationSeverity);
      alertHistory.setNotificationDate(notificationDate);
      alertHistory.setTriggeringAlert(triggeringAlert);
      alertHistory.setAcknowledged(acknowledged);
   }

   public void testGetMessage() throws Exception {
      assertEquals(message, alertHistory.getMessage());
   }

   public void testGetNotificationSeverity() throws Exception {
      assertEquals(notificationSeverity, alertHistory.getNotificationSeverity());
   }

   public void testGetNotificationDate() throws Exception {
      assertEquals(notificationDate, alertHistory.getNotificationDate());
   }

   public void testGetTriggeringAlert() throws Exception {
      assertEquals(triggeringAlert, alertHistory.getTriggeringAlert());
   }

   public void testGetAcknowledged() throws Exception {
      assertEquals(acknowledged, alertHistory.getAcknowledged());
   }

   public void testToString() throws Exception {
      assertNotNull(alertHistory.toString());
   }
}