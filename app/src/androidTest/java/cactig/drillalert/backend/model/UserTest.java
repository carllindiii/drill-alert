package cactig.drillalert.backend.model;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;

import cactig.drillalert.backend.model.well.WellPermission;

public class UserTest extends TestCase {
   Integer id = 0;
   String guid = "00000000-0000-0000-0000-000000000000";
   String displayName = "Fred Flintstone";
   String email = "fred@sdi.com";
   List<WellPermission> wellPermissions = Arrays.asList(new WellPermission(1, 2, true));

   private User user;

   public void setUp() throws Exception {
      super.setUp();

      user = new User(id, guid, displayName, email, wellPermissions);
   }

   public void testGetID()
   {
      assertEquals(id, user.getId());
   }

   public void testGetGUID()
   {
      assertEquals(guid, user.getGuid());
   }

   public void testGetDisplayName()
   {
      assertEquals(displayName, user.getDisplayName());
   }

   public void testGetEmail()
   {
      assertEquals(email, user.getEmail());
   }

   public void testWellPermissions()
   {
      assertEquals(wellPermissions, user.getWellPermissions());
   }
}