package cactig.drillalert.backend.model.dashboard;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class PageTest extends TestCase {
   private Integer dashboardId = 234;
   private Integer id = 3;
   private Integer position = 11;
   private Integer xDim = 5;
   private Integer yDim = 6;
   private String name = "name";
   private Integer type = 1; /* TYPE: Track(0), Canvas(1), Rose(2) */
   private List<Item> items = new ArrayList<>();
   private Page page;

   protected void setUp()
   {
      page = new Page();

      page.setId(id);
      page.setName(name);
      page.setType(type);
      page.setType(type);
      page.setItems(new ArrayList<Item>());
      page.setXDim(xDim);
      page.setYDim(yDim);
      page.setPos(position);
      page.setDashboardId(dashboardId);
   }

   public void testConstructors()
   {
      //Ensure that no exceptions are thrown
      new Page(0, 1, 2, 3, "Testing", 4, new ArrayList<Item>());
      new Page(0, 1, 2, 3, 4, "Testing", 5, new ArrayList<Item>());
   }

   public void testGetId() throws Exception {
      assertEquals(id, page.getId());
   }

   public void testGetPos() throws Exception {
      assertEquals(position, page.getPos());
   }

   public void testGetXDim() throws Exception {
      assertEquals(xDim, page.getXDim());
   }

   public void testGetYDim() throws Exception {
      assertEquals(yDim, page.getYDim());
   }

   public void testGetName() throws Exception {
      assertEquals(name, page.getName());
   }

   public void testGetType() throws Exception {
      assertEquals(type, page.getType());
   }

   public void testGetItem() throws Exception {
      assertEquals(items, page.getItems());
   }

   public void testGetDashboardId()
   {
      assertEquals(dashboardId, page.getDashboardId());
   }

   public void testAddItem() throws Exception {
      Item item = new Item();

      page.addItem(item);
      assertTrue(page.getItems().contains(item));
   }

   public void testToString() throws Exception {
      //Verify no exceptions are thrown
      page.toString();

      page.setType(null);
      page.toString();

      page.setType(0);
      page.toString();

      page.setType(1);
      page.toString();

      page.setType(2);
      page.toString();

      page.setType(3);
      page.toString();
   }

   public void testEquals()
   {
      Page notEqualsPage = new Page(0, 1, 2, 3, "Testing", 4, new ArrayList<Item>());

      assertEquals(page, page);
      assertFalse(page.equals(null));
      assertFalse(page.equals(notEqualsPage));
   }
}