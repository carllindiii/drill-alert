package cactig.drillalert.backend.model.alert;

import junit.framework.TestCase;

public class AlertTest extends TestCase {
   private Alert alert;
   private Integer id = 1234;
   private String curveId = "0001";
   private String name = "uniqueName";
   private Boolean rising = true;
   private Integer wellBoreId = 9842;
   private String priority = "Critical";
   private Double threshold = 123.5;

   protected void setUp() throws Exception {
      alert = new Alert(curveId, name, rising, wellBoreId, priority, threshold);
   }

   public void testGetId() throws Exception {
      alert.setId(id);
      assertEquals(id, alert.getId());
   }

   public void testSetId() throws Exception {
      alert.setId(0);
      assertEquals(0, (int)alert.getId());
   }

   public void testGetCurveId() throws Exception {
      assertEquals(curveId, alert.getCurveId());
   }

   public void testSetCurveId() throws Exception {
      alert.setCurveId("1000");
      assertEquals("1000", alert.getCurveId());
   }

   public void testGetName() throws Exception {
      assertEquals(name, alert.getName());
   }

   public void testSetName() throws Exception {
      alert.setName("other");
      assertEquals("other", alert.getName());
   }

   public void testGetRising() throws Exception {
      assertEquals(rising, alert.getRising());
   }

   public void testSetRising() throws Exception {
      alert.setRising(!rising);
      assertEquals(!rising, (boolean)alert.getRising());
   }

   public void testGetWellBoreId() throws Exception {
      assertEquals(wellBoreId, alert.getWellBoreId());
   }

   public void testSetWellBoreId() throws Exception {
      alert.setWellBoreId(1540);
      assertEquals(1540, (int)alert.getWellBoreId());
   }

   public void testGetPriority() throws Exception {
      assertEquals(priority, alert.getPriority());
   }

   public void testSetPriority() throws Exception {
      alert.setPriority("Information");
      assertEquals("Information", alert.getPriority());
   }

   public void testGetThreshold() throws Exception {
      assertEquals(threshold, alert.getThreshold());
   }

   public void testSetThreshold() throws Exception {
      alert.setThreshold(10045d);
      assertEquals(10045d, alert.getThreshold());
   }

   public void testToString() throws Exception {
      assertNotNull(alert.toString());
   }
}