package cactig.drillalert.backend.model.dashboard;

import junit.framework.TestCase;

/**
 * Created by cjohnson on 5/31/15.
 */
public class ItemSettingsTest extends TestCase {
   private ItemSettings itemSettings;

   private Integer itemId = 12;
   private Integer id = 13;
   private Integer stepSize = 14;
   private Integer startRange = 15;
   private Integer endRange = 16;
   private Integer divisionSize = 17;
   private Integer scaleType = 18;

   public void setUp() throws Exception {
      super.setUp();

      itemSettings = new ItemSettings();
      itemSettings.setItemId(itemId);
      itemSettings.setId(id);
      itemSettings.setStepSize(stepSize);
      itemSettings.setStartRange(startRange);
      itemSettings.setEndRange(endRange);
      itemSettings.setDivisionSize(divisionSize);
      itemSettings.setScaleType(scaleType);
   }

   public void tearDown() throws Exception {

   }

   public void testGetItemId() throws Exception {
      assertEquals(itemId, itemSettings.getItemId());
   }

   public void testGetId() throws Exception {
      assertEquals(id, itemSettings.getId());
   }

   public void testGetStepSize() throws Exception {
      assertEquals(stepSize, itemSettings.getStepSize());
   }

   public void testGetStartRange() throws Exception {
      assertEquals(startRange, itemSettings.getStartRange());
   }

   public void testGetEndRange() throws Exception {
      assertEquals(endRange, itemSettings.getEndRange());
   }

   public void testGetDivisionSize() throws Exception {
      assertEquals(divisionSize, itemSettings.getDivisionSize());
   }

   public void testGetScaleType() throws Exception {
      assertEquals(scaleType, itemSettings.getScaleType());
   }

   public void testToString() throws Exception {
      assertNotNull(itemSettings.toString());
   }
}