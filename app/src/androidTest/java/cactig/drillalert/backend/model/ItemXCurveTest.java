package cactig.drillalert.backend.model;

import junit.framework.TestCase;

/**
 * Created by cjohnson on 5/31/15.
 */
public class ItemXCurveTest extends TestCase {
   private ItemXCurve itemXCurve;
   private Integer id;
   private Integer itemId;
   private String curveId;

   public void setUp() throws Exception {
      super.setUp();

      itemXCurve = new ItemXCurve(id, itemId, curveId);
   }

   public void tearDown() throws Exception {

   }

   public void testGetId() throws Exception {
      itemXCurve.setId(id);
      assertEquals(itemId, itemXCurve.getId());
   }

   public void testGetItemId() throws Exception {
      itemXCurve.setItemId(itemId);
      assertEquals(itemId, itemXCurve.getItemId());
   }

   public void testGetCurveId() throws Exception {
      itemXCurve.setCurveId(curveId);
      assertEquals(curveId, itemXCurve.getCurveId());
   }

   public void testEquals() throws Exception {
      assertEquals(itemXCurve, itemXCurve);
   }

   public void testHashCode() throws Exception {
      assertNotNull(itemXCurve.hashCode());
   }

   public void testToString() throws Exception {
      assertNotNull(itemXCurve.toString());
   }
}