package cactig.drillalert.backend.model.dashboard;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class DashboardTest extends TestCase {
   private Integer id = 123;
   private String name = "unique name";
   private List<Page> pages = new ArrayList<>();
   private String wellboreId = "5a19ef58-f1bd-47cd-8736-9b7e67d62b2b";
   private Integer userId = 556;
   private Dashboard dashboard;

   protected void setUp() throws Exception {
      dashboard = new Dashboard();

      dashboard.setId(id);
      dashboard.setUserId(userId);
      dashboard.setName(name);
      dashboard.setPages(pages);
      dashboard.setWellboreId(wellboreId);
   }

   public void testGetId() throws Exception {
      assertEquals(id, dashboard.getId());
   }

   public void testGetName() throws Exception {
      assertEquals(name, dashboard.getName());
   }

   public void testGetPages() throws Exception {
      assertEquals(pages, dashboard.getPages());
   }

   public void testGetWellboreId() throws Exception {
      assertEquals(wellboreId, dashboard.getWellboreId());
   }

   public void testGetUserId() throws Exception {
      assertEquals(userId, dashboard.getUserId());
   }

   public void testToString() throws Exception {
      assertNotNull(dashboard.toString());
   }
}