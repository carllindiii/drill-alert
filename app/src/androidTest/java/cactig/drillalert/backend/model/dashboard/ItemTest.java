package cactig.drillalert.backend.model.dashboard;

import junit.framework.TestCase;

import java.util.ArrayList;

public class ItemTest extends TestCase {
   private String id = "123";
   private Integer xPos = 0;
   private Integer yPos = 1;
   private String jsFile = "not a real file";
   private Integer pageId = 0;
   private Item item;

   public void setUp() throws Exception {
      super.setUp();

      item = new Item();

      item.setId(pageId);
      item.setXPos(xPos);
      item.setYPos(yPos);
      item.setJsFile(jsFile);
      item.setPageId(pageId);
//      item.setItemSettings(new ArrayList<ItemSettingsInternal>());
   }

   public void testGetId() throws Exception {

   }

   public void testSetId() throws Exception {

   }

   public void testGetXPos() throws Exception {

   }

   public void testSetXPos() throws Exception {

   }

   public void testGetYPos() throws Exception {

   }

   public void testSetYPos() throws Exception {

   }

   public void testGetJsFile() throws Exception {

   }

   public void testSetJsFile() throws Exception {

   }

   public void testGetCurveId() throws Exception {

   }

   public void testSetCurveId() throws Exception {

   }

   public void testToString() throws Exception {
      assertNotNull(item.toString());
   }
}