package cactig.drillalert.backend.parser;

import junit.framework.TestCase;

import java.util.List;

public class DashboardsParserTest extends TestCase {

   Parser viewsParser;

   public void setUp() throws Exception {
      super.setUp();

      viewsParser = DashboardsParser.getInstance();
   }

   public void tearDown() throws Exception {

   }

   public void testGetInstance() throws Exception {
      Parser viewsParser = DashboardsParser.getInstance();
      Parser viewsParser2 = DashboardsParser.getInstance();
      assertFalse(null == viewsParser);
      assertEquals(viewsParser, viewsParser2);
      assertTrue(viewsParser == viewsParser2);
   }

   public void testParse() throws Exception {

      try {
         assertTrue(viewsParser.parse("") instanceof List);
      }
      catch(Exception exception)
      {
         //TODO: fail
      }

      //Test null handling
      viewsParser.parse(null);
   }
}