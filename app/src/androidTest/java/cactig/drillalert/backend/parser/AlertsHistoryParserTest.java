package cactig.drillalert.backend.parser;

import junit.framework.TestCase;

public class AlertsHistoryParserTest extends TestCase {

   public void testGetInstance() throws Exception {
      assertEquals(AlertsHistoryParser.getInstance(), AlertsHistoryParser.getInstance());
   }

   public void testParse() throws Exception {
      AlertsHistoryParser alertsHistoryParser = AlertsHistoryParser.getInstance();

//      assertNotNull(alertsHistoryParser.parse(null));
//      assertNotNull(alertsHistoryParser.parse(""));
      String exampleJSON = "[\n" +
              "  {\n" +
              "    \"Message\": \"sample string 1\",\n" +
              "    \"NotificationSeverity\": 0,\n" +
              "    \"NotificationDate\": \"2015-03-10T21:28:50.0693873+00:00\",\n" +
              "    \"TriggeringAlert\": 3,\n" +
              "    \"Acknowledged\": true\n" +
              "  },\n" +
              "  {\n" +
              "    \"Message\": \"sample string 1\",\n" +
              "    \"NotificationSeverity\": 0,\n" +
              "    \"NotificationDate\": \"2015-03-10T21:28:50.0693873+00:00\",\n" +
              "    \"TriggeringAlert\": 3,\n" +
              "    \"Acknowledged\": true\n" +
              "  }\n" +
              "]";

//      assertNotNull(alertsHistoryParser.parse(exampleJSON));
//      assertTrue(alertsHistoryParser.parse(exampleJSON) instanceof List);
//      assertTrue(alertsHistoryParser.parse(exampleJSON).size() > 0);
   }
}