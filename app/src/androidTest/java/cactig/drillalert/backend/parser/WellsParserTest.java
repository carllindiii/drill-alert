package cactig.drillalert.backend.parser;

import junit.framework.TestCase;

import java.util.ArrayList;

import cactig.drillalert.backend.model.well.Well;


public class WellsParserTest extends TestCase {

   Parser wellsParser;

   public void setUp() throws Exception {
      super.setUp();

      wellsParser = WellsParser.getInstance();
   }

   public void tearDown() throws Exception {

   }

   public void testGetInstance() throws Exception {
      Parser wellsParser = WellsParser.getInstance();
      Parser wellsParser2 = WellsParser.getInstance();
      assertFalse(null == wellsParser);
      assertEquals(wellsParser, wellsParser2);
      assertTrue(wellsParser == wellsParser2);
   }

   public void testParse() throws Exception {

      //Test null handling
      assertEquals(new ArrayList<Well>(), wellsParser.parse(null));

//      //empty json
//      String emptyWellsJson = "[]";
//      assertEquals(new ArrayList<Well>(), wellsParser.parse(emptyWellsJson));

//      // single well
//      String singleWellJson = "{\"id\":3,\"name\":\"Harrison Hole\",\"location\":\"Harrison, CA\",\"wellBores\":[]}";
//      ArrayList<Well> singleWellList = new ArrayList<Well>();
//      singleWellList.add(new Well(3, "Harrison Hole", "Harrison, CA", new ArrayList<WellBore>()));
//      assertEquals(singleWellJson, wellsParser.parse(singleWellJson));
   }
}
