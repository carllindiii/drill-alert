package cactig.drillalert.backend.parser;

import junit.framework.TestCase;

public class AlertsParserTest extends TestCase {
   public void testGetInstance() throws Exception {
      assertEquals(AlertsParser.getInstance(), AlertsParser.getInstance());
   }

   //TODO: enable assertions
   public void testParse() throws Exception {
      AlertsParser alertsParser = AlertsParser.getInstance();

      assertNotNull(alertsParser.parse(null));
      String exampleJSON = "[\n" +
              "  {\n" +
              "    \"Message\": \"sample string 1\",\n" +
              "    \"NotificationSeverity\": 0,\n" +
              "    \"NotificationDate\": \"2015-03-10T21:28:50.0693873+00:00\",\n" +
              "    \"TriggeringAlert\": 3,\n" +
              "    \"Acknowledged\": true\n" +
              "  },\n" +
              "  {\n" +
              "    \"Message\": \"sample string 1\",\n" +
              "    \"NotificationSeverity\": 0,\n" +
              "    \"NotificationDate\": \"2015-03-10T21:28:50.0693873+00:00\",\n" +
              "    \"TriggeringAlert\": 3,\n" +
              "    \"Acknowledged\": true\n" +
              "  }\n" +
              "]";

      try {
         assertNotNull(alertsParser.parse(exampleJSON));
      }
      catch (Exception exception)
      {
         //TODO: Fail
      }
//      assertTrue(alertsParser.parse(exampleJSON) instanceof List);
//      assertTrue(alertsParser.parse(exampleJSON).size() > 0);
   }
}