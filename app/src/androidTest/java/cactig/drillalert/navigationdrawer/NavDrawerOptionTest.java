package cactig.drillalert.navigationdrawer;

import junit.framework.TestCase;


public class NavDrawerOptionTest extends TestCase {

   public void testNavDrawerOption()
   {
      assertTrue(NavDrawerOption.values().length > 0);

      for(NavDrawerOption navDrawerOption : NavDrawerOption.values()) {
         assertNotNull(NavDrawerOption.valueOf(navDrawerOption.toString()));
      }
   }

   public void testGetNames()
   {
      assertNotNull(NavDrawerOption.getNames());
   }
}