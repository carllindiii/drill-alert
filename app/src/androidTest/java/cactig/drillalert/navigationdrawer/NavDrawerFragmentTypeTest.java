package cactig.drillalert.navigationdrawer;

import junit.framework.TestCase;


public class NavDrawerFragmentTypeTest extends TestCase {
   public void testNavDrawerFragmentType()
   {
      assertTrue(NavDrawerFragmentType.values().length > 0);

      for(NavDrawerFragmentType navDrawerFragmentType : NavDrawerFragmentType.values()) {
         assertNotNull(NavDrawerFragmentType.valueOf(navDrawerFragmentType.toString()));
      }
   }
}