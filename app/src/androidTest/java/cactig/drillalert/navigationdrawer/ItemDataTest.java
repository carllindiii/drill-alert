package cactig.drillalert.navigationdrawer;

import junit.framework.TestCase;

public class ItemDataTest extends TestCase {
   private String title = "Title";
   private int url = 4;
   private ItemData itemData = new ItemData(title, 4);

   public void testGetTitle() throws Exception {
      assertEquals(title, itemData.getTitle());
   }

   public void testSetTitle() throws Exception {
      title = "new";
      itemData.setTitle(title);
      assertEquals(title, itemData.getTitle());
   }

   public void testGetImageUrl() throws Exception {
      assertEquals(url, itemData.getImageUrl());
   }

   public void testSetImageUrl() throws Exception {
      url = 56;
      itemData.setImageUrl(url);
      assertEquals(url, itemData.getImageUrl());
   }
}