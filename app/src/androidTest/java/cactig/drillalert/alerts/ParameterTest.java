package cactig.drillalert.alerts;

import junit.framework.TestCase;

public class ParameterTest extends TestCase {
   public void testParameter()
   {
      assertTrue(Parameter.values().length > 0);

      for(Parameter parameter : Parameter.values()) {
         assertNotNull(Parameter.valueOf(parameter.toString()));
      }
   }
}