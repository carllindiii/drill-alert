package cactig.drillalert.alerts;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class AlertHeaderTest extends TestCase {
   public void testNotificationHeader()
   {
      assertTrue(AlertHeader.values().length > 0);
      List<String> notificationIds = new ArrayList<>();
      notificationIds.add("ARCHIVED");
      notificationIds.add("INFORMATIONAL");
      notificationIds.add("WARNING");
      notificationIds.add("CRITICAL");

      for(String id : notificationIds)
      {
         assertNotNull(AlertHeader.valueOf(id));
      }
   }

   public void testGetColor()
   {
      assertNotNull(AlertHeader.ARCHIVED.getColor());
   }

   public void testToString()
   {
      assertNotNull(AlertHeader.values()[0].toString());
   }
}