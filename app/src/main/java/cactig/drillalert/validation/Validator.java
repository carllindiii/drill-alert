package cactig.drillalert.validation;

import android.widget.Toast;

import resources.NameGenerator;

/**
 * Created by cjohnson on 4/20/15.
 */
public class Validator {
   private static NameGenerator nameGenerator = NameGenerator.getInstance();

   public static boolean isValidName(String name)
   {
      if (name == null || name.isEmpty() || nameGenerator.isNameInUse(name)) {
         return false;
      }

      return true;
   }

   public static boolean isValidDouble(String text)
   {
      if (text == null || text.isEmpty())
      {
         return false;
      }
      else
      {
         try{
            Double.parseDouble(text);
            return true;
         }
         catch (Exception exception)
         {
            return false;
         }
      }
   }
}
