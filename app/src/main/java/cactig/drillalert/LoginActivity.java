package cactig.drillalert;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import cactig.drillalert.backend.LoginWebViewClient;
import cactig.drillalert.backend.model.User;
import cactig.drillalert.backend.service.PermissionsService;
import cactig.drillalert.navigationdrawer.NavigationDrawerActivity;

/**
 * A login screen that displays the SDI login web view
 */
public class LoginActivity extends Activity {
   final static String TAG = "LoginActivity";
   final static String loginURL = "https://drillalert.azurewebsites.net/";
   LoginWebViewClient loginWebViewClient = LoginWebViewClient.getInstance(this);
   WebView loginWebView;

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.login_webview);

      loginWebView = (WebView) findViewById(R.id.login_webview);
      loginWebView.setWebViewClient(loginWebViewClient);
      loginWebView.loadUrl(loginURL);
      loginWebView.getSettings().setJavaScriptEnabled(true);
   }


   public void login() {
      // once on login, execute the permissions endpoint so the backend has our registration ID
      new PermissionsService().getPermissions(getApplicationContext());

      startActivity(new Intent(this, NavigationDrawerActivity.class));
      finish();
   }
}