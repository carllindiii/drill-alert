package cactig.drillalert.views.manage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.R;
import cactig.drillalert.views.manage.creation.CanvasCreationActivity;
import cactig.drillalert.views.manage.creation.CompassCreationActivity;
import cactig.drillalert.views.manage.creation.PlotCreationActivity;
import resources.adapter.utility.IntentKeyManager;

public class CreatePageActivity extends ActionBarActivity {
   private static final String TITLE_KEY = "AddVisualsActivityTitle";
   private static final String TAG = "CreatePanelActivity";

   private static final int SELECTED_VIEWS_REQUEST = 0;
   private static final int PLOT_REQUEST = 1;
   private static final int COMPASS_REQUEST = 2;
   private static final int CANVAS_REQUEST = 3;

   private String visualKey = IntentKeyManager.getSelectedVisualKey();
   private String wellboreIdKey = IntentKeyManager.getWellboreIdKey();
   private String dashboardsKey = IntentKeyManager.getDashboardsKey();
   private String panelKey = IntentKeyManager.getPanelKey();

   private String wellboreId;

   private List<Dashboard> selectedDashboards;

   private Page page;

   private int CANVAS = 0;
   private int PLOT = 1;
   private int COMPASS = 2;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      String title = getIntent().getStringExtra(TITLE_KEY);
      setContentView(R.layout.activity_add_visuals);

      Intent intent = getIntent();

      if(intent != null)
      {
         wellboreId = intent.getStringExtra(wellboreIdKey);

         if(wellboreId == null || wellboreId.isEmpty())
         {
            Log.d(TAG, "Unable to retrieve wellboreId");
         }
      }

      if(title != null)
      {
         setTitle(title);
      }
      else
      {
         setTitle("Add Item");
      }

      Button createPlotButton = (Button) findViewById(R.id.create_plot_button);
      createPlotButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), PlotCreationActivity.class);
            intent.putExtra(wellboreIdKey, wellboreId);
            startActivityForResult(intent, PLOT_REQUEST);
         }
      });

      Button createCompassButton = (Button) findViewById(R.id.create_compass_button);
      createCompassButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), CompassCreationActivity.class);
            intent.putExtra(wellboreIdKey, wellboreId);
            startActivityForResult(intent, COMPASS_REQUEST);
         }
      });

      Button createCanvasButton = (Button) findViewById(R.id.create_canvas_button);
      createCanvasButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), CanvasCreationActivity.class);
            intent.putExtra(wellboreIdKey, wellboreId);
            startActivityForResult(intent, CANVAS_REQUEST);
         }
      });
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
         case android.R.id.home:
            setResult(Activity.RESULT_CANCELED);
            finish();
      }

      return true;
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      Intent intent = new Intent();

      switch (requestCode) {
         case (PLOT_REQUEST): {
            if (resultCode == Activity.RESULT_OK) {
               page = (Page) data.getSerializableExtra(panelKey);
            }
            break;
         }
         case (COMPASS_REQUEST): {
            if (resultCode == Activity.RESULT_OK) {
               page = (Page) data.getSerializableExtra(panelKey);
            }
            break;
         }
         case (CANVAS_REQUEST): {
            if (resultCode == Activity.RESULT_OK) {
               page = (Page) data.getSerializableExtra(panelKey);
            }
            break;
         }
      }

      intent.putExtra(panelKey, page);
      setResult(Activity.RESULT_OK, intent);
      finish();
   }
}
