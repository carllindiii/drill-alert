package cactig.drillalert.views.manage;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.R;
import cactig.drillalert.backend.model.dashboard.Page;
import resources.CheckboxListItem;
import resources.adapter.CheckboxArrayAdapter;

/**
 * Created by cjohnson on 4/13/15.
 */
public class PanelAdapter extends CheckboxArrayAdapter<Page> {
   private static final String TAG = "ViewsAdapter";

   // child data in format of header title, child title


   public PanelAdapter(Context context, List<Page> pages) {
      super(context, R.layout.sortable_list_row, getCheckboxList(pages));
   }

   @Override
   public boolean hasStableIds() {
      return false;
   }

   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
      View row = convertView;
      StringHolder holder = null;

      if (row == null) {
         LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
         row = inflater.inflate(getLayoutResourceId(), parent, false);

         holder = new StringHolder();
         holder.txtTitle = (TextView) row.findViewById(R.id.sortable_row_text);

         row.setTag(holder);
      } else {
         holder = (StringHolder) row.getTag();
      }

      List<CheckboxListItem<Page>> data = getAllData();
      if(data != null && position < data.size()) {
         CheckboxListItem<Page> panelCheckboxListItem = data.get(position);

         if(panelCheckboxListItem != null && panelCheckboxListItem.getItem() != null) {
            holder.txtTitle.setText(panelCheckboxListItem.getItem().toString());
         }
      }

      if (isSelected(position)) {
         TypedValue typedValue = new TypedValue();
         getContext().getTheme().resolveAttribute(android.R.attr.activatedBackgroundIndicator, typedValue, true);
         row.setBackgroundResource(typedValue.resourceId);
      }

      return row;
   }

   private static List<CheckboxListItem<Page>> getCheckboxList(List<Page> pages)
   {
      ArrayList<CheckboxListItem<Page>> checkboxListItemList = new ArrayList<>();

      if(pages != null) {
         for (Page page : pages) {
            checkboxListItemList.add(new CheckboxListItem<Page>(page, false, true));
         }
      }

      return checkboxListItemList;
   }

   private static class StringHolder
   {
      TextView txtTitle;
   }

}
