package cactig.drillalert.views.manage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.R;
import cactig.drillalert.backend.model.well.ItemTypeEnum;
import cactig.drillalert.backend.service.DashboardService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.util.SystemUiHider;
import cactig.drillalert.views.manage.creation.CanvasCreationActivity;
import cactig.drillalert.views.manage.creation.CompassCreationActivity;
import cactig.drillalert.views.manage.creation.PlotCreationActivity;
import resources.CheckboxListItem;
import resources.NameGenerator;
import resources.adapter.CheckboxArrayAdapter;
import resources.adapter.utility.CheckboxPopupUtility;
import resources.adapter.utility.IntentKeyManager;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class ManagePanelsActivity extends ActionBarActivity implements UIRunnable<Dashboard>, AdapterView.OnItemClickListener {
   private CheckboxArrayAdapter dataAdapter;

   private String wellboreIdKey = IntentKeyManager.getWellboreIdKey();
   private String visualKey = IntentKeyManager.getSelectedVisualKey();
   private String dashboardKey = IntentKeyManager.getDashboardsKey();
   private String createFromExistingKey = IntentKeyManager.getUseAsTemplateKey();
   private String panelKey = IntentKeyManager.getPanelKey();

   private Item selectedItem = null;

   private static final String TAG = "ManagePanelsActivity";
   private static final int VISUAL_REQUEST = 1;

   private Page selectedPage = null;
   private Dashboard dashboard;

   private ListView listView;

   private String wellboreId;

   private EditText nameEditText;

   private PanelAdapter panelAdapter;

   private NameGenerator nameGenerator = NameGenerator.getInstance();

   public PanelAdapter getPanelAdapter() {
      return panelAdapter;
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      setContentView(R.layout.activity_manage_panels);
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      getSupportActionBar().setHomeButtonEnabled(false);

      dashboard = (Dashboard)getIntent().getSerializableExtra(dashboardKey);
      wellboreId = getIntent().getStringExtra(wellboreIdKey);

      listView = (ListView)findViewById(R.id.panels_list_view);

      if(dashboard == null)
      {
         dashboard = new Dashboard();
         dashboard.setName(nameGenerator.generateName("Dashboard"));
         dashboard.setPages(new ArrayList<Page>());
         dashboard.setWellboreId(wellboreId);
      }
      updateAdapter(dashboard.getPages());
      setTitle(dashboard.getName());

      nameEditText = (EditText)findViewById(R.id.manage_panels_name);
      nameEditText.setText(dashboard.getName());

      CheckboxPopupUtility.prepareRemoveListView(listView, panelAdapter);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_manage_panels, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle_light_blue clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      switch (id) {
         case R.id.save:
            dashboard.setName(nameEditText.getText().toString());
            dashboard.setPages(getPanels());

            // Create new dashboard if id doesn't exist yet
            if(dashboard.getId() == null) {
               new DashboardService(this).createNewDashboard(dashboard);
            }
            // Update dashboard otherwise
            else {
               new DashboardService(this).updateDashboard(dashboard);
            }

            Intent intent = new Intent();
            intent.putExtra(dashboardKey, dashboard);
            setResult(Activity.RESULT_OK, intent);
            finish();
            break;
         case R.id.create_new_panel:
            selectedPage = null;
            Intent addVisualIntent = new Intent(this, CreatePageActivity.class);
            addVisualIntent.putExtra(wellboreIdKey, wellboreId);
            startActivityForResult(addVisualIntent, VISUAL_REQUEST);
            break;
         case R.id.use_template:
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
            builderSingle.setTitle("Select One Name:-");
            final ArrayAdapter<Page> arrayAdapter = getTemplateAdapter();

            builderSingle.setNegativeButton("cancel",
                    new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                       }
                    });

            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          addPanel(arrayAdapter.getItem(which));
                       }
                    });
            builderSingle.show();
            break;
         default:
            break;
      }

      return true;
   }

   @Override
   public void run(List<Dashboard> dashboards) {
      //TODO: Do anything here???
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      switch(requestCode) {
         case (VISUAL_REQUEST) : {
            if (resultCode == Activity.RESULT_OK) {
               Object object = data.getSerializableExtra(panelKey);

               if(object != null && object instanceof Page) {
                  Page newPage = (Page)object;
                  addPanel(newPage);

                  NameGenerator.getInstance().addName(newPage.getName());
               }
            }
            else
            {
              addPanel(selectedPage);
            }

            break;
         }
      }
   }

   @Override
   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      if(position < dashboard.getPages().size())
      {
         Intent intent = null;

         selectedPage = dashboard.getPages().remove(position);

         if(selectedPage.getType().equals(ItemTypeEnum.Canvas.getValue()))
         {
            intent = new Intent(getApplicationContext(), CanvasCreationActivity.class);
         }
         else if(selectedPage.getType().equals(ItemTypeEnum.Plot.getValue()))
         {
            intent = new Intent(getApplicationContext(), PlotCreationActivity.class);
         }
         else if(selectedPage.getType().equals(ItemTypeEnum.Compass.getValue()))
         {
            intent = new Intent(getApplicationContext(), CompassCreationActivity.class);
         }

//               intent.putExtra(allVisualsKey, selectedPanel.getItems());

         intent.putExtra(panelKey, selectedPage);
         intent.putExtra(createFromExistingKey, false);
         intent.putExtra(wellboreIdKey, wellboreId);

         startActivityForResult(intent, VISUAL_REQUEST);
      }
   }

   private void updateAdapter(List<Page> updatedPages)
   {
      panelAdapter = new PanelAdapter(this, updatedPages);
      listView.setOnItemClickListener(this);
      listView.setAdapter(panelAdapter);
      dashboard.setPages(updatedPages);
      CheckboxPopupUtility.prepareRemoveListView(listView, panelAdapter);
   }

   private List<Page> getPanels()
   {
      List<Page> pages = new ArrayList<Page>();

      for(CheckboxListItem<Page> checkboxListItem : panelAdapter.getAllData())
      {
         pages.add(checkboxListItem.getItem());
      }

      return pages;
   }

   private void addPanel(Page newPage)
   {
      List<Page> updatedPages = new ArrayList<>();

      if(newPage == null)
      {
         return;
      }

      updatedPages.add(newPage);
      for(Page page : dashboard.getPages())
      {
         updatedPages.add(page);
      }

      updateAdapter(updatedPages);
   }

   private ArrayAdapter<Page> getTemplateAdapter()
   {
      final ArrayAdapter<Page> arrayAdapter = new ArrayAdapter<Page>(this, R.layout.sortable_list_row);

      new DashboardService(new UIRunnable<Dashboard>() {
         @Override
         public void run(List<Dashboard> data) {
            List<Page> existingPages = getPanels();

            for(Dashboard dashboard : data)
            {
               for(Page page : dashboard.getPages())
               {
                  if(!existingPages.contains(page))
                  {
                     arrayAdapter.add(page);
                  }
               }
            }

            arrayAdapter.notifyDataSetChanged();
         }
      }).collectDashboards(wellboreId);

      return arrayAdapter;
   }
}
