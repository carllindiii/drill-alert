package cactig.drillalert.views.manage.creation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.ListHolder;
import cactig.drillalert.backend.model.well.Curve;
import cactig.drillalert.backend.service.CurvesService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.R;
import resources.CheckboxListItem;
import resources.adapter.CheckboxArrayAdapter;
import resources.adapter.utility.IntentKeyManager;

/**
 * Created by cjohnson on 3/31/15.
 */
public class CurveSelectionActivity extends ActionBarActivity implements UIRunnable<Curve> {
   private CheckboxArrayAdapter<Curve> dataAdapter;
   private View view;
   private ListView listView;

   private static final String TAG = "CurveSelection";

   private static final String wellboreIdKey = IntentKeyManager.getWellboreIdKey();
   private static final String selectedCurvesKey = IntentKeyManager.getSelectedCurvesKey();
   private static final String allCurvesKey = IntentKeyManager.getAllCurvesKey();


   private String wellboreId;
   private List<String> selectedCurveIds;
   private List<Curve> allCurves;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      view = getLayoutInflater().inflate(R.layout.activity_curve_select, null);
      listView = (ListView)view.findViewById(R.id.curves_list);

      setContentView(view);

      loadIntentData();

      run(allCurves);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.select, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle_light_blue clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      switch (id) {
         case R.id.select:
            List<CheckboxListItem> checkedItems = getCheckedItems(dataAdapter);
            List<Curve> curveList = new ArrayList<>();

            for(CheckboxListItem checkedItem : checkedItems)
            {
               if(checkedItem != null && checkedItem.getItem() instanceof Curve)
               {
                  curveList.add((Curve)checkedItem.getItem());
               }
            }
            ListHolder<Curve> selectedListHolder = new ListHolder(curveList);

            Intent intent = new Intent();
            intent.putExtra(selectedCurvesKey, selectedListHolder);
            setResult(Activity.RESULT_OK, intent);
            finish();
            break;
         case android.R.id.home:
            setResult(Activity.RESULT_CANCELED);
            finish();
         default:
            break;
      }

      return true;
   }

   @Override
   public void run(List<Curve> data) {
      //Array list of countries
      ArrayList<CheckboxListItem> checkboxListItemList = new ArrayList<CheckboxListItem>();

      for(Curve curve: data)
      {
         boolean checked = isChecked(curve);
         checkboxListItemList.add(new CheckboxListItem(curve, checked, checked));
      }

      //create an ArrayAdaptar from the String Array
      dataAdapter = new CheckboxArrayAdapter(this, R.layout.checkbox_list_item, checkboxListItemList);

      listView.setAdapter(dataAdapter);
   }

   private static List<CheckboxListItem> getCheckedItems(CheckboxArrayAdapter<Curve> checkboxAdapter)
   {
      List<CheckboxListItem> checkedItems = new ArrayList<>();

      for(CheckboxListItem checkboxListItem : checkboxAdapter.getAllData())
      {
         if(checkboxListItem.isChecked()) {
            checkedItems.add(checkboxListItem);
         }
      }

      return checkedItems;
   }

   private void loadIntentData()
   {
      Intent intent = getIntent();

      if(intent == null)
      {
         Log.d(TAG, "Unable to retrieve intent");
      }
      else
      {
         wellboreId = intent.getStringExtra(wellboreIdKey);

         if(wellboreId == null || wellboreId.isEmpty()) {
            Log.d(TAG, "Unable to retrieve wellboreId from the intent");
         }

         Object object = intent.getSerializableExtra(selectedCurvesKey);

         if(object != null && object instanceof ListHolder)
         {
            selectedCurveIds = ((ListHolder<String>)object).getList();
         }

         object = intent.getSerializableExtra(allCurvesKey);

         if(object != null && object instanceof ListHolder)
         {
            allCurves = ((ListHolder<Curve>)object).getList();
         }
      }
   }

   private boolean isChecked(Curve curve)
   {
      if(curve != null) {
         for (String selectedCurve : selectedCurveIds) {
            if (curve.getId().equals(selectedCurve)) {
               return true;
            }
         }
      }

      return false;
   }
}
