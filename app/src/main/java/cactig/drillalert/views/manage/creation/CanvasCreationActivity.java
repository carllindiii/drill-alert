package cactig.drillalert.views.manage.creation;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.ItemSettings;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.R;
import cactig.drillalert.backend.model.well.Curve;
import cactig.drillalert.backend.service.CurvesService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.validation.Validator;
import resources.CheckboxElementListener;
import resources.NameGenerator;
import resources.adapter.CanvasListAdapter;
import resources.adapter.utility.AdapterFactory;
import resources.adapter.utility.CheckboxPopupUtility;
import resources.adapter.utility.IntentKeyManager;
import resources.dragndroplist.TouchInterceptor;

public class CanvasCreationActivity extends ActionBarActivity {
   private TouchInterceptor mList;
   private TouchInterceptor.DropListener mDropListener;

   private static final int CANVAS = 0;
   private static final int PLOT = 1;
   private static final int COMPASS = 2;

   private static final String panelKey = IntentKeyManager.getPanelKey();
   private static final String wellboreIdKey = IntentKeyManager.getWellboreIdKey();
   private static final String createFromExistingKey = IntentKeyManager.getUseAsTemplateKey();

   private boolean createFromExisting;

   private String wellboreId;

   private Spinner curvesSpinner;
   private EditText nameEditText;

   private CanvasListAdapter dataAdapter;
   private Button addGaugeButton;
   private Button addReadoutButton;

   private NameGenerator nameGenerator = NameGenerator.getInstance();

   private Page page;

   private static final String TAG = "CanvasCreationActivity";

   private ListView listView;

   private boolean modifying;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_canvas_creation);

      listView = (ListView)findViewById(R.id.canvasList);
      curvesSpinner = (Spinner)findViewById(R.id.canvas_spinner);
      nameEditText = (EditText)findViewById(R.id.canvas_name);
      addGaugeButton = (Button)findViewById(R.id.add_gauge_button);
      addReadoutButton = (Button)findViewById(R.id.add_number_readout_button);

      curvesSpinner.setAdapter(new ArrayAdapter<Curve>(getApplicationContext(), android.R.layout.simple_list_item_1, new ArrayList<Curve>()));

      Intent intent = getIntent();
      Page page = (Page)intent.getSerializableExtra(panelKey);

      modifying = (page != null);

      wellboreId = intent.getStringExtra(wellboreIdKey);
      createFromExisting = intent.getBooleanExtra(createFromExistingKey, false);

      nameEditText.setText(NameGenerator.getInstance().generateName("Canvas"));

      loadFromExisting(page, createFromExisting);


      listView.setAdapter(dataAdapter);
      CheckboxPopupUtility.prepareRemoveListView(listView, dataAdapter);

      mDropListener = new TouchInterceptor.DropListener() {
         public void drop(int from, int to) {
            //Assuming that item is moved up the list
            int direction = -1;
            int loopStart = from;
            int loopEnd = to;

            //For instance where the item is dragged down the list
            if(from < to) {
               direction = 1;
            }

            Item target = dataAdapter.getItem(from);

            for(int index = loopStart; index != loopEnd; index = index + direction){
               dataAdapter.set(index, dataAdapter.getItem(index + direction));
            }

            dataAdapter.set(to, target);
         }
      };

      mList = (TouchInterceptor)listView;
      mList.setDropListener(mDropListener);
      registerForContextMenu(mList);

      initializeSpinner();

      addGaugeButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Item item = getItem((Curve) curvesSpinner.getSelectedItem(), "Gauge");
            dataAdapter.add(item);

            addGaugeButton.setEnabled(false);
            addGaugeButton.setBackgroundColor(Color.GRAY);
         }
      });

      addReadoutButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Item item = getItem((Curve) curvesSpinner.getSelectedItem(), "Readout");
            dataAdapter.add(item);

            addReadoutButton.setEnabled(false);
            addReadoutButton.setBackgroundColor(Color.GRAY);
         }
      });

      setTitle("Create a Canvas");

      updateButtonsEnabled();

      addFocusListeners();
   }


   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.save_create_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle_light_blue clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      switch (id) {
         case R.id.save:
            if(validate()) {
               page = getPage();
               Intent intent = new Intent();
               intent.putExtra(panelKey, page);
               setResult(Activity.RESULT_OK, intent);
               finish();
            }
            break;
         case R.id.use_template:
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
            builderSingle.setTitle("Select One Name:-");
            builderSingle.setNegativeButton("cancel",
                    new DialogInterface.OnClickListener() {

                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                       }
                    });

            final ArrayAdapter<Page> adapter = AdapterFactory.getTemplatePanelAdapter(CANVAS, wellboreId, this);

            builderSingle.setAdapter(adapter,
                    new DialogInterface.OnClickListener() {

                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          page = adapter.getItem(which);
                          loadFromExisting(page, true);
                       }
                    });
            builderSingle.show();
            break;
         default:
            break;
      }

      return true;
   }

   private void initializeSpinner()
   {
      curvesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
         @Override
         public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            updateButtonsEnabled();
         }

         @Override
         public void onNothingSelected(AdapterView<?> parent) {

         }
      });

      new CurvesService(new UIRunnable<Curve>() {
         @Override
         public void run(List<Curve> curves) {
            ArrayAdapter<Curve> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, curves);

            curvesSpinner.setAdapter(arrayAdapter);
         }
      }).collectCurves(wellboreId, "Time");
   }

   private Item getItem(Curve curve, String type)
   {
      Integer xPos = 0;
      Integer yPos = 0;
      List<String> curveIds = new ArrayList<>();
      String jsFile = null;

      if(type.equalsIgnoreCase("gauge"))
      {
         jsFile = "gauge.js";
      }
      else
      {
         jsFile = "readout.js";
         Log.e("CANVAS CREATION", "NO SUPPORT FOR READOUTS");
      }

      if(curve == null) {
         Log.d(TAG, "Invalid curve");
      }
      else
      {
         curveIds.add(curve.getId());
         Item newItem = new Item();

         newItem.setJsFile(type);
         newItem.setXPos(xPos);
         newItem.setYPos(yPos);
         newItem.setJsFile(jsFile);
         newItem.setItemSettings(new ArrayList<ItemSettings>());
         newItem.setPageId(page.getId());
         // set object variables here

         return newItem;
      }

      return null;
   }

   private void updateButtonsEnabled()
   {
      Curve curve = (Curve) curvesSpinner.getSelectedItem();
      addGaugeButton.setEnabled(true);
      addGaugeButton.setBackgroundColor(getResources().getColor(R.color.canvas_add_gauge_background));
      addReadoutButton.setEnabled(true);
      addReadoutButton.setBackgroundColor(getResources().getColor(R.color.canvas_add_readout_background));


//      for (Item item : dataAdapter.getAllData()) {
//         if (item.getCurveIds() != null && curve != null && curve.getId() != null && item.getCurveIds().contains(curve.getId())) {
//            if (item.getJsFile().equalsIgnoreCase("Gauge")) {
//               addGaugeButton.setEnabled(false);
//               addGaugeButton.setBackgroundColor(Color.GRAY);
//            } else {
//               addReadoutButton.setEnabled(false);
//               addReadoutButton.setBackgroundColor(Color.GRAY);
//            }
//         }
//      }
   }

   private Page getPage() {
      //TODO: Determine the x/y positions

      String name = nameEditText.getText().toString();
      List<Item> visualizations = dataAdapter.getAllData();

      page.setName(name);
      page.setType(CANVAS);
      page.setItems(visualizations);

      return page;
   }

   private void loadFromExisting(Page page, boolean createFromExisting)
   {
      List<Item> items;

      if(page == null)
      {
         items = new ArrayList<>();
         this.page = new Page();
      }
      else
      {
         items = page.getItems();

         nameEditText.setText(page.getName());

         this.page = page;
      }

      if(createFromExisting)
      {
         nameEditText.setText(nameGenerator.generateName("Canvas"));

         if(page != null)
         {
            page.setId(null);
         }
      }

      if(dataAdapter == null) {
         dataAdapter = new CanvasListAdapter(this, R.layout.sortable_list_row, items);
         dataAdapter.addCheckboxElementListener(new CheckboxElementListener() {
            @Override
            public void fireElementsRemoved() {
               updateButtonsEnabled();
            }
         });
      }
      else
      {
         dataAdapter.setData(items);
      }

      listView.setAdapter(dataAdapter);

      updateButtonsEnabled();
   }

   private void addFocusListeners()
   {
      nameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
         @Override
         public void onFocusChange(View v, boolean hasFocus) {
            validateName();
         }
      });
   }

   private boolean validateName()
   {
      if(!modifying && !Validator.isValidName(nameEditText.getText().toString()))
      {
         Toast.makeText(getApplicationContext(), "The \"Name\" must be unique and non-empty", Toast.LENGTH_SHORT).show();
         nameEditText.setText(NameGenerator.getInstance().generateName("Canvas"));

         return false;
      }

      return true;
   }

   private boolean validate()
   {
      boolean valid = validateName();

      if(dataAdapter.getAllData().isEmpty())
      {
         Toast.makeText(getApplicationContext(), "Please add at least one gauge or readout before saving", Toast.LENGTH_SHORT).show();

         valid = false;
      }

      return valid;
   }


}
