package cactig.drillalert.views.manage.creation;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.LoginWebViewClient;
import cactig.drillalert.backend.model.ItemXCurve;
import cactig.drillalert.backend.model.ListHolder;
import cactig.drillalert.backend.model.dashboard.ItemSettings;
import cactig.drillalert.backend.model.dashboard.ItemSettingsInternal;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.backend.model.well.Curve;
import cactig.drillalert.R;
import cactig.drillalert.backend.service.CurvesService;
import cactig.drillalert.backend.service.ItemCurvesService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.validation.InputFilterMinMax;
import cactig.drillalert.validation.Validator;
import resources.NameGenerator;
import resources.adapter.utility.AdapterFactory;
import resources.adapter.utility.IntentKeyManager;

public class PlotCreationActivity extends ActionBarActivity {
   private List<ItemSettingsInternal> itemSettingsSpinnerModel = new ArrayList<>();
   private Activity context = this;

   private LoginWebViewClient loginWebViewClient = LoginWebViewClient.getInstance();

   private String panelKey = IntentKeyManager.getPanelKey();
   private String useAsTemplateKey = IntentKeyManager.getUseAsTemplateKey();
   private String visualKey = IntentKeyManager.getSelectedVisualKey();
   private String selectedCurvesKey = IntentKeyManager.getSelectedCurvesKey();
   private String wellboreIdKey = IntentKeyManager.getWellboreIdKey();
   private String allCurvesKey = IntentKeyManager.getAllCurvesKey();

   private String wellboreId;

   private static final int CURVES_REQUEST_CODE = 0;

   private static final int CANVAS = 0;
   private static final int PLOT = 1;
   private static final int COMPASS = 2;

   private static final String TAG = "PlotCreationActivity";

   private int tempId = 0;
   private int previousPosition = 0;
   private List<Curve> selectedCurves = new ArrayList<>();
   private Page pageToModify;
   private boolean useAsTemplate;

   private static final int DEFAULT_STEP_SIZE = 5;
   private static final int DEFAULT_START_RANGE = 0;
   private static final int DEFAULT_END_RANGE = 100;

   private static final int DEFAULT_DIVISION_SIZE = 5;

   private boolean modifying;

   private static final int LINEAR = 0;
   private static final int LOGARITHMIC = 1;

   private List<Curve> allCurves;

   private List<ItemXCurve> itemXCurves = new ArrayList<>();

   private static final int TRUE_VERTICAL_DEPTH = 0;
   private static final int TIME = 1;
   private static final int DEPTH = 2;
   private static final int VERTICAL_SECTION = 3;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      setContentView(R.layout.activity_plot_creation);
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      getSupportActionBar().setHomeButtonEnabled(false);

      //Add action listeners to hide/expand submenus
      Button ivtSettingsButton = (Button) findViewById(R.id.ivt_settings_button);
      ivtSettingsButton.setOnClickListener(getHideExpandButtonListener(findViewById(R.id.ivt_settings_menu)));

      Button itemSettingsSettingsButton = (Button) findViewById(R.id.item_settings_button);
      itemSettingsSettingsButton.setOnClickListener(getHideExpandButtonListener(findViewById(R.id.item_settings_menu)));

      //initialize the spinner
      initializeIVTSpinner((Spinner) findViewById(R.id.ivt_options_spinner));

      final Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      itemSettingsSpinner.setAdapter(new ArrayAdapter<ItemSettingsInternal>(this, android.R.layout.simple_spinner_item, (ArrayList) itemSettingsSpinnerModel));
      itemSettingsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
         @Override
         public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            updateItemInfo();
            initializeItemSettingsSettingsPanel();
            initializeIVTSettingsPanel();
            previousPosition = getCurrentItemSettingsPosition();
         }

         @Override
         public void onNothingSelected(AdapterView<?> parent) {

         }
      });

      //initialize itemSettings add/remove buttons
      Button addItemSettingsButton = (Button) findViewById(R.id.add_item_settings_button);
      addItemSettingsButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            addItemSettings();
         }
      });
      addItemSettings();

      final Button removeItemSettingsButton = (Button) findViewById(R.id.remove_item_settings_button);
      removeItemSettingsButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            removeItemSettings();
         }
      });
      removeItemSettingsButton.setEnabled(false);

      //initialize curve add/remove buttons
      Button selectCurvesButton = (Button) findViewById(R.id.select_curves_button);
      selectCurvesButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Intent intent = new Intent(context, CurveSelectionActivity.class);
            ListHolder<String> listHolder = new ListHolder<String>(itemSettingsSpinnerModel.get(getCurrentItemSettingsPosition()).getCurveIds());

            intent.putExtra(wellboreIdKey, wellboreId);
            intent.putExtra(allCurvesKey, new ListHolder<Curve>(getFilteredCurves(getIVTType())));
            intent.putExtra(selectedCurvesKey, listHolder);
            startActivityForResult(intent, CURVES_REQUEST_CODE);
         }
      });

      getIntentData();

      new CurvesService(getCurvesRunnable()).collectCurves(wellboreId);

      EditText nameEditText = (EditText)findViewById(R.id.plot_name);
      nameEditText.setText(NameGenerator.getInstance().generateName("Plot"));

      modifying = (pageToModify != null);

      if(pageToModify == null)
      {
         pageToModify = new Page();
      }
      else
      {
         loadFromExisting(pageToModify, useAsTemplate);
      }

      addFilterToEditText();
      addFocusListeners();

   }

   private void setItemXCurves(List<ItemXCurve> itemXCurves)
   {
      this.itemXCurves = itemXCurves;
   }

   private List<Curve> getFilteredCurves(int IVT)
   {
      List<Curve> filtered = new ArrayList<>();

      for(Curve curve : allCurves)
      {
         if(curve.getIvt() == IVT)
         {
            filtered.add(curve);
         }
      }

      return filtered;
   }

   private int getTypeValue(String type)
   {
      if(type.equals("Depth"))
      {
         return DEPTH;
      }
      else if(type.equals("True Vertical Depth"))
      {
         return TRUE_VERTICAL_DEPTH;
      }
      else if(type.equals("Time"))
      {
         return TIME;
      }
      else
      {
         return VERTICAL_SECTION;
      }
   }

   private UIRunnable<Curve> getCurvesRunnable()
   {
      return new UIRunnable<Curve>() {
         @Override
         public void run(List<Curve> data) {
            Button selectCurvesButton = (Button)findViewById(R.id.select_curves_button);

            selectCurvesButton.setEnabled(true);

            setAllCurves(data);
         }
      };
   }

   private void setAllCurves(List<Curve> allCurves)
   {
      if(allCurves == null)
      {
         allCurves = new ArrayList<>();
      }
      this.allCurves = allCurves;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.save_create_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();

      switch (id) {
         case R.id.save:
            updateItemInfo();
            pageToModify.setName(getName());
            if(validateItemSettingss())
            {
               Intent intent = new Intent();
               intent.putExtra(panelKey, getPage());
               setResult(Activity.RESULT_OK, intent);

               //TODO: CJ - it seems like the item id is null here.
               // update the item X curves on the backend
               List<ItemXCurve> itemXCurves = getItemXCurves();
               for(ItemXCurve itemXCurve : itemXCurves) {
                  new ItemCurvesService(null).updateItemCurve(itemXCurve);
               }

               finish();
            }

            break;
         case R.id.use_template:
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
            builderSingle.setTitle("Select One Name:-");
            builderSingle.setNegativeButton("cancel",
                    new DialogInterface.OnClickListener() {

                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                       }
                    });

            final ArrayAdapter<Page> adapter = AdapterFactory.getTemplatePanelAdapter(PLOT, wellboreId, this);
            builderSingle.setAdapter(adapter,
                    new DialogInterface.OnClickListener() {

                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          pageToModify = adapter.getItem(which);
                          loadFromExisting(pageToModify, true);
                       }
                    });
            builderSingle.show();
            break;
         default:
            break;
      }

      return true;
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      ListHolder<Curve> listHolder = null;
      switch(requestCode) {
         case (CURVES_REQUEST_CODE) : {
            if (resultCode == Activity.RESULT_OK && data != null) {
               listHolder = (ListHolder) data.getSerializableExtra(selectedCurvesKey);

               if(listHolder == null)
               {
                  Log.d(TAG, "Unable to retrieve allCurves");
               }
               else
               {
                  selectedCurves = listHolder.getList();
               }

               updateItemInfo();
            }
         }
         break;
      }
   }

   private void initializeIVTSpinner(Spinner spinner) {
      ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this, R.array.ivt_array, android.R.layout.simple_spinner_item);
      arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

      spinner.setAdapter(arrayAdapter);
   }

   private void initializeNumberPicker(NumberPicker numberPicker) {
      final int offset = -(Integer.MAX_VALUE / 2);

      numberPicker.setMaxValue(Integer.MAX_VALUE);
      numberPicker.setMinValue(0);
      numberPicker.setValue(offset);

      numberPicker.setFormatter(new NumberPicker.Formatter() {
         @Override
         public String format(int index) {
            return Integer.toString(index + offset);
         }
      });
   }

   private View.OnClickListener getHideExpandButtonListener(final View viewToHideExpand) {
      return new View.OnClickListener() {
         @Override
         public void onClick(View v) {

            if (viewToHideExpand.getVisibility() == View.VISIBLE) {
               viewToHideExpand.setVisibility(View.GONE);
            } else {
               viewToHideExpand.setVisibility(View.VISIBLE);
            }
         }
      };
   }

   private void addItemSettings()
   {
      Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      Button removeSettingsItemButton = (Button) findViewById(R.id.remove_item_settings_button);

      updateItemInfo();

      String name = "track" + tempId;
      int ivtStepSize = getIVTStepSize();
      int ivtStartRange = getStartRange();
      int ivtEndRange = getEndRange();

      List<String> curveIds = getCurveIds();

      if(curveIds.isEmpty() && !itemSettingsSpinnerModel.isEmpty()){
         Toast toast = Toast.makeText(this, "Please select the curves to display in the current Track before adding another.", Toast.LENGTH_LONG);
         toast.show();
      }
      else {
         ItemSettingsInternal itemSettings = new ItemSettingsInternal();
         itemSettings.setStepSize(ivtStepSize);
         itemSettings.setStartRange(ivtStartRange);
         itemSettings.setEndRange(ivtEndRange);
         itemSettings.setDivisionSize(getDivisionSize());
         itemSettings.setScaleType(getScaleType());
         itemSettings.setCurveIds(curveIds);

         itemSettingsSpinnerModel.add(itemSettings);
         itemSettingsSpinner.setSelection(itemSettingsSpinnerModel.size() - 1);
         previousPosition = itemSettingsSpinnerModel.size() - 1;
         ((ArrayAdapter) itemSettingsSpinner.getAdapter()).notifyDataSetChanged();

         initializeItemSettingsSettingsPanel(DEFAULT_DIVISION_SIZE, LINEAR);
         initializeIVTSettingsPanel(DEFAULT_STEP_SIZE, DEFAULT_START_RANGE, DEFAULT_END_RANGE);

         if (itemSettingsSpinnerModel.size() > 1) {
            removeSettingsItemButton.setEnabled(true);
         }
      }

      curveIds = new ArrayList<>();
   }

   private void addItemSettings(ItemSettingsInternal itemSettings)
   {
      Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      Button removeButton = (Button)findViewById(R.id.remove_item_settings_button);
      Button addButton = (Button)findViewById(R.id.add_item_settings_button);
      List<String> curveIds = itemSettings.getCurveIds();
      selectedCurves = new ArrayList<>();

      if(curveIds != null) {
         for (String id : curveIds) {
            selectedCurves.add(new Curve(id, null, null, null, null, null));
         }
      }

      itemSettingsSpinnerModel.add(itemSettings);
      itemSettingsSpinner.setSelection(itemSettingsSpinnerModel.size() - 1);

      previousPosition = itemSettingsSpinnerModel.size() - 1;
      ((ArrayAdapter) itemSettingsSpinner.getAdapter()).notifyDataSetChanged();

      initializeItemSettingsSettingsPanel(itemSettings.getDivisionSize(), itemSettings.getScaleType());
      initializeIVTSettingsPanel(itemSettings.getStepSize(), itemSettings.getStartRange(), itemSettings.getEndRange());

      if (itemSettingsSpinnerModel.size() > 1) {
         removeButton.setEnabled(true);
      }

      if (itemSettingsSpinnerModel.size() > 2) {
         addButton.setEnabled(false);
      }
   }

   private void updateItemInfo()
   {
      if(previousPosition < itemSettingsSpinnerModel.size()) {

         int ivtStepSize = getIVTStepSize();
         int ivtStartRange = getStartRange();
         int ivtEndRange = getEndRange();

         int divisionSize = getDivisionSize();
         int scaleType = getScaleType();

         ItemSettingsInternal itemSettings = (ItemSettingsInternal) itemSettingsSpinnerModel.get(previousPosition);
         itemSettings.setStepSize(ivtStepSize);
         itemSettings.setStartRange(ivtStartRange);
         itemSettings.setEndRange(ivtEndRange);
         itemSettings.setDivisionSize(divisionSize);
         itemSettings.setScaleType(scaleType);
         itemSettings.setCurveIds(getCurveIds());
      }
   }

   private void removeItemSettings()
   {
      Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      Button removeItemSettingsButton = (Button) findViewById(R.id.remove_item_settings_button);
      Button addButton = (Button)findViewById(R.id.add_item_settings_button);

      int currentPosition = itemSettingsSpinner.getSelectedItemPosition();

      itemSettingsSpinnerModel.remove(currentPosition);
      itemSettingsSpinner.setSelection(currentPosition - 1);
      previousPosition = currentPosition - 1;

      if(itemSettingsSpinnerModel.size() < 2)
      {
         Button removeButton = (Button)findViewById(R.id.remove_item_settings_button);
         removeButton.setEnabled(false);
      }

      if(itemSettingsSpinnerModel.size() < 2)
      {
         removeItemSettingsButton.setEnabled(false);
      }

      addButton.setEnabled(true);
   }

   private String getName()
   {
      EditText editText = (EditText)findViewById(R.id.plot_name);

      return editText.getText().toString();
   }

   private int getIVTType()
   {
      Spinner ivtTypeSpinner = (Spinner)findViewById(R.id.ivt_options_spinner);

      return ivtTypeSpinner.getSelectedItemPosition();
   }

   private int getIVTStepSize()
   {
      EditText editText = (EditText)findViewById(R.id.ivt_step_size);

      return parseInteger(editText.getText().toString());
   }

   private int getStartRange()
   {
      EditText editText = (EditText)findViewById(R.id.ivt_start_range);

      return parseInteger(editText.getText().toString());
   }

   private int getEndRange()
   {
      EditText editText = (EditText)findViewById(R.id.ivt_end_range);

      return parseInteger(editText.getText().toString());
   }

   private List<String> getCurveIds()
   {
      List<String> selectedIds = new ArrayList<>();

      for(int index = 0; index < selectedCurves.size(); index++)
      {
         selectedIds.add(selectedCurves.get(index).getId());
      }

      return selectedIds;
   }

   private int getDivisionSize()
   {
      EditText divisionSize = (EditText) findViewById(R.id.item_settings_division_size);

      return parseInteger(divisionSize.getText().toString());
   }

   private boolean getLinear()
   {
      RadioButton linear = (RadioButton) findViewById(R.id.linear_radio_button);
      return linear.isChecked();
   }

   private int getScaleType()
   {
      if(getLogarithmic())
      {
         return LOGARITHMIC;
      }
      else
      {
         return LINEAR;
      }
   }

   private boolean getLogarithmic()
   {
      RadioButton logarithmic = (RadioButton) findViewById(R.id.logarithmic_radio_button);

      return logarithmic.isChecked();
   }

   private int parseInteger(String text)
   {
      try{
         return Integer.parseInt(text);
      }
      catch (Exception exception)
      {
         Log.d(TAG, exception.getMessage());
      }

      return 0;
   }

   private Page getPage()
   {
      String name = getName();

      pageToModify.setName(name);
      pageToModify.setXDim(0);
      pageToModify.setYDim(0);
      pageToModify.setType(PLOT);
      pageToModify.setItems(getItems());

      return pageToModify;
   }

   private List<ItemXCurve> getItemXCurves()
   {
      String jsFile = "graph.js";
      Integer pageId = pageToModify.getId();
      List<ItemXCurve> itemXCurves = new ArrayList<>();
      int id = 0;
      for(ItemSettingsInternal itemSettingsInternal : itemSettingsSpinnerModel)
      {
         ItemSettings itemSettings = new ItemSettings(itemSettingsInternal.getItemId(), itemSettingsInternal.getId(), itemSettingsInternal.getStepSize(), itemSettingsInternal.getStartRange(),
                 itemSettingsInternal.getEndRange(), itemSettingsInternal.getDivisionSize(), itemSettingsInternal.getScaleType());
         List<ItemSettings> itemSettingses = new ArrayList<>();
         itemSettingses.add(itemSettingsInternal);

         Item item = new Item();
         item.setJsFile(jsFile);
         item.setPageId(pageId);
         item.setXPos(0);
         item.setYPos(0);
         item.setItemSettings(itemSettingses);

         for(String curveId : itemSettingsInternal.getCurveIds()) {
            ItemXCurve itemXCurve = new ItemXCurve(id++, item.getId(), curveId);
            itemXCurves.add(itemXCurve);
         }
      }

      return itemXCurves;
   }

   private List<Item> getItems()
   {
      String jsFile = "graph.js";
      Integer pageId = pageToModify.getId();
      ArrayList<Item> items = new ArrayList<>();

      for(ItemSettingsInternal itemSettingsInternal : itemSettingsSpinnerModel)
      {
         //TODO: Bad constructor call - fix by passing in the step size and itemid
         ItemSettings itemSettings = new ItemSettings(itemSettingsInternal.getStepSize(), itemSettingsInternal.getStartRange(),
                 itemSettingsInternal.getEndRange(), itemSettingsInternal.getDivisionSize(), itemSettingsInternal.getScaleType());
         List<ItemSettings> itemSettingses = new ArrayList<>();
         itemSettingses.add(itemSettings);

         Item item = new Item();
         item.setJsFile(jsFile);
         item.setPageId(pageId);
         item.setXPos(0);
         item.setYPos(0);
         item.setItemSettings(itemSettingses);

         items.add(item);
      }

      return items;
   }

   private void initializeIVTSettingsPanel()
   {
      Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      int currentPosition = itemSettingsSpinner.getSelectedItemPosition();

      ItemSettingsInternal itemSettings = (ItemSettingsInternal) itemSettingsSpinnerModel.get(currentPosition);

      initializeIVTSettingsPanel(itemSettings.getStepSize(), itemSettings.getStartRange(), itemSettings.getEndRange());
   }

   private void initializeItemSettingsSettingsPanel()
   {
      Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      int currentPosition = itemSettingsSpinner.getSelectedItemPosition();

      ItemSettingsInternal itemSettings = (ItemSettingsInternal) itemSettingsSpinnerModel.get(currentPosition);

      initializeItemSettingsSettingsPanel(itemSettings.getDivisionSize(), itemSettings.getScaleType());
   }

   private void initializeIVTSettingsPanel(int stepSize, int startRange, int endRange)
   {
      int ivtSelectedIndex = getIVTSelectedIndex();

      Spinner ivtSpinner = (Spinner) findViewById(R.id.ivt_options_spinner);
      EditText stepSizeEditText = (EditText) findViewById(R.id.ivt_step_size);
      EditText startRangeEditText = (EditText) findViewById(R.id.ivt_start_range);
      EditText endRangeEditText = (EditText) findViewById(R.id.ivt_end_range);

      stepSizeEditText.setText(Integer.toString(stepSize));
      startRangeEditText.setText(Integer.toString(startRange));
      endRangeEditText.setText(Integer.toString(endRange));

      ivtSpinner.setSelection(ivtSelectedIndex);
   }

   private void initializeItemSettingsSettingsPanel(int divisionSize, int scaleType)
   {
      Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      Button itemSettingsSettingsButton = (Button) findViewById(R.id.item_settings_button);
      EditText divisionSizeEditText = (EditText) findViewById(R.id.item_settings_division_size);
      RadioButton linearRadioButton = (RadioButton) findViewById(R.id.linear_radio_button);
      RadioButton logarithmicRadioButton = (RadioButton) findViewById(R.id.logarithmic_radio_button);
      String name = itemSettingsSpinner.getSelectedItem().toString();

      itemSettingsSettingsButton.setText(name + " Settings");
      divisionSizeEditText.setText(Integer.toString(divisionSize));
      if(scaleType == LOGARITHMIC) {
         logarithmicRadioButton.setChecked(true);
      }
      else
      {
         linearRadioButton.setChecked(true);
      }
   }

   private int getIVTSelectedIndex()
   {
      Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      ItemSettingsInternal itemSettings = (ItemSettingsInternal)itemSettingsSpinner.getSelectedItem();

//      TODO: CJ - Depending on Gabe's response,
//      return itemSettings.getIVT() or iterate over all allCurves for the well, find the corresponding curve and get the ivt from the curve.

      return 0;
   }

   private int getCurrentItemSettingsPosition()
   {
      Spinner itemSettingsSpinner = (Spinner) findViewById(R.id.track_spinner);
      int currentPosition = itemSettingsSpinner.getSelectedItemPosition();

      return currentPosition;
   }

   private void getIntentData()
   {
      Intent intent = getIntent();

      if(intent != null)
      {
         useAsTemplate = intent.getBooleanExtra(useAsTemplateKey, false);

         Object panelObject = intent.getSerializableExtra(panelKey);

         if(panelObject != null)
         {
            if(panelObject instanceof Page)
            {
               pageToModify = (Page)panelObject;
            }
            else
            {
               Log.d(TAG, "Expected panel but found a different type of object");
            }
         }

         wellboreId = intent.getStringExtra(wellboreIdKey);
         if(wellboreId == null || wellboreId.isEmpty()) {
            Log.d(TAG, "Unable to retrieve wellboreId from intent");
         }
      }
   }

   private void loadFromExisting(Page page, boolean useAsTemplate)
   {
      pageToModify = page;

      if(useAsTemplate)
      {
         pageToModify.setId(null);
         pageToModify.setDashboardId(null);
      }

      if(!useAsTemplate)
      {
         setName(page.getName());
      }

//      //CLEAR any existing data


      for(final Item item : page.getItems())
      {
         new ItemCurvesService(new UIRunnable<ItemXCurve>() {
            @Override
            public void run(List<ItemXCurve> data) {
               List<String> curvesIds = getCurveIds(item.getId(), data);
               ItemSettingsInternal itemSettingsInternal =
                       new ItemSettingsInternal(item.getItemSettings().get(0), curvesIds);

               for(int i = 0; i < itemSettingsSpinnerModel.size();)
               {
                  itemSettingsSpinnerModel.remove(i);
               }

               addItemSettings(itemSettingsInternal);

            }
         }).collectItemCurves(item.getId());
      }
   }

   private List<String> getCurveIds(int id, List<ItemXCurve> itemXCurves)
   {
      List<String> curveIds = new ArrayList<String>();

      for(ItemXCurve itemXCurve : itemXCurves)
      {
         if(itemXCurve.getId() == id)
         {
            curveIds.add(itemXCurve.getCurveId());
         }
      }

      return curveIds;
   }

   private void setName(String name)
   {
      EditText nameEditText = (EditText) findViewById(R.id.plot_name);

      if(name == null){
         Log.d(TAG, "Attempted to set plot name to null");
      }
      else {
         nameEditText.setText(name);
      }
   }

   private void addFilterToEditText()
   {
      EditText stepSize = (EditText) findViewById(R.id.ivt_step_size);
      EditText divisionSize = (EditText) findViewById(R.id.item_settings_division_size);

      stepSize.setFilters(new InputFilter[]{new InputFilterMinMax("1", Integer.toString(Integer.MAX_VALUE))});
      divisionSize.setFilters(new InputFilter[]{new InputFilterMinMax("1", Integer.toString(Integer.MAX_VALUE))});
   }

   private void addFocusListeners() {
      final EditText name = (EditText) findViewById(R.id.plot_name);

      final EditText stepSize = (EditText) findViewById(R.id.ivt_step_size);
      final EditText startRange = (EditText) findViewById(R.id.ivt_start_range);
      final EditText endRange = (EditText) findViewById(R.id.ivt_end_range);

      final EditText divisionSize = (EditText) findViewById(R.id.item_settings_division_size);
      final NameGenerator nameGenerator = NameGenerator.getInstance();

      name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
         @Override
         public void onFocusChange(View v, boolean hasFocus) {
            if(!modifying && !Validator.isValidName(name.getText().toString()))
            {
               name.setText(nameGenerator.generateName(getName()));
               Toast.makeText(getApplicationContext(), "The \"Name\" must be unique and non-empty", Toast.LENGTH_SHORT).show();
            }
         }
      });

      stepSize.setOnFocusChangeListener(getOnFocusChangeListener("Step size", "5"));
      divisionSize.setOnFocusChangeListener(getOnFocusChangeListener("Division size", "5"));

      startRange.setOnFocusChangeListener(new View.OnFocusChangeListener() {
         @Override
         public void onFocusChange(View view, boolean hasFocus) {
            EditText editText = (EditText) view;
            String text = editText.getText().toString();
            if (text.isEmpty()) {
               editText.setText("0");
               Toast.makeText(getApplicationContext(), "The \"Start Range\" must be non-empty", Toast.LENGTH_SHORT).show();
            }

            validateStartEndRanges();
         }
      });

      endRange.setOnFocusChangeListener(new View.OnFocusChangeListener() {
         @Override
         public void onFocusChange(View view, boolean hasFocus) {
            EditText editText = (EditText) view;
            String text = editText.getText().toString();
            if (text.isEmpty()) {
               editText.setText("0");
               Toast.makeText(getApplicationContext(), "The \"End Range\" must be non-empty", Toast.LENGTH_SHORT).show();
            }

            validateStartEndRanges();
         }
      });
   }

   private View.OnFocusChangeListener getOnFocusChangeListener(final String name, final String defaultText)
   {
      return new View.OnFocusChangeListener() {
         @Override
         public void onFocusChange(View view, boolean hasFocus) {
            EditText editText = (EditText)view;
            if(editText.getText().toString().isEmpty()) {
               editText.setText(defaultText);
               Toast.makeText(getApplicationContext(), "The \"" + name + " \" must be non-empty", Toast.LENGTH_SHORT).show();
            }
         }
      };
   }

   //Assumes that the text in the fields is a number
   private void validateStartEndRanges()
   {
      EditText startRangeEditText = (EditText)findViewById(R.id.ivt_start_range);
      EditText endRangeEditText = (EditText)findViewById(R.id.ivt_end_range);

      Integer startRange = Integer.parseInt(startRangeEditText.getText().toString());
      Integer endRange = Integer.parseInt(endRangeEditText.getText().toString());

      if(endRange <= startRange)
      {
         endRangeEditText.setText(Integer.toString(startRange + 1));
         Toast.makeText(getApplicationContext(), "The \"Start Range\" must be smaller than the \"End Range\"", Toast.LENGTH_SHORT).show();
      }
   }

   private boolean validateItemSettingss()
   {
      for(ItemSettingsInternal itemSettings : itemSettingsSpinnerModel)
      {
         if(itemSettings.getCurveIds().isEmpty())
         {
            Toast.makeText(this, "Please select at least one curve for " + itemSettings.toString(), Toast.LENGTH_LONG).show();
            return false;
         }
      }

      return true;
   }
}
