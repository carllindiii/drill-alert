package cactig.drillalert.views.manage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.ListHolder;
import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.service.DashboardService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.R;
import resources.CheckboxListItem;
import resources.adapter.CheckboxArrayAdapter;
import resources.adapter.utility.IntentKeyManager;

/**
 * Created by cjohnson on 3/31/15.
 */
public class SelectViewsActivity extends ActionBarActivity implements UIRunnable<Dashboard> {
   private static final String TAG = "SelectViewsActivity";

   private CheckboxArrayAdapter<Dashboard> dataAdapter;

   private View view;
   private ListView listView;


   private String wellboreIdKey = IntentKeyManager.getWellboreIdKey();
   private String dashboardKey = IntentKeyManager.getDashboardsKey();

   private String wellboreId;

   private List<Dashboard> previousSelection;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      view = getLayoutInflater().inflate(R.layout.activity_curve_select, null);
      listView = (ListView)view.findViewById(R.id.curves_list);

      dataAdapter = new CheckboxArrayAdapter(this,
              R.layout.checkbox_list_item, new ArrayList<>());

      setContentView(view);

      Intent intent = getIntent();

      if(intent == null)
      {
         Log.d(TAG, "Unable to retrieve intent");
      }
      else
      {
         wellboreId = intent.getStringExtra(wellboreIdKey);

         if(wellboreId == null ||wellboreId.isEmpty()) {
            Log.d(TAG, "Unable to retrieve wellboreId");
         }

         Object object = intent.getSerializableExtra(dashboardKey);

         if(object != null && object instanceof ListHolder) {
            List<Dashboard> dashboards = ((ListHolder<Dashboard>)object).getList();

            previousSelection = dashboards;
         }
      }

      new DashboardService(this).collectDashboards(wellboreId);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.select, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();

      switch (id) {
         case R.id.select:
            List<CheckboxListItem> checkedItems = getCheckedItems(dataAdapter);
            List<Dashboard> dashboards = new ArrayList<>();

            for(CheckboxListItem checkedItem : checkedItems)
            {
               if(checkedItem != null && checkedItem.getItem() instanceof Dashboard)
               {
                  dashboards.add((Dashboard) checkedItem.getItem());
               }
            }

            ListHolder<Dashboard> listHolder = new ListHolder(dashboards);

            Intent intent = new Intent();
            intent.putExtra(dashboardKey, listHolder);
            setResult(Activity.RESULT_OK, intent);
            finish();
            break;
         case android.R.id.home:
            setResult(Activity.RESULT_CANCELED);
            finish();
         default:
            break;
      }

      return true;
   }

   @Override
   public void run(List<Dashboard> data) {
      //Array list of countries
      ArrayList<CheckboxListItem> checkboxListItemList = new ArrayList<CheckboxListItem>();

      for(Dashboard dashboard: data)
      {
         if(previousSelection == null || !previousSelection.contains(dashboard))
         {
            checkboxListItemList.add(new CheckboxListItem(dashboard, false, false));
         }
         else
         {
            checkboxListItemList.add(new CheckboxListItem(dashboard, true, true));
         }
      }

      //create an ArrayAdaptar from the String Array
      dataAdapter = new CheckboxArrayAdapter(this,
              R.layout.checkbox_list_item, checkboxListItemList);

      listView.setAdapter(dataAdapter);
   }

   private static List<CheckboxListItem> getCheckedItems(CheckboxArrayAdapter<Dashboard> checkboxAdapter)
   {
      List<CheckboxListItem> checkedItems = new ArrayList<>();

      for(CheckboxListItem checkboxListItem : checkboxAdapter.getAllData())
      {
         if(checkboxListItem.isChecked()) {
            checkedItems.add(checkboxListItem);
         }
      }

      return checkedItems;
   }
}
