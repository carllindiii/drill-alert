package cactig.drillalert.views.manage.creation;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.R;
import resources.adapter.utility.IntentKeyManager;

public class CompassCreationActivity extends ActionBarActivity {
   private String visualKey = IntentKeyManager.getSelectedVisualKey();
   private String createFromExistingKey = IntentKeyManager.getUseAsTemplateKey();

   private boolean createFromTemplate = false;
   private Item itemToModify = null;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      super.setTitle("Create a Compass");

      setContentView(R.layout.activity_compass_creation);
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      getSupportActionBar().setHomeButtonEnabled(false);

      List<String> sensorTypeList = new ArrayList<String>();
      sensorTypeList.add("Gyroscopic");
      ArrayAdapter sensorTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, sensorTypeList);
      sensorTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      ((Spinner)findViewById(R.id.sensorTypeSpinner)).setAdapter(sensorTypeAdapter);

      List<String> dataSourceList = new ArrayList<String>();
      dataSourceList.add("Tool Face 1");
      ArrayAdapter dataSourceAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, dataSourceList);
      dataSourceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      ((Spinner)findViewById(R.id.dataTypeSpinner)).setAdapter(dataSourceAdapter);

      Intent intent = getIntent();

      if(intent != null)
      {
         intent.getBooleanExtra(createFromExistingKey, false);
         Object object = intent.getSerializableExtra(visualKey);

         if(object != null && object instanceof Item)
         {
            itemToModify = (Item)object;

            initializeFields();
         }
      }
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.save_create_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle_light_blue clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      switch (id) {
         case R.id.save:
            Intent intent = new Intent();
            intent.putExtra(visualKey, getPanel());
            setResult(Activity.RESULT_OK, intent);
            finish();
            break;
         case R.id.use_template:
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
            builderSingle.setTitle("Select One Name:-");
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    this,
                    android.R.layout.simple_list_item_1);
            arrayAdapter.add("Compass 1");
            arrayAdapter.add("Compass 2");
            arrayAdapter.add("Compass 3");
            arrayAdapter.add("Compass 4");
            arrayAdapter.add("Compass 5");
            builderSingle.setNegativeButton("cancel",
                    new DialogInterface.OnClickListener() {

                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                       }
                    });

            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {

                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          //TODO: Setup up view based on a selected item
                       }
                    });
            builderSingle.show();
            break;
         default:
            break;
      }

      return true;
   }

   private void initializeSpinner(Spinner spinner, int arrayResource) {
      ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this, R.array.ivt_array, android.R.layout.simple_spinner_item);
      arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

      spinner.setAdapter(arrayAdapter);
   }

   private Page getPanel()
   {
      Integer pos = 0;
      Integer xDim = 0;
      Integer yDim = 0;
      Integer viewId = 0;
      String name = getName();
      Integer type = 2; /* Canvas(0), Plot(1), Compass(2) */
      List<Item> items = new ArrayList<>();

      items.add(getCompassVisual());

      Page page = new Page();
      page.setName(name);
      page.setType(type);
      page.setItems(items);

      return page;
   }

   private Item getCompassVisual()
   {
      Integer id = 0;
      Integer xPos = 0;
      Integer yPos = 0;
      String jsFile = "Compass";
      Integer curveId = 0;
      List<Integer> curveIds = new ArrayList<>();

      Item item = new Item();
      //set object variables here

      return item;
   }

   private String getName()
   {
      EditText editText = (EditText)findViewById(R.id.compass_name);

      return editText.getText().toString();
   }

   private void initializeFields()
   {
      if(itemToModify != null)
      {
         EditText editText = (EditText)findViewById(R.id.compass_name);

//         if(!createFromTemplate)
//         {
//            editText.setText(itemToModify.getName());
//         }

         //TODO: SET OTHER FIELDS HERE
      }
   }
}
