package cactig.drillalert.views.manage;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.R;
import resources.CheckboxListItem;
import resources.adapter.CheckboxArrayAdapter;

/**
 * Created by cjohnson on 2/11/15.
 */
public class DashboardAdapter extends CheckboxArrayAdapter<Dashboard> {
   private static final String TAG = "ViewsAdapter";
   private Context context;
   private List<Dashboard> dashboards; // header titles

   // child data in format of header title, child title


   public DashboardAdapter(Context context, List<Dashboard> dashboards) {
      super(context, R.layout.sortable_list_row, getCheckboxList(dashboards));

      this.context = context;
      this.dashboards = new ArrayList<Dashboard>();

      if(dashboards == null)
      {
         dashboards = new ArrayList<>();
      }

      for (Dashboard dashboard : dashboards) {
         if (dashboard != null) {
            this.dashboards.add(dashboard);
         }
      }
   }

   @Override
   public boolean hasStableIds() {
      return false;
   }

   public void setValueAt(int groupPosition, int childPosition, Page page)
   {
      dashboards.get(groupPosition).getPages().set(childPosition, page);
      notifyDataSetChanged();
   }

   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
      View row = convertView;
      StringHolder holder = null;

      if (row == null) {
         LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
         row = inflater.inflate(getLayoutResourceId(), parent, false);

         holder = new StringHolder();
         holder.txtTitle = (TextView) row.findViewById(R.id.sortable_row_text);

         row.setTag(holder);
      } else {
         holder = (StringHolder) row.getTag();
      }

      Dashboard dashboard = getAllData().get(position).getItem();

      //The user went back and we dont want to add this
      if(dashboard == null)
      {
         getAllData().remove(position);
         notifyDataSetChanged();
      }
      if(dashboard != null) {
         holder.txtTitle.setText(dashboard.toString());
      }
      if (isSelected(position)) {
         TypedValue typedValue = new TypedValue();
         getContext().getTheme().resolveAttribute(android.R.attr.activatedBackgroundIndicator, typedValue, true);
         row.setBackgroundResource(typedValue.resourceId);
      }

      return row;
   }

   private static List<CheckboxListItem<Dashboard>> getCheckboxList(List<Dashboard> dashboards)
   {
      ArrayList<CheckboxListItem<Dashboard>> checkboxListItemList = new ArrayList<>();

      if(dashboards != null) {
         for (Dashboard dashboard : dashboards) {
            checkboxListItemList.add(new CheckboxListItem<Dashboard>(dashboard, false, true));
         }
      }

      return checkboxListItemList;
   }

   private static class StringHolder
   {
      TextView txtTitle;
   }
}
