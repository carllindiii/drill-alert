package cactig.drillalert.views.manage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.backend.service.DashboardService;
import cactig.drillalert.R;
import resources.CheckboxListItem;
import resources.NameGenerator;
import resources.adapter.utility.CheckboxPopupUtility;
import resources.adapter.utility.IntentKeyManager;

public class ManageDashboardsActivity extends ActionBarActivity implements UIRunnable<Dashboard>, AdapterView.OnItemClickListener{
   private String visualKey = IntentKeyManager.getSelectedVisualKey();
   private String wellboreIdKey = IntentKeyManager.getWellboreIdKey();

   private String panelKey = IntentKeyManager.getPanelKey();
   private String dashboardsKey = IntentKeyManager.getDashboardsKey();
   private String createFromExistingKey = IntentKeyManager.getUseAsTemplateKey();
   private int editGroupIndex = Integer.MIN_VALUE;
   private int editGroupChildIndex = Integer.MIN_VALUE;

   private ListView mListView;

   private static final int PANEL_REQUEST = 1;
   private static final String TAG = "ManageViews";

   private String wellboreId = null;

   private static final int CANVAS = 0;
   private static final int PLOT = 1;
   private static final int COMPASS = 2;

   private DashboardAdapter dashboardAdapter;

   private Dashboard temporaryHolder;

   private List<Dashboard> originalDashboards;

   private NameGenerator nameGenerator = NameGenerator.getInstance();

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      setContentView(R.layout.activity_manage_dashboards);
      dashboardAdapter = new DashboardAdapter(this, new ArrayList());
      mListView = (ListView) findViewById(R.id.dashboard_list_view);

      setTitle("Manage Dashboards");

      Intent intent = getIntent();

      if(intent != null)
      {
         wellboreId = intent.getStringExtra(wellboreIdKey);
         (new DashboardService(this)).collectDashboards(wellboreId);
      }

      mListView.setOnItemClickListener(this);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_manage_dashboards, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      switch (id) {
         case R.id.save:
            List<Dashboard> dashboards = dashboardAdapter.getAllItems();
            List<Dashboard> removedDashboards = getRemovedDashboards();

            // Delete the removed dashboards from the backend
            for(Dashboard removeDashboard : removedDashboards) {
               new DashboardService(this).deleteDashboard(removeDashboard);
            }

            // creating/updating dashboards is done in ManagePanelsActivity

            finish();
            break;

         case R.id.create_new_dashboard:
            temporaryHolder = null;
            Intent managePanelsIntent = new Intent(this, ManagePanelsActivity.class);
            managePanelsIntent.putExtra(wellboreIdKey, wellboreId);
            startActivityForResult(managePanelsIntent, PANEL_REQUEST);
            break;
         case R.id.use_template:
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
            builderSingle.setTitle("Select One Name:-");
            final ArrayAdapter<Dashboard> arrayAdapter = getTemplateAdapter();

            builderSingle.setNegativeButton("cancel",
                    new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                       }
                    });

            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          Dashboard dashboard = arrayAdapter.getItem(which);
                          List<Dashboard> dashboards = new ArrayList<Dashboard>();
                          dashboards.add(dashboard);

                          dashboard = dashboards.get(0);
                          dashboard.setName(nameGenerator.generateName(dashboard.getName()));
                          dashboard.setId(0);
                          dashboardAdapter.add(new CheckboxListItem<Dashboard>(dashboard, false, false));
                       }
                    });
            builderSingle.show();
            break;
         default:
            break;

      }

      return true;
   }

   @Override
   public void run(List<Dashboard> dashboards) {
      if(dashboards == null)
      {
         dashboards = new ArrayList<>();
      }

      dashboardAdapter = new DashboardAdapter(this, dashboards);

      mListView.setAdapter(dashboardAdapter);
      mListView.setOnItemClickListener(this);

      CheckboxPopupUtility.prepareRemoveListView(mListView, dashboardAdapter);

      if(originalDashboards == null)
      {
         originalDashboards = dashboards;
      }
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      switch (requestCode) {
         case (PANEL_REQUEST): {
            if (resultCode == Activity.RESULT_OK) {
               Object object = data.getSerializableExtra(dashboardsKey);

               if(object == null || !(object instanceof Dashboard))
               {
                  Log.d(TAG, "Unable to retrieve user saved information");
               }
               else
               {
                  Dashboard dashboard = (Dashboard)object;
                  List<Dashboard> newDashboards = new ArrayList<>();

                  newDashboards.add(dashboard);

                  for(Dashboard old : dashboardAdapter.getAllItems())
                  {
                     newDashboards.add(old);
                  }

                  run(newDashboards);
               }
            }
            else
            {
               List<Dashboard> newDashboards = new ArrayList<>();

               newDashboards.add(temporaryHolder);

               for(Dashboard old : dashboardAdapter.getAllItems())
               {
                  newDashboards.add(old);
               }

               run(newDashboards);
            }

            break;
         }
      }
   }

   private void set(List<Dashboard> dashboards, Dashboard dashboard) {
      int existingLocation = -1;

      for(int index = 0; index < dashboards.size(); index++)
      {
         if(dashboard.getName().equals(dashboards.get(index).getName()))
         {
            existingLocation = index;
         }
      }

      if(existingLocation > -1)
      {
         dashboards.set(existingLocation, dashboard);
      }
      else
      {
         dashboards.add(dashboard);
      }
   }

   @Override
   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      if (parent == null || parent.getAdapter() == null) { return; }

      Object object = parent.getItemAtPosition(position);

      if(object == null || !(object instanceof CheckboxListItem)) { return; }

      CheckboxListItem<Dashboard> dashboardCheckboxListItem = (CheckboxListItem<Dashboard>)object;
      Dashboard dashboard = dashboardCheckboxListItem.getItem();
      temporaryHolder = dashboard;
      dashboardAdapter.remove(dashboardCheckboxListItem);

      Intent intent = new Intent(this, ManagePanelsActivity.class);
      intent.putExtra(wellboreIdKey, wellboreId);
      intent.putExtra(dashboardsKey, dashboard);
      startActivityForResult(intent, PANEL_REQUEST);
   }

   private List<Dashboard> getRemovedDashboards()
   {
      List<Dashboard> removed = new ArrayList<>();
      List<Dashboard> dashboards = dashboardAdapter.getAllItems();

      for(Dashboard dashboard : originalDashboards)
      {
         if(!dashboards.contains(dashboard))
         {
            removed.add(dashboard);
         }
      }

      return removed;
   }

   private ArrayAdapter<Dashboard> getTemplateAdapter()
   {
      return new ArrayAdapter<Dashboard>(this, R.layout.sortable_list_row, dashboardAdapter.getAllItems());
   }
}
