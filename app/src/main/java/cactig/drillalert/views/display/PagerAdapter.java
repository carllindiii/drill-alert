package cactig.drillalert.views.display;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

/**
 * Pager adapter class used in the ViewPagerFragment
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
   private ArrayList<Fragment> fragments;
   private ViewPager.OnPageChangeListener changeListener;

   public PagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments, ViewPager.OnPageChangeListener changeListener) {
      super(fm);
      this.fragments = fragments;
      this.changeListener = changeListener;
   }

   @Override
   public Fragment getItem(int position) {
      UrlWebViewFragment fragment = null;
      if(! fragments.isEmpty()) {
         fragment = (UrlWebViewFragment) fragments.get(position);
      }
      return fragment;
   }

   @Override
   public int getCount() {
      return fragments.size();
   }

   /**
    * This override is necessary to be able to fully recreate views on update
    *
    * see StackOverflow: http://stackoverflow.com/questions/10849552/update-viewpager-dynamically
    * @param item
    * @return
    */
   @Override
   public int getItemPosition(Object item) {
      return POSITION_NONE;
   }

   public void update(List<Fragment> newFragments) {
      //once to clear current data
      this.fragments.clear();
      this.fragments.addAll(newFragments);
      this.notifyDataSetChanged();

      changeListener.onPageSelected(0);
   }
}
