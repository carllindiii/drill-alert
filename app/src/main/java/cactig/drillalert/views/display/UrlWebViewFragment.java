package cactig.drillalert.views.display;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewFragment;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cactig.drillalert.backend.model.well.CurvePoint;
import cactig.drillalert.backend.model.well.ItemTypeEnum;
import cactig.drillalert.backend.service.CurvePointsService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.util.DashboardViewCreator;

public class UrlWebViewFragment extends WebViewFragment {
   private static final String TAG = "UrlWebViewFragment";

   private static final int IMMEDIATE_RETRIEVAL_MS = 0;
   private static final int SEQUENTIAL_RETRIEVAL_MS = 10000;

   private static final String VISUAL_ID_TAG = "visualId";
   private static final String WELLBORE_ID_TAG = "wellboreId";
   private static final String ITEM_TYPE_ENUM_TAG = "itemTypeEnum";
   private static final String URL_TAG = "url";
   private static final String CONFIG_JS_TAG = "configJS";
   private static final String UPDATE_JS_TAG = "updateJS";
   private static final String TITLE = "title";

   private Integer visualId;
   private Integer wellboreId;
   private ItemTypeEnum itemType;
   private static String url;
   private String configJS;

   private WebView webView;

   private static String title;

   static int timeValue = 0;

   /**
    *
    * @param visualId
    * @param wellboreId
    * @param itemType
    * @param url
    * @param configJS
    * @return
    */
   public static UrlWebViewFragment newInstance(String title, Integer visualId, Integer wellboreId, ItemTypeEnum itemType, String url, String configJS) {
      UrlWebViewFragment fragment = new UrlWebViewFragment();
      Bundle args = new Bundle();

      args.putInt(VISUAL_ID_TAG, visualId);
      args.putInt(WELLBORE_ID_TAG, wellboreId);
      args.putString(ITEM_TYPE_ENUM_TAG, itemType.toString());
      args.putString(URL_TAG, url);
      args.putString(CONFIG_JS_TAG, configJS);
      args.putString(TITLE, title);

      fragment.setArguments(args);

      return fragment;
   }

   public String getTitle() {
      return title;
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      getActivity().setTitle("Dashboard");

      webView = (WebView) super.onCreateView(inflater, container, savedInstanceState);

      visualId = getArguments().getInt(VISUAL_ID_TAG);
      wellboreId = getArguments().getInt(WELLBORE_ID_TAG);
      itemType = ItemTypeEnum.fromString(getArguments().getString(ITEM_TYPE_ENUM_TAG));
      url = getArguments().getString(URL_TAG);
      configJS = getArguments().getString(CONFIG_JS_TAG);
      title = getArguments().getString(TITLE);

      WebSettings webSettings = webView.getSettings();
      webSettings.setJavaScriptEnabled(true);
      webSettings.setDomStorageEnabled(true);
      setHasOptionsMenu(true);

//      Don't set these!!! They make the scaling all weird
//      webSettings.setLoadWithOverviewMode(false);
//      webSettings.setUseWideViewPort(false);      // need to turn this off for scaling purposes
//      webSettings.setBuiltInZoomControls(true);  // zooming on webview

      return webView;
   }

   @Override
   public void onStart() {
      super.onStart();
      webView.setWebChromeClient(new WebChromeClient());
      webView.setWebViewClient(new WebViewClient() {
         @Override
         public void onPageFinished(WebView view, String url) {
            Timer timer = new Timer();

            //call initial js method
            webView.loadUrl(configJS);

            timer.schedule(new TimerTask() {
               @Override
               public void run() {

                  timeValue++;
                  if(timeValue > 100) {
                     timeValue = 0;
                  }

                  //FIXME: pass actual data
                  new CurvePointsService(getUIRunnable()).collectCurvePoints(0, "0", "1000");
               }
            }, IMMEDIATE_RETRIEVAL_MS, SEQUENTIAL_RETRIEVAL_MS);
         }
      });

      WebSettings webSettings = webView.getSettings();
      webSettings.setJavaScriptEnabled(true);
      webSettings.setDomStorageEnabled(true);

      webView.post(new Runnable() {
         @Override
         public void run() {
            webView.loadUrl(url);
         }
      });
   }

   private UIRunnable<CurvePoint> getUIRunnable() {
      return new UIRunnable<CurvePoint>() {
         @Override
         public void run(List<CurvePoint> curvePoints) {

            if(curvePoints != null) {
               for(CurvePoint curvePoint : curvePoints) {
                  for (CurvePoint.Data data : curvePoint.getData()) {
                     String updateJS = DashboardViewCreator.createUpdateJS(itemType, data);

                     if (webView != null && updateJS != null) {
                        webView.loadUrl(updateJS);
                        Log.d(TAG, data.toString());
                     }
                     else {
                        Log.d(TAG, "webview or update JS doesn't exist");
                     }
                  }
               }
            }
            else {
               Log.d(TAG, "No curve points found");
            }
         }
      };
   }

}