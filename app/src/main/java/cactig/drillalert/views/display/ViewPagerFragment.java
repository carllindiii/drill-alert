package cactig.drillalert.views.display;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.R;
import cactig.drillalert.backend.model.ListHolder;
import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.service.DashboardService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.util.DashboardViewCreator;
import cactig.drillalert.views.manage.ManageDashboardsActivity;
import cactig.drillalert.views.manage.SelectViewsActivity;
import resources.adapter.utility.IntentKeyManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewPagerFragment extends Fragment implements UIRunnable<Dashboard> {
   private static final String TAG = "VIEW_PAGER_FRAGMENT";

   private PagerAdapter pagerAdapter;
   private ViewPager viewPager;
   private String wellboreId = null;

   private String wellboreIdKey = IntentKeyManager.getWellboreIdKey();
   private String dashboardsKey = IntentKeyManager.getDashboardsKey();

   private List<Dashboard> dashboards;

   private static final int SELECTED_VIEWS_REQUEST = 0;

   private Activity mActivity;

   private ViewPager.OnPageChangeListener changeListener;

   public static ViewPagerFragment newInstance() {
      return new ViewPagerFragment();
   }


   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
      setHasOptionsMenu(true);

      // Inflate the layout for this fragment
      ArrayList<Fragment> initialFragments = new ArrayList<>();

      changeListener = new ViewPager.OnPageChangeListener() {
         @Override
         public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //ignored
         }

         @Override
         public void onPageSelected(int position) {
            UrlWebViewFragment fragment = (UrlWebViewFragment) pagerAdapter.getItem(position);
            if(fragment != null) {
               mActivity.setTitle((fragment).getTitle());
            }
         }

         @Override
         public void onPageScrollStateChanged(int state) {

         }
      };

      pagerAdapter = new PagerAdapter(getChildFragmentManager(), initialFragments, changeListener);

      View indicatorView = inflater.inflate(R.layout.indicator, container, false);
      viewPager = (ViewPager) indicatorView.findViewById(R.id.pager);
      viewPager.setAdapter(pagerAdapter);
      CirclePageIndicator circlePageIndicator = (CirclePageIndicator) indicatorView.findViewById(R.id.indicator);
      circlePageIndicator.setViewPager(viewPager);
      circlePageIndicator.setFillColor(Color.BLACK);
      circlePageIndicator.setStrokeColor(Color.BLACK);

      circlePageIndicator.setOnPageChangeListener(changeListener);

      Bundle args = getArguments();
      if (args  != null && args.containsKey(wellboreIdKey)) {
         wellboreId = args.getString(wellboreIdKey);
         new DashboardService(this).collectDashboards(wellboreId);
      }
      else {
         Log.d(TAG, "Unable to retrieve wellbore id");
      }

      return indicatorView;
   }

   @Override
   public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
      menu.clear();
      inflater.inflate(R.menu.menu_view, menu);
      super.onCreateOptionsMenu(menu, inflater);
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // handle_light_blue item selection
      switch (item.getItemId()) {
         case R.id.manage_views_item:
            Intent intent = new Intent(getActivity(), ManageDashboardsActivity.class);
            intent.putExtra(wellboreIdKey, wellboreId);
            startActivity(intent);
            return true;
         case R.id.select_view_item:
            getSelectedDashboards();
         default:
            return super.onOptionsItemSelected(item);
      }
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      switch (requestCode) {
         case (SELECTED_VIEWS_REQUEST): {
            if (resultCode == Activity.RESULT_OK) {
               ListHolder<Dashboard> listHolder = (ListHolder<Dashboard>) data.getSerializableExtra(dashboardsKey);

               if (listHolder == null || listHolder.getList() == null) {
                  Log.d(TAG, "Unable to retrieve selected dashboards");
               } else {
                  dashboards = listHolder.getList();
                  //update the dashboard
                  run(dashboards);
               }
            }
         }
      }
   }

   private void getSelectedDashboards() {
      Intent intent = new Intent(getActivity(), SelectViewsActivity.class);
      ListHolder<Dashboard> listHolder = new ListHolder<>(dashboards);
      intent.putExtra(dashboardsKey, listHolder);
      intent.putExtra(wellboreIdKey, wellboreId);
      startActivityForResult(intent, SELECTED_VIEWS_REQUEST);
   }

   @Override
   public void run(List<Dashboard> dashboards) {
      // Get the screen dimensions

      Display display = mActivity.getWindowManager().getDefaultDisplay();
      Point size = new Point();
      display.getSize(size);
      //too big....so reduce it
      JSDimensions jsDimensions = new JSDimensions(size);

      List<Fragment> fragments = DashboardViewCreator.createFragmentsFromList(
              dashboards,
              jsDimensions.getWidth(),
              jsDimensions.getHeight()
      );

      this.dashboards = dashboards;
      pagerAdapter.update(fragments);

      if(fragments.size() > 0)
      {
         changeListener.onPageSelected(0);
      }
   }

   @Override
   public void onAttach(Activity activity) {
      super.onAttach(activity);
      mActivity = activity;
   }

   /**
    * The screen sizes aren't matching the JS.
    * This does a manual sizing here to adjust for differences
    */
   public class JSDimensions {
      private Point size;

      public JSDimensions(Point size) {
         this.size = size;
      }

      public int getWidth() {
         // 20 for margins
         return (size.x);
      }

      public int getHeight() {
         return size.y;
      }
   }
}
