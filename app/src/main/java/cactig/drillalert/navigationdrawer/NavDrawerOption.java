package cactig.drillalert.navigationdrawer;

/**
 * Created by cjohnson on 12/21/14.
 */
public enum NavDrawerOption {
   NOTIFICATIONS, WELLS, LOGOUT;

   public static String[] getNames() {
      String[] names = new String[values().length];
      NavDrawerOption[] values = values();

      for (int nameIndex = 0; nameIndex < values.length; nameIndex++) {
         NavDrawerOption navDrawerOption = values[nameIndex];
         String name = navDrawerOption.name().toLowerCase();
         name = capitalizeFirstLetter(name);
         names[nameIndex] = name;
      }

      return names;
   }

   private static String capitalizeFirstLetter(String line) {
      return Character.toUpperCase(line.charAt(0)) + line.substring(1);
   }
}
