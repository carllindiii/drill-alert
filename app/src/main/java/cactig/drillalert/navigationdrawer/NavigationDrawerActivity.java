/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cactig.drillalert.navigationdrawer;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import cactig.drillalert.LoginActivity;
import cactig.drillalert.backend.LoginWebViewClient;
import cactig.drillalert.R;
import cactig.drillalert.alerts.AlertsHistoryFragment;
import cactig.drillalert.views.display.ViewPagerFragment;
import cactig.drillalert.wells.WellsFragment;
import resources.adapter.utility.IntentKeyManager;

public class NavigationDrawerActivity extends ActionBarActivity implements NavigationDrawerAdapter.OnItemClickListener {
   public static final String CURRENT_STATE_KEY = "ActionBarActivityCurrentStateKey";

   private DrawerLayout mDrawerLayout;
   private RecyclerView mDrawerList;
   private ActionBarDrawerToggle mDrawerToggle;

   private CharSequence mDrawerTitle;
   private CharSequence mTitle;

   private WellsFragment wellsFragment = WellsFragment.newInstance();
   private ViewPagerFragment viewsFragment = ViewPagerFragment.newInstance();
   private AlertsHistoryFragment alertsHistoryFragment = AlertsHistoryFragment.newInstance();

   private LoginWebViewClient loginWebViewClient = LoginWebViewClient.getInstance();

   private NavDrawerFragmentType selectedType = null;

   private DataFragment retainedFragment = null;

   private String wellboreId = null;

   private String wellboreIdKey = IntentKeyManager.getWellboreIdKey();


   @Override
   protected void onCreate(Bundle savedInstanceState) {
      getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_navigation_drawer);
      mTitle = mDrawerTitle = getTitle();
      mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
      mDrawerList = (RecyclerView) findViewById(R.id.left_drawer);
      mDrawerList.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

      // set a custom shadow that overlays the main content when the drawer opens
      mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
      // improve performance by indicating the list if fixed size.
      mDrawerList.setHasFixedSize(true);
      mDrawerList.setLayoutManager(new LinearLayoutManager(this));

      // set up the drawer's list view with items and click listener
      ItemData[] itemData = new ItemData[3];
      itemData[0] = new ItemData("Alerts", R.drawable.notification);
      itemData[1] = new ItemData("Wells", R.drawable.oil_well);
      itemData[2] = new ItemData("Logout", R.drawable.logout);
      mDrawerList.setAdapter(new NavigationDrawerAdapter(itemData, this));

      // enable ActionBar app icon to behave as action to toggle nav drawer
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setHomeButtonEnabled(true);

      // ActionBarDrawerToggle ties together the the proper interactions
      // between the sliding drawer and the action bar app icon
      mDrawerToggle = new ActionBarDrawerToggle(
              this,                  /* host Activity */
              mDrawerLayout,         /* DrawerLayout object */
              R.string.drawer_open,  /* "open drawer" description for accessibility */
              R.string.drawer_close  /* "close drawer" description for accessibility */
      ) {
         public void onDrawerClosed(View view) {
            invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
         }

         public void onDrawerOpened(View drawerView) {
            invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
         }
      };
      mDrawerLayout.setDrawerListener(mDrawerToggle);

      Bundle extras = getIntent().getExtras();

      retainedFragment = (DataFragment)getFragmentManager().findFragmentByTag("data");

      if(retainedFragment != null)
      {
         selectedType = retainedFragment.getNavDrawerFragmentType();
         wellboreId = retainedFragment.getWellboreId();

         Bundle bundle = new Bundle();
         bundle.putSerializable(wellboreIdKey, wellboreId);
         viewsFragment.setArguments(bundle);
         transition(viewsFragment, "Dashboard");
      }
      else {

         if(retainedFragment == null)
         {
            retainedFragment = new DataFragment();
            getFragmentManager().beginTransaction().add(retainedFragment, "data").commit();
         }

         if (extras != null) {
            Object directoryScreen = extras.get(CURRENT_STATE_KEY);

            if (directoryScreen != null && directoryScreen instanceof NavDrawerFragmentType) {
               selectItem((NavDrawerFragmentType) directoryScreen);
            }
         } else {
            selectItem(NavDrawerFragmentType.WELL);
         }
         //code to make sure keyboard won't pop up all weird
         this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
      }
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // The action bar home/up action should open or close the drawer.
      // ActionBarDrawerToggle will take care of this.
      if (mDrawerToggle.onOptionsItemSelected(item)) {
         return true;
      }
      return false;
   }


   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_navigation_drawer, menu);
      return true;
   }

   /* The click listener for RecyclerView in the navigation drawer */
   @Override
   public void onClick(View view, int position) {
      selectItem(position);
   }

   public void selectItem(NavDrawerFragmentType navDrawerFragmentType) {
      selectedType = navDrawerFragmentType;
      Fragment newFragment;
      String title = "";
      switch (navDrawerFragmentType) {
         case NOTIFICATION:
            newFragment = alertsHistoryFragment;
            title = "Alerts";
            break;
         case WELL:
            newFragment = wellsFragment;
            title = "Wells";
            break;
         case VIEW:
            newFragment = viewsFragment;
            title = "Views";
            break;
         default:
            newFragment = wellsFragment;
      }

      transition(newFragment, title);
   }

   private void selectItem(int position) {
      // update the main content by replacing fragments
      Fragment newFragment;

      switch (NavDrawerOption.values()[position]) {
         case NOTIFICATIONS:
            selectItem(NavDrawerFragmentType.NOTIFICATION);
            break;
         case WELLS:
            selectItem(NavDrawerFragmentType.WELL);
            break;
         case LOGOUT:
            loginWebViewClient.logout();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            break;
         default:
            selectItem(NavDrawerFragmentType.WELL);
      }
   }

   private void transition(Fragment newFragment, String title) {
      FragmentManager fragmentManager = getFragmentManager();
      FragmentTransaction ft = fragmentManager.beginTransaction();
      ft.replace(R.id.content_frame, newFragment);
      ft.commit();

      // update selected item menu_delete, then close the drawer
      mDrawerLayout.closeDrawer(mDrawerList);
      setTitle(title);
   }

   @Override
   public void setTitle(CharSequence title) {
      mTitle = title;
      SpannableString spannableString = new SpannableString(title);
      spannableString.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, spannableString.length(),
              Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      getSupportActionBar().setTitle(spannableString);
   }

   /**
    * When using the ActionBarDrawerToggle, you must call it during
    * onPostCreate() and onConfigurationChanged()...
    */
   @Override
   protected void onPostCreate(Bundle savedInstanceState) {
      super.onPostCreate(savedInstanceState);
      // Sync the toggle state after onRestoreInstanceState has occurred.
      mDrawerToggle.syncState();
   }

   @Override
   public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);
      // Pass any configuration change to the drawer toggles
      mDrawerToggle.onConfigurationChanged(newConfig);
   }

   public void setSelectedType(NavDrawerFragmentType navDrawerFragmentType)
   {
      this.selectedType = navDrawerFragmentType;
   }

   public void setWellboreId(String wellboreId)
   {
      this.wellboreId = wellboreId;
   }

   @Override
   public void onDestroy()
   {
      super.onDestroy();

      retainedFragment.setNavDrawerFragmentType(selectedType);
      retainedFragment.setWellboreId(wellboreId);
   }
}
