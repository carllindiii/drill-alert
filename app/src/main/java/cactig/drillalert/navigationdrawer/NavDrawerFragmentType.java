package cactig.drillalert.navigationdrawer;

/**
 * Created by cjohnson on 2/2/15.
 */
public enum NavDrawerFragmentType {
   WELL, VIEW, NOTIFICATION
}
