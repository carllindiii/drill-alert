package cactig.drillalert.navigationdrawer;

import android.app.Fragment;
import android.os.Bundle;

/**
 * Created by cjohnson on 5/4/15.
 */
public class DataFragment extends Fragment {
   // data object we want to retain
   private NavDrawerFragmentType navDrawerFragmentType;
   private String wellboreId;

   // this method is only called once for this fragment
   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      // retain this fragment
      setRetainInstance(true);
   }

   public NavDrawerFragmentType getNavDrawerFragmentType() {
      return navDrawerFragmentType;
   }

   public void setNavDrawerFragmentType(NavDrawerFragmentType navDrawerFragmentType) {
      this.navDrawerFragmentType = navDrawerFragmentType;
   }

   public String getWellboreId() {
      return wellboreId;
   }

   public void setWellboreId(String wellboreId) {
      this.wellboreId = wellboreId;
   }
}
