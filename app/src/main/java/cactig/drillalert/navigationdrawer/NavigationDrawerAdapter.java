/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cactig.drillalert.navigationdrawer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import cactig.drillalert.R;

/**
 * Adapter for the planet data used in our drawer menu,
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

   private ItemData[] itemsData;
   private OnItemClickListener mListener;

   public interface OnItemClickListener {
      public void onClick(View view, int position);
   }

   public NavigationDrawerAdapter(ItemData[] itemsData, OnItemClickListener onItemClickListener) {
      this.itemsData = itemsData;
      mListener = onItemClickListener;
   }

   // Create new views (invoked by the layout manager)
   @Override
   public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                  int viewType) {
      // create a new view
      View itemLayoutView = LayoutInflater.from(parent.getContext())
              .inflate(R.layout.drawer_list_item, null);

      // create ViewHolder

      ViewHolder viewHolder = new ViewHolder(itemLayoutView);
      return viewHolder;
   }

   // Replace the contents of a view (invoked by the layout manager)
   @Override
   public void onBindViewHolder(ViewHolder viewHolder, final int position) {

      // - get data from your itemsData at this position
      // - replace the contents of the view with that itemsData

      viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
      viewHolder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            mListener.onClick(view, position);
         }
      });
      viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());


   }

   // inner class to hold a reference to each item of RecyclerView
   public static class ViewHolder extends RecyclerView.ViewHolder {

      public TextView txtViewTitle;
      public ImageView imgViewIcon;

      public ViewHolder(View itemLayoutView) {
         super(itemLayoutView);
         txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.drawer_label);
         imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
      }
   }


   // Return the size of your itemsData (invoked by the layout manager)
   @Override
   public int getItemCount() {
      return itemsData.length;
   }
}
