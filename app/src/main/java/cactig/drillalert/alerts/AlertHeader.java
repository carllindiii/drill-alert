package cactig.drillalert.alerts;


import android.graphics.Color;

/**
 * Created by cjohnson on 2/11/15.
 */
public enum AlertHeader {
   CRITICAL(Color.RED), WARNING(Color.YELLOW), INFORMATIONAL(Color.GREEN), ARCHIVED(Color.CYAN);

   private int color;

   private AlertHeader(int color)
   {
      this.color = color;
   }

   private String getName(String original) {
      AlertHeader[] values = values();

         String name = original.toLowerCase();
         return name = capitalizeFirstLetter(name);
   }

   public int getColor()
   {
      return color;
   }

   @Override
   public String toString()
   {
      return getName(super.toString());
   }

   private static String capitalizeFirstLetter(String line) {
      return Character.toUpperCase(line.charAt(0)) + line.substring(1);
   }
}
