package cactig.drillalert.alerts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cactig.drillalert.backend.model.alert.AlertHistory;
import cactig.drillalert.R;

/**
 * Created by cjohnson on 2/11/15.
 */
public class AlertsHistoryAdapter extends BaseExpandableListAdapter {
   private static final String TAG = "AlertsAdapter";
   private Context context;
   private List<String> notificationHeaders; // header titles
   private Map<String, Integer> headerColors;
   private List<AlertHistory> critical;
   private List<AlertHistory> warning;
   private List<AlertHistory> informational;
   private List<AlertHistory> archived;

   private static final int INFORMATION_LEVEL = 0;
   private static final int WARNING_LEVEL = 1;
   private static final int CRITICAL_LEVEL = 2;

   private Integer Priority; // 0 - information, 1 - warning, 2 - critical

   public AlertsHistoryAdapter(Context context, List<AlertHistory> alertsHistory) {

      critical = new ArrayList<>();
      warning = new ArrayList<>();
      informational = new ArrayList<>();
      archived = new ArrayList<>();

      for(AlertHistory alertHistory : alertsHistory)
      {
         if(alertHistory.getNotificationSeverity() == CRITICAL_LEVEL)
         {
            critical.add(alertHistory);
         }
         else if(alertHistory.getNotificationSeverity() == WARNING_LEVEL)
         {
            warning.add(alertHistory);
         }
         else if(alertHistory.getNotificationSeverity() == INFORMATION_LEVEL)
         {
            informational.add(alertHistory);
         }
      }

      this.context = context;
      this.notificationHeaders = new ArrayList<String>();

      headerColors = new HashMap<>();

      for(AlertHeader alertHeader : AlertHeader.values())
      {
         String headerName = alertHeader.toString();

         notificationHeaders.add(headerName);
         headerColors.put(headerName, alertHeader.getColor());
      }
   }

   @Override
   public Object getChild(int groupPosition, int childPosition) {
      if (groupPosition >= 3 || groupPosition < 0) {
         Log.e(TAG, "Invalid category index access");
         return null;
      }

      List<AlertHistory> notifications = getNotifications(groupPosition);

      if (childPosition >= notifications.size() || childPosition < 0) {
         Log.e(TAG, "Invalid notification index access");
         return null;
      }

      if (notifications == null) {
         return null;
      }

      return notifications.get(childPosition);
   }

   @Override
   public long getChildId(int groupPosition, int childPosition) {
      return childPosition;
   }

   @Override
   public View getChildView(int groupPosition, final int childPosition,
                            boolean isLastChild, View convertView, ViewGroup parent) {

      final String childText = getChild(groupPosition, childPosition).toString();

      if (convertView == null) {
         LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         convertView = infalInflater.inflate(R.layout.list_item, null);
      }

      TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);

      txtListChild.setText(childText);
      return convertView;
   }

   @Override
   public int getChildrenCount(int groupPosition) {
      if (groupPosition >= 3 || groupPosition < 0) {
         Log.e(TAG, "Invalid header index access");
         return 0;
      }

      String header = notificationHeaders.get(groupPosition);
      List<AlertHistory> notifications = getNotifications(groupPosition);

      if (notifications == null) {
         return 0;
      }

      return notifications.size();
   }

   @Override
   public Object getGroup(int groupPosition) {
      return getNotifications(groupPosition).get(groupPosition);
   }

   @Override
   public int getGroupCount() {
      return this.notificationHeaders.size();
   }

   @Override
   public long getGroupId(int groupPosition) {
      return groupPosition;
   }

   @Override
   public View getGroupView(int groupPosition, boolean isExpanded,
                            View convertView, ViewGroup parent) {
      String headerTitle = notificationHeaders.get(groupPosition);

      if (convertView == null) {
         LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         convertView = inflater.inflate(R.layout.list_group, null);
      }

      TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
      lblListHeader.setTypeface(null, Typeface.BOLD);
      lblListHeader.setText(headerTitle);

      String header = notificationHeaders.get(groupPosition);
      lblListHeader.setTextColor(headerColors.get(header));

      return convertView;
   }

   @Override
   public boolean hasStableIds() {
      return false;
   }

   @Override
   public boolean isChildSelectable(int groupPosition, int childPosition) {
      return true;
   }

   private List<AlertHistory> getNotifications(int groupPosition)
   {
      if(groupPosition == 0)
      {
         return critical;
      }
      else if(groupPosition == 1)
      {
         return warning;
      }
      else if(groupPosition == 2)
      {
         return informational;
      }
      else
      {
         return archived;
      }
   }
}
