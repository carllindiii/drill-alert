package cactig.drillalert.alerts;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.LoginWebViewClient;
import cactig.drillalert.backend.model.alert.Alert;
import cactig.drillalert.backend.service.AlertsService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.R;
import resources.adapter.utility.IntentKeyManager;

public class ManageAlertsActivity extends ActionBarActivity implements UIRunnable, ExpandableListView.OnChildClickListener, DialogInterface.OnClickListener {
   private ExpandableListView mListView;
   private Alert alertToEdit = null;
   private boolean createFromExisting = false;

   private String alertKey = IntentKeyManager.getAlertKey();
   private String createFromExistingKey = IntentKeyManager.getUseAsTemplateKey();


   private AlertsAdapter alertsAdapter;
   private ArrayAdapter<Alert> dialogAdapter;

   private static final int ALERT_REQUEST = 0;
   private static final String TAG = "MANAGE_ALERTS_ACTIVITY";

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(getLayoutInflater().inflate(R.layout.fragment_item_list, null));

      mListView = (ExpandableListView) findViewById(R.id.expandable_list_view);
      new AlertsService(this).collectAlerts();
      setTitle("Manage Alerts");
   }


   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_alerts, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();

      switch (id) {
         case R.id.save_alert:
            List<Alert> allAlerts = alertsAdapter.getAllAlerts();
            for(Alert alert : allAlerts) {
               // if id is null, create new alert using POST
               if(alert.getId() == null) {
                  new AlertsService(this).createNewAlert(alert);
               } else {
                  new AlertsService(this).updateAlert(alert);
               }
            }
            finish();
            break;
         case R.id.create_new_alert:
            Intent viewIntent = new Intent(this, AddAlertActivity.class);

            alertToEdit = null;
            startActivityForResult(viewIntent, ALERT_REQUEST);
            break;
         case R.id.create_from_existing_alert:
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);

            builderSingle.setTitle("Select One Name:-");
            builderSingle.setNegativeButton("cancel",
                    new DialogInterface.OnClickListener() {

                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                       }
                    });

            dialogAdapter = getTemplateAdapter();

            builderSingle.setAdapter(dialogAdapter, this);
            builderSingle.show();
            break;
         default:
            break;
      }

      return true;
   }

   @Override
   public void run(List alerts) {
      if(alerts == null)
      {
         alerts = new ArrayList<>();
      }
      alertsAdapter = new AlertsAdapter(this, alerts);

      mListView.setAdapter(alertsAdapter);
      mListView.setOnChildClickListener(this);
   }

   @Override
   public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
      if (parent == null || parent.getAdapter() == null)
      {
         return false;
      }


      ExpandableListAdapter expandableListAdapter = parent.getExpandableListAdapter();
      Object child = expandableListAdapter.getChild(groupPosition, childPosition);

      if(child == null || !(child instanceof Alert))
      {
         return false;
      }

      alertToEdit = (Alert)child;
      Intent intent = new Intent(this, AddAlertActivity.class);

      intent.putExtra(alertKey, alertToEdit);
      createFromExisting = false;
      intent.putExtra(alertKey, alertToEdit);
      startActivityForResult(intent, ALERT_REQUEST);
      return true;
   }

   @Override
   public void onClick(DialogInterface dialog, int which) {
      Alert template = dialogAdapter.getItem(which);
      alertToEdit = null;

      if(template == null)
      {
         Log.d(TAG, "Unable to load information for the selected alert");
      }
      else
      {
         Intent intent = new Intent(this, AddAlertActivity.class);

         createFromExisting = true;
         intent.putExtra(alertKey, template);
         intent.putExtra(createFromExistingKey, createFromExisting);
         startActivityForResult(intent, ALERT_REQUEST);
      }
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      Alert returnedAlert = null;
      switch(requestCode) {
         case (ALERT_REQUEST) : {
            if (resultCode == Activity.RESULT_OK && data != null) {
               returnedAlert = (Alert) data.getSerializableExtra(alertKey);

               if(returnedAlert != null)
               {
                  if(alertsAdapter != null)
                  {
                     if(alertToEdit == null)
                     {
                        alertsAdapter.addAlert(returnedAlert);
                     }
                     else
                     {
                        alertsAdapter.setAlert(alertToEdit, returnedAlert);
                     }
                  }
                  else
                  {
                     Log.d(TAG, "Unable to save returnedAlert");
                  }
               }
            }
            break;
         }
      }
   }

   private ArrayAdapter<Alert> getTemplateAdapter()
   {
      ArrayAdapter<Alert> alertArrayAdapter = new ArrayAdapter<Alert>(this, android.R.layout.simple_list_item_1);
      alertArrayAdapter.addAll(alertsAdapter.getAllAlerts());

      return alertArrayAdapter;
   }
}
