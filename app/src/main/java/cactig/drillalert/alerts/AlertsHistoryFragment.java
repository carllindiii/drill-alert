package cactig.drillalert.alerts;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.LoginWebViewClient;
import cactig.drillalert.backend.model.alert.AlertHistory;
import cactig.drillalert.backend.service.AlertsHistoryService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlertsHistoryFragment extends Fragment implements UIRunnable<AlertHistory>, ExpandableListView.OnChildClickListener {
   private ExpandableListView mListView;

   public AlertsHistoryFragment() {
      // Required empty public constructor
   }

   public static AlertsHistoryFragment newInstance() {
      return new AlertsHistoryFragment();
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.fragment_item_list, container, false);
      mListView = (ExpandableListView) view.findViewById(R.id.expandable_list_view);

      new AlertsHistoryService(this).displayAlertsHistory();
      setHasOptionsMenu(true);
      return view;
   }

   @Override
   public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
      // Inflate the menu; this adds items to the action bar if it is present.
      menuInflater.inflate(R.menu.menu_add_alert, menu);
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();

      switch (id) {
         case R.id.manage_alerts:
            startActivity(new Intent(getActivity(), ManageAlertsActivity.class));
            break;
         default:
            break;
      }

      return true;
   }

   @Override
   public void run(List<AlertHistory> alertHistories) {
      if(alertHistories == null)
      {
         alertHistories = new ArrayList<>();
      }
      AlertsHistoryAdapter alertsHistoryAdapter = new AlertsHistoryAdapter(getActivity(), alertHistories);

      mListView.setAdapter(alertsHistoryAdapter);
      mListView.setOnChildClickListener(this);
   }

   @Override
   public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

      if (parent == null || parent.getAdapter() == null)
      {
         return false;
      }


      ExpandableListAdapter expandableListAdapter = parent.getExpandableListAdapter();
      Object child = expandableListAdapter.getChild(groupPosition, childPosition);

      if(child == null || !(child instanceof AlertHistory))
      {
         return false;
      }

      //TODO: Do something here
//      WellBore wellBore = (WellBore)child;
//      ((NavigationDrawerActivity)context).selectItem(NavDrawerFragmentType.VIEW);

      return true;
   }
}
