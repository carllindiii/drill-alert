package cactig.drillalert.alerts;

/**
 * Created by cjohnson on 3/9/15.
 */
public enum Parameter {
   ANGLE, AZIMUTH, INCLINATION, PRESSURE, TEMPERATURE, VIBRATION
}
