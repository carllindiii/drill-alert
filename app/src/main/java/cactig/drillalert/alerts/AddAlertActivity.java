package cactig.drillalert.alerts;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import cactig.drillalert.backend.model.alert.Alert;
import cactig.drillalert.R;
import cactig.drillalert.backend.model.well.Curve;
import cactig.drillalert.backend.service.CurvesService;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.validation.Validator;
import resources.NameGenerator;
import resources.adapter.utility.IntentKeyManager;

public class AddAlertActivity extends ActionBarActivity implements UIRunnable<Curve>{
   private String alertKey = IntentKeyManager.getAlertKey();
   private String createFromExistingKey = IntentKeyManager.getUseAsTemplateKey();

   private Alert alertToModify = null;
   private static String TAG = "ADD_ALERT_ACTIVITY";
   private boolean createFromExisting = false;

   private static final NameGenerator nameGenerator = NameGenerator.getInstance();

   private boolean modifying;

   private List<Integer> curveIds;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      Intent intent = getIntent();
      setContentView(R.layout.activitiy_add_alert);

      Spinner IVTSpinner = (Spinner)findViewById(R.id.alerts_ivt_spinner);

      EditText nameEditText = (EditText)findViewById(R.id.add_alert_name);
      nameEditText.setText(nameGenerator.generateName("Alert"));


      if(intent == null)
      {
         setTitle("Add an Alert");
         alertToModify = new Alert(null, null, null, null, null, null);
      }
      else {
         alertToModify = (Alert) intent.getSerializableExtra(alertKey);
         createFromExisting = intent.getBooleanExtra(createFromExistingKey, createFromExisting);

         modifying = (alertToModify != null);

         if (alertToModify == null)
         {
            setTitle("Add an Alert");
            alertToModify = new Alert(null, null, null, null, null, null);
         }
         else
         {
            setTitle("Edit an Alert");
            initializeFields(alertToModify);
         }
      }


      initializeIVTSpinner(IVTSpinner);
      new CurvesService(this).collectCurves(null, getIVT());

      addFocusListeners();
   }


   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_save, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle_light_blue clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      switch (id) {
         case R.id.save:
            Intent intent = new Intent();
            intent.putExtra(alertKey, getAlert());
            setResult(Activity.RESULT_OK, intent);
            nameGenerator.addName(alertToModify.getName());
            finish();
            break;
      }

      return true;
   }

   private void initializeIVTSpinner(Spinner spinner) {
      ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this, R.array.ivt_array, android.R.layout.simple_spinner_item);
      arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

      spinner.setAdapter(arrayAdapter);

      spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
         @Override
         public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            new CurvesService(getUIRunnable()).collectCurves(null, getIVT());
         }

         @Override
         public void onNothingSelected(AdapterView<?> parent) {

         }
      });
   }

   private String getIVT()
   {
      Spinner IVTSpinner = (Spinner)findViewById(R.id.alerts_ivt_spinner);
      return (String)IVTSpinner.getSelectedItem();
   }

   private UIRunnable getUIRunnable()
   {
      return this;
   }

   private Alert getAlert()
   {
      EditText nameField = (EditText)findViewById(R.id.add_alert_name);
      RadioButton risingRadioButton = (RadioButton)findViewById(R.id.rising_radio_button);
      Spinner curveIdSpinner = (Spinner)findViewById(R.id.alert_history_parameters);

      String name = nameField.getText().toString();
      Boolean rising = risingRadioButton.isChecked();
      // TODO: We shouldn't be hardcoding this....
      Integer wellBoreId = 1;
      String curveId = ((Curve)curveIdSpinner.getSelectedItem()).getId();
      String priority = getPriority();
      Double threshold = getThreshold();

      alertToModify.setCurveId(curveId);
      alertToModify.setName(name);
      alertToModify.setRising(rising);
      if(alertToModify.getWellBoreId() == null || alertToModify.getWellBoreId() == 0) {
         alertToModify.setWellBoreId(wellBoreId);
      }
      alertToModify.setPriority(priority);
      alertToModify.setThreshold(threshold);
      return alertToModify;
   }

   private String getPriority()
   {
      // 0 - information, 1 - warning, 2 - critical
      RadioButton critical = (RadioButton)findViewById(R.id.critical_radio_button);
      RadioButton warning = (RadioButton)findViewById(R.id.warning_radio_button);
      RadioButton information = (RadioButton)findViewById(R.id.information_radio_button);

      if(critical.isChecked())
      {
         return "Critical";
      }
      else if(warning.isChecked())
      {
         return "Warning";
      }
      else if(information.isChecked())
      {
         return "Information";
      }
      else
      {
         Log.d(TAG, "Unable to determine the priority");
         return "";
      }
   }

   private Double getThreshold()
   {
      EditText thresholdField = (EditText)findViewById(R.id.threshold_field);
      String text = thresholdField.getText().toString();
      Double threshold = 0.0;

      try
      {
         threshold = Double.parseDouble(text);

         if(threshold > Double.MAX_VALUE)
         {
            return Double.MAX_VALUE;
         }
      }
      catch(Exception exception)
      {
         Log.d(TAG, exception.getMessage());
      }

      return threshold;
   }

   private void initializeFields(final Alert alert)
   {
      EditText nameField = (EditText)findViewById(R.id.add_alert_name);
      RadioButton risingRadioButton = (RadioButton)findViewById(R.id.rising_radio_button);
      RadioButton fallingRadioButton = (RadioButton)findViewById(R.id.falling_radio_button);

      RadioButton critical = (RadioButton)findViewById(R.id.critical_radio_button);
      RadioButton warning = (RadioButton)findViewById(R.id.warning_radio_button);
      RadioButton information = (RadioButton)findViewById(R.id.information_radio_button);

      EditText thresholdField = (EditText)findViewById(R.id.threshold_field);

      if(!createFromExisting) {
         nameField.setText(alert.getName());
      }

      risingRadioButton.setChecked(alert.getRising());
      fallingRadioButton.setChecked(!alert.getRising());

      //Critical = 2, Warning = 1, Information = 0
      if(alert.getPriority().equals("Critical"))
      {
         critical.setChecked(true);
      }
      else if(alert.getPriority().equals("Warning"))
      {
         warning.setChecked(true);
      }
      else
      {
         information.setChecked(true);
      }

      if(alert != null) {
         try {
            thresholdField.setText(Double.toString(alert.getThreshold()));
         } catch (NumberFormatException exception) {
            Log.d(TAG, "Unable to parse the threshold");
         }
      }

      new CurvesService(new UIRunnable<Curve>() {
         @Override
         public void run(List<Curve> data) {
            Spinner IVTSpinner = (Spinner)findViewById(R.id.alerts_ivt_spinner);

            for(Curve curve : data)
            {
               if(alert.getCurveId().equals(curve.getId()))
               {
                  IVTSpinner.setSelection(curve.getIvt());
               }
            }
         }
      }).collectCurves(null, getIVT());

   }

   private void addFocusListeners()
   {
      final EditText nameEditText = (EditText)findViewById(R.id.add_alert_name);
      final EditText threshHoldEditText = (EditText)findViewById(R.id.threshold_field);

      nameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
         @Override
         public void onFocusChange(View v, boolean hasFocus) {
            if(!modifying && alertToModify.getId() == null && !Validator.isValidName(nameEditText.getText().toString()))
            {
               nameEditText.setText(nameGenerator.generateName("Alert"));
               Toast.makeText(getApplicationContext(), "The \"Name\" must be unique and non-empty", Toast.LENGTH_SHORT).show();
            }
         }
      });

      threshHoldEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
         @Override
         public void onFocusChange(View v, boolean hasFocus) {
            if(!Validator.isValidDouble(threshHoldEditText.getText().toString()))
            {
               threshHoldEditText.setText("0");
               Toast.makeText(getApplicationContext(), "The \"Threshold\" must be non-empty", Toast.LENGTH_SHORT).show();
            }
         }
      });
   }

   @Override
   public void run(List<Curve> data) {
      Spinner spinner = (Spinner)findViewById(R.id.alert_history_parameters);
      ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, data);
      arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      spinner.setAdapter(arrayAdapter);

      int index = 0;
      while(index < data.size() && !(data.get(index).getId().equals(alertToModify.getCurveId())))
      {
         index++;
      }

      if(index < data.size())
      {
         spinner.setSelection(index);
      }
   }
}
