package cactig.drillalert.alerts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cactig.drillalert.backend.model.alert.Alert;
import cactig.drillalert.R;

/**
 * Created by cjohnson on 2/11/15.
 */
public class AlertsAdapter extends BaseExpandableListAdapter {
   private static final String TAG = "AlertsAdapter";
   private Context context;
   private List<String> notificationHeaders; // header titles
   private Map<String, Integer> headerColors;
   private List<Alert> critical;
   private List<Alert> warning;
   private List<Alert> informational;
   private List<Alert> archived;

   private static final String INFORMATION_LEVEL = "Information";
   private static final String WARNING_LEVEL = "Warning";
   private static final String CRITICAL_LEVEL = "Critical";

   private Integer Priority; // 0 - information, 1 - warning, 2 - critical

   public AlertsAdapter(Context context, List<Alert> alerts) {

      critical = new ArrayList<>();
      warning = new ArrayList<>();
      informational = new ArrayList<>();
      archived = new ArrayList<>();

      if(alerts == null)
      {
         alerts = new ArrayList<>();
      }

      alerts.removeAll(Collections.singleton(null));

      for(Alert alert : alerts)
      {
         if(alert.getPriority() == null)
         {
            continue;
         }
         if(alert.getPriority().equals(CRITICAL_LEVEL))
         {
            critical.add(alert);
         }
         else if(alert.getPriority().equals(WARNING_LEVEL))
         {
            warning.add(alert);
         }
         else if(alert.getPriority().equals(INFORMATION_LEVEL))
         {
            informational.add(alert);
         }
      }

      this.context = context;
      this.notificationHeaders = new ArrayList<String>();

      headerColors = new HashMap<>();

      for(AlertHeader alertHeader : AlertHeader.values())
      {
         String headerName = alertHeader.toString();

         notificationHeaders.add(headerName);
         headerColors.put(headerName, alertHeader.getColor());
      }
   }

   @Override
   public Object getChild(int groupPosition, int childPosition) {
      if (groupPosition >= 3 || groupPosition < 0) {
         Log.e(TAG, "Invalid category index access");
         return null;
      }

      List<Alert> alerts = getAlerts(groupPosition);

      if (childPosition >= alerts.size() || childPosition < 0) {
         Log.e(TAG, "Invalid notification index access");
         return null;
      }

      if (alerts == null) {
         return null;
      }

      return alerts.get(childPosition);
   }

   @Override
   public long getChildId(int groupPosition, int childPosition) {
      return childPosition;
   }

   @Override
   public View getChildView(int groupPosition, final int childPosition,
                            boolean isLastChild, View convertView, ViewGroup parent) {

      final String childText = getChild(groupPosition, childPosition).toString();

      if (convertView == null) {
         LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         convertView = infalInflater.inflate(R.layout.list_item, null);
      }

      TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);

      txtListChild.setText(childText);
      return convertView;
   }

   @Override
   public int getChildrenCount(int groupPosition) {
      if (groupPosition >= 3 || groupPosition < 0) {
         Log.e(TAG, "Invalid header index access");
         return 0;
      }

      String header = notificationHeaders.get(groupPosition);
      List<Alert> alerts = getAlerts(groupPosition);

      if (alerts == null) {
         return 0;
      }

      return alerts.size();
   }

   @Override
   public Object getGroup(int groupPosition) {
      return getAlerts(groupPosition).get(groupPosition);
   }

   @Override
   public int getGroupCount() {
      return this.notificationHeaders.size();
   }

   @Override
   public long getGroupId(int groupPosition) {
      return groupPosition;
   }

   @Override
   public View getGroupView(int groupPosition, boolean isExpanded,
                            View convertView, ViewGroup parent) {
      String headerTitle = notificationHeaders.get(groupPosition);

      if (convertView == null) {
         LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         convertView = inflater.inflate(R.layout.list_group, null);
      }

      TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
      lblListHeader.setTypeface(null, Typeface.BOLD);
      lblListHeader.setText(headerTitle);

      String header = notificationHeaders.get(groupPosition);
      lblListHeader.setTextColor(headerColors.get(header));

      return convertView;
   }

   @Override
   public boolean hasStableIds() {
      return false;
   }

   @Override
   public boolean isChildSelectable(int groupPosition, int childPosition) {
      return true;
   }

   public void addAlert(Alert alert)
   {
      String priority = alert.getPriority();

      if(priority.equals(CRITICAL_LEVEL))
      {
         critical.add(alert);
      }
      else if(priority.equals(WARNING_LEVEL))
      {
         warning.add(alert);
      }
      else if(priority.equals(INFORMATION_LEVEL))
      {
         informational.add(alert);
      }

      notifyDataSetChanged();
   }

   public void setAlert(Alert oldAlert, Alert newAlert)
   {
      if(oldAlert.getPriority().equals(CRITICAL_LEVEL))
      {
         critical.remove(oldAlert);
      }
      else if(oldAlert.getPriority().equals(WARNING_LEVEL))
      {
         warning.remove(oldAlert);
      }
      else if(oldAlert.getPriority().equals(INFORMATION_LEVEL))
      {
         informational.remove(oldAlert);
      }

      if(newAlert.getPriority().equals(CRITICAL_LEVEL))
      {
         critical.add(newAlert);
      }
      else if(newAlert.getPriority().equals(WARNING_LEVEL))
      {
         warning.add(newAlert);
      }
      else if(newAlert.getPriority().equals(INFORMATION_LEVEL))
      {
         informational.add(newAlert);
      }

      notifyDataSetChanged();
   }

   public List<Alert> getAllAlerts()
   {
      ArrayList<Alert> alerts = new ArrayList<>();

      alerts.addAll(critical);
      alerts.addAll(warning);
      alerts.addAll(informational);
      alerts.addAll(archived);

      return alerts;
   }

   private List<Alert> getAlerts(int groupPosition)
   {
      if(groupPosition == 0)
      {
         return critical;
      }
      else if(groupPosition == 1)
      {
         return warning;
      }
      else if(groupPosition == 2)
      {
         return informational;
      }
      else
      {
         return archived;
      }
   }
}
