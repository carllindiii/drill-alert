package cactig.drillalert.wells;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.LoginWebViewClient;
import cactig.drillalert.backend.model.well.Well;
import cactig.drillalert.backend.model.well.WellBore;
import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.backend.service.WellService;
import cactig.drillalert.R;
import cactig.drillalert.navigationdrawer.NavDrawerFragmentType;
import cactig.drillalert.navigationdrawer.NavigationDrawerActivity;
import cactig.drillalert.views.display.ViewPagerFragment;
import resources.adapter.utility.IntentKeyManager;


public class WellsFragment extends Fragment implements UIRunnable<Well>, ExpandableListView.OnChildClickListener {
   private String wellboreIdKey = IntentKeyManager.getWellboreIdKey();

   /**
    * The fragment's ListView/GridView.
    */
   private ExpandableListView mListView;

   public static WellsFragment newInstance() {
      WellsFragment fragment = new WellsFragment();
      Bundle args = new Bundle();
      fragment.setArguments(args);

      return fragment;
   }

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {

      View view = inflater.inflate(R.layout.fragment_item_list, container, false);
      mListView = (ExpandableListView) view.findViewById(R.id.expandable_list_view);
      new WellService(this).collectWells();

      // Set OnItemClickListener so we can be notified on item clicks
//      mListView.setOnItemClickListener(this);
      getActivity().setTitle("Wells");
      return view;
   }

   public void setEmptyText(CharSequence emptyText) {
      View emptyView = mListView.getEmptyView();

      if (emptyView instanceof TextView) {
         ((TextView) emptyView).setText(emptyText);
      }
   }

   @Override
   public void run(List<Well> wells) {
      if(wells == null)
      {
         wells = new ArrayList<>();
      }
      WellsAdapter adapter = new WellsAdapter(getActivity(), wells);

      mListView.setAdapter(adapter);
      mListView.setOnChildClickListener(this);
   }

   @Override
   public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

      if (parent == null || parent.getAdapter() == null)
      {
         return false;
      }


      ExpandableListAdapter expandableListAdapter = parent.getExpandableListAdapter();
      Object child = expandableListAdapter.getChild(groupPosition, childPosition);

      if(child == null || !(child instanceof WellBore))
      {
         return false;
      }

      WellBore wellBore = (WellBore)child;

      Bundle bundle = new Bundle();
      bundle.putString(wellboreIdKey, wellBore.getId());

      ViewPagerFragment viewPagerFragment = new ViewPagerFragment();
      viewPagerFragment.setArguments(bundle);

      NavigationDrawerActivity navigationDrawerActivity = (NavigationDrawerActivity)getActivity();
      navigationDrawerActivity.setSelectedType(NavDrawerFragmentType.VIEW);
      navigationDrawerActivity.setWellboreId(wellBore.getId());
      getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, viewPagerFragment).commit();
//      startActivity(new Intent(getActivity(), ManageViews.class));
      return true;
   }
}
