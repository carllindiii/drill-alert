package cactig.drillalert.wells;


import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.R;
import cactig.drillalert.backend.model.well.Well;
import cactig.drillalert.backend.model.well.WellBore;

/**
 * Wells adapter for displaying list of wells
 */
public class WellsAdapter extends BaseExpandableListAdapter {
   private static final String TAG = "WellsAdapter";
   private Context context;
   private List<Well> wells; // header titles

   // child data in format of header title, child title


   public WellsAdapter(Context context, List<Well> wells) {
      if (wells == null) {
         wells = new ArrayList<>();
      }

      this.context = context;
      this.wells = new ArrayList<Well>();

      for (Well well : wells) {
         if (well != null) {
            this.wells.add(well);
         }
      }
   }

   @Override
   public Object getChild(int groupPosition, int childPosition) {
      if (groupPosition >= wells.size() || groupPosition < 0) {
         Log.e(TAG, "Invalid well index access");
         return null;
      }

      Well well = wells.get(groupPosition);
      List<WellBore> wellbores = well.getWellBores();

      if (childPosition >= wellbores.size() || childPosition < 0) {
         Log.e(TAG, "Invalid wellbore index access");
         return null;
      }

      if (wellbores == null) {
         return null;
      }

      return wellbores.get(childPosition);
   }

   @Override
   public long getChildId(int groupPosition, int childPosition) {
      return childPosition;
   }

   @Override
   public View getChildView(int groupPosition, final int childPosition,
                            boolean isLastChild, View convertView, ViewGroup parent) {

      final String childText = getChild(groupPosition, childPosition).toString();

      if (convertView == null) {
         LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         convertView = infalInflater.inflate(R.layout.list_item, null);
      }

      TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);

      txtListChild.setText(childText);
      return convertView;
   }

   @Override
   public int getChildrenCount(int groupPosition) {
      if (groupPosition >= wells.size() || groupPosition < 0) {
         Log.e(TAG, "Invalid well index access");
         return 0;
      }

      List<WellBore> wellBores = wells.get(groupPosition).getWellBores();

      if (wellBores == null) {
         return 0;
      }

      return wellBores.size();
   }

   @Override
   public Object getGroup(int groupPosition) {
      return this.wells.get(groupPosition);
   }

   @Override
   public int getGroupCount() {
      return this.wells.size();
   }

   @Override
   public long getGroupId(int groupPosition) {
      return groupPosition;
   }

   @Override
   public View getGroupView(int groupPosition, boolean isExpanded,
                            View convertView, ViewGroup parent) {
      String headerTitle = getGroup(groupPosition).toString();
      if (convertView == null) {
         LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         convertView = inflater.inflate(R.layout.list_group, null);
      }

      TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
      lblListHeader.setTypeface(null, Typeface.BOLD);
      lblListHeader.setText(headerTitle);

      return convertView;
   }

   @Override
   public boolean hasStableIds() {
      return false;
   }

   @Override
   public boolean isChildSelectable(int groupPosition, int childPosition) {
      return true;
   }
}
