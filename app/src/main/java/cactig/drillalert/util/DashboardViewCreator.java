package cactig.drillalert.util;

import android.app.Fragment;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.backend.model.dashboard.ItemSettings;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.model.well.CurvePoint;
import cactig.drillalert.backend.model.well.ItemTypeEnum;
import cactig.drillalert.views.display.UrlWebViewFragment;

/**
 * A utility to create UrlWebViewFragments of graphs
 */

//TODO: Android does not scale their webviews properly so we manually set bounds for the gauges and plots
public class DashboardViewCreator {
   private static final String TAG = "Dashboard View Creator";

   private static final int GAUGE_TRANSITION_MS = 4000;

   //TODO: hardcode id of 0 because each panel only has one visual for now
   private static final int defaultId = 0;

   /**
    * Given a list of dashboards and the device's width and height, create a list of fragments containing UrlWebViewFragments
    * @param dashboards
    * @param width
    * @param height
    * @return
    */
   public static ArrayList<Fragment> createFragmentsFromList(List<Dashboard> dashboards, int width, int height) {
      ArrayList<Fragment> fragments = new ArrayList<>();
      final int ringWidth = 300 / 5;

      // currently displays all dashboards given
      for(Dashboard dashboard : dashboards) {
         for (Page page : dashboard.getPages()) {

            if(! page.getItems().isEmpty()) {
               Item item = page.getItems().get(0);

               //Get the first item settings to display for the item
               List<ItemSettings> itemSettingsList = item.getItemSettings();
               if(itemSettingsList != null && !itemSettingsList.isEmpty()) {
//                  maxValueCurvePoint = itemSettingsList.get(0).getEndRange();
               }

               if (page.getType() == ItemTypeEnum.Canvas.getValue()) {
                  fragments.add(createGauge(page.getName(), 300, 500, 300, defaultId, ringWidth, 100, GAUGE_TRANSITION_MS));
               } else if (page.getType() == ItemTypeEnum.Plot.getValue()) {
                  fragments.add(createPlot(page.getName(), 500, 300, defaultId, 100, 100, null, null));
               } else if (page.getType() == ItemTypeEnum.Compass.getValue()) {
                  // Do nothing because compasses don't exist yet :(
                  Log.d(TAG, "Compasses don't exist yet :(");
               }
            }
         }
      }

      return fragments;
   }


   /**
    * Create Plot UrlWebViewFragment from the given parameters.
    * First manually serializes js, then creates webView.
    *
    * According to JS documentation:
    * @param width the width of the graph visualization
    * @param height the height of the graph visualization
    * @param id the identifier of the plot (should be unique)
    * @param yMax the y domain maximum
    * @param xMax the x domain maximum
    * @param data an array of values to draw on the graph immediately after initialization (Curves)
    * @param idata an array of values that corresponds with the data array
    * @return URLWebViewFragment containing a plot with the specified parameters
    */
   public static UrlWebViewFragment createPlot(String title, int width, int height, int id, int yMax, int xMax, List<Integer> data, List<Integer> idata) {
      String config = "{" +
                       "title : " + "\"" + title + "\"" + "," +
                       "titleSize : " + 20 + "," +
                       "units : " + "\"units\"" + "," +
                       "yMax : " + yMax + ", " +
                       "xMax : " + xMax + ", " +
                       "data : " + listToDataString(data) + ", " +
                       "idata: " + listToDataString(idata) + ", " +
                       "width : " + width + ", " +
                       "height : " + height + ", " +
                       "id : " + id + "}";

      Log.d(TAG, "plot config: " + config);

      // we need to pass id and
      return UrlWebViewFragment.newInstance(title, id, id, ItemTypeEnum.Plot,
              "file:///android_asset/index.html",
              "javascript: master.init(" + config + ")");
   }

   /**
    * Create Gauge UrlWebViewFragment from the given parameters.
    * First manually serializes js, then creates webView.
    * @param width the width of the div containing the gauge in px
    * @param height the height of the div containing the gauge in px
    * @param size the diameter of the whole gauge in pixels
    * @param id the identifier of the gauge(should be unique)
    * @param ringWidth how thick(wide) the color spectrum part of the gauge (px)
    * @param maxValue the maximum value the gauge can reach
    * @param transitionMs the time it takes to transition to the new value in ms
    * @return
    */
   public static UrlWebViewFragment createGauge(String title, int width, int height, int size, int id, int ringWidth, int maxValue, int transitionMs) {
      String config = "{size : " + size + ", " +
                       "clipWidth : " + width + ", " +
                       "clipHeight : " + height + ", " +
                       "ringWidth: " + ringWidth + ", " +
                       "maxValue : " + maxValue + ", " +
                       "transitionMs : " + transitionMs + ", " +
                       "id : " + id + "}";

      Log.d(TAG, "gauge config: " + config);

      return UrlWebViewFragment.newInstance(title, id, id, ItemTypeEnum.Canvas,
              "file:///android_asset/index.html",
              "javascript: masterGauge.init(" + config + ")");
   }

   /**
    * Create JS acceptable list string
    * @param dataList
    * @return
    */
   private static String listToDataString(List<Integer> dataList) {
      String dataString = "[";

      // must add a single value so plots don't seize up
      if(dataList == null) {
         dataList = new ArrayList<>();
         dataList.add(0);
      }

      for(Integer point : dataList) {

         dataString += point.toString() + ", ";

      }

         dataString = dataString.substring(0, dataString.length() - 2);

      dataString += "]";

      return dataString;
   }


   /**
    * Creates the update JS for
    * @param itemType
    * @param data
    * @return
    */
   public static String createUpdateJS(ItemTypeEnum itemType, CurvePoint.Data data) {
      String updateJS = "javascript: ";

      if(data == null) {
         throw new IllegalArgumentException("The curve point must not be null to create the update JS");
      }

         switch (itemType) {
            case Canvas:
               // (datapoint, id)
               updateJS += "masterGauge.update(" + data.getValue() + ", " +
                       defaultId + ");";
               break;
            case Plot:
               // (xDataPoint, yDataPoint, id)
               updateJS += "master.tick(" + data.getValue() + ", " +
                       data.getIv() + ", " +
                       defaultId + ");";
               break;
            case Compass:
               // Do nothing because compasses don't exist yet :(
               Log.d(TAG, "Compasses don't exist yet :(");
               break;
            default:
               Log.d(TAG, "Incompatible ItemType to create updateJS");
         }

//      Log.d(TAG, updateJS);
      return updateJS;
   }



   /**
    * Testing Purposes only - Example Plot
    * @param width
    * @param height
    * @return
    */
   public static UrlWebViewFragment createExamplePlot(int width, int height) {
      List<Integer> data = Arrays.asList(1, 2, 3, 4, 5);
      List<Integer> idata = Arrays.asList(0, 1, 2, 3, 4);

      return createPlot("fake", width, height, 0, 10, 10, data, idata);
   }

   /**
    * Testing Purposes only - Example Gauge
    * @param width
    * @param height
    * @return
    */
   public static UrlWebViewFragment createExampleGauge(int width, int height) {
      return createGauge("fake", width, height, width, 0, width / 5, 10, GAUGE_TRANSITION_MS);
   }
}
