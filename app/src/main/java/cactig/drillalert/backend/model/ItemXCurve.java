package cactig.drillalert.backend.model;

import java.io.Serializable;

/**
 * Created by cjohnson on 5/23/15.
 */
public class ItemXCurve implements Serializable {
   private Integer Id;
   private Integer ItemId;
   private String CurveId;

   public ItemXCurve(Integer id, Integer itemId, String curveId) {
      Id = id;
      ItemId = itemId;
      CurveId = curveId;
   }

   public Integer getId() {
      return Id;
   }

   public void setId(Integer id) {
      Id = id;
   }

   public Integer getItemId() {
      return ItemId;
   }

   public void setItemId(Integer itemId) {
      ItemId = itemId;
   }

   public String getCurveId() {
      return CurveId;
   }

   public void setCurveId(String curveId) {
      CurveId = curveId;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      ItemXCurve that = (ItemXCurve) o;

      if (Id != null ? !Id.equals(that.Id) : that.Id != null) return false;
      if (ItemId != null ? !ItemId.equals(that.ItemId) : that.ItemId != null) return false;
      return !(CurveId != null ? !CurveId.equals(that.CurveId) : that.CurveId != null);

   }

   @Override
   public int hashCode() {
      int result = Id != null ? Id.hashCode() : 0;
      result = 31 * result + (ItemId != null ? ItemId.hashCode() : 0);
      result = 31 * result + (CurveId != null ? CurveId.hashCode() : 0);
      return result;
   }

   @Override
   public String toString() {
      return "ItemXCurves{" +
              "Id=" + Id +
              ", ItemId=" + ItemId +
              ", CurveId='" + CurveId + '\'' +
              '}';
   }
}
