package cactig.drillalert.backend.model.dashboard;

import java.util.ArrayList;
import java.util.List;

import resources.NameGenerator;

/**
 * Item Settings -- Internal version
 */
public class ItemSettingsInternal extends ItemSettings{
   private String name;
   private List<String> curveIds = new ArrayList<>();
   private NameGenerator nameGenerator = NameGenerator.getInstance();

   public ItemSettingsInternal()
   {
      super();
      name = nameGenerator.generateName("Track: " );
   }

   public ItemSettingsInternal(Integer stepSize, Integer startRange, Integer endRange, Integer divisionSize, Integer scaleType, List<String> curveIds) {
      super(stepSize, startRange, endRange, divisionSize, scaleType);

      this.curveIds = curveIds;
   }

   public ItemSettingsInternal(Integer itemId, Integer id, Integer stepSize, Integer startRange, Integer endRange, Integer divisionSize, Integer scaleType, List<String> curveIds) {
      super(itemId, id, stepSize, startRange, endRange, divisionSize, scaleType);

      this.curveIds = curveIds;
   }

   public ItemSettingsInternal(ItemSettings itemSettings, List<String> curveIds)
   {
      super(itemSettings.getItemId(), itemSettings.getId(), itemSettings.getStepSize(), itemSettings.getStartRange(), itemSettings.getEndRange(), itemSettings.getDivisionSize(), itemSettings.getScaleType());
      this.curveIds = curveIds;

      //TODO: CJ - get this from the item.
      this.name = nameGenerator.generateName("Track");
   }

   public List<String> getCurveIds() {
      return curveIds;
   }

   public void setCurveIds(List<String> curveIds) {
      this.curveIds = curveIds;
   }

   @Override
   public String toString() {
      return name;
   }
}
