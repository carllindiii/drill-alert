package cactig.drillalert.backend.model.dashboard;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cjohnson on 5/22/15.
 */
public class ItemSettings implements Serializable{
   Integer ItemId;
   Integer Id;
   Integer StepSize;
   Integer StartRange;
   Integer EndRange;
   Integer DivisionSize;
   Integer ScaleType;

   public ItemSettings() {

   }

   public ItemSettings(Integer stepSize, Integer startRange, Integer endRange, Integer divisionSize, Integer scaleType) {
      StepSize = stepSize;
      StartRange = startRange;
      EndRange = endRange;
      DivisionSize = divisionSize;
      ScaleType = scaleType;
   }

   public ItemSettings(Integer itemId, Integer id, Integer stepSize, Integer startRange, Integer endRange, Integer divisionSize, Integer scaleType) {
      ItemId = itemId;
      Id = id;
      StepSize = stepSize;
      StartRange = startRange;
      EndRange = endRange;
      DivisionSize = divisionSize;
      ScaleType = scaleType;
   }

   public Integer getItemId() {
      return ItemId;
   }

   public void setItemId(Integer itemId) {
      ItemId = itemId;
   }

   public Integer getId() {
      return Id;
   }

   public void setId(Integer id) {
      Id = id;
   }

   public Integer getStepSize() {
      return StepSize;
   }

   public void setStepSize(Integer stepSize) {
      StepSize = stepSize;
   }

   public Integer getStartRange() {
      return StartRange;
   }

   public void setStartRange(Integer startRange) {
      StartRange = startRange;
   }

   public Integer getEndRange() {
      return EndRange;
   }

   public void setEndRange(Integer endRange) {
      EndRange = endRange;
   }

   public Integer getDivisionSize() {
      return DivisionSize;
   }

   public void setDivisionSize(Integer divisionSize) {
      DivisionSize = divisionSize;
   }

   public Integer getScaleType() {
      return ScaleType;
   }

   public void setScaleType(Integer scaleType) {
      ScaleType = scaleType;
   }

   @Override
   public String toString() {
      return "ItemSettings{" +
              "ItemId=" + ItemId +
              ", Id=" + Id +
              ", StepSize=" + StepSize +
              ", StartRange=" + StartRange +
              ", EndRange=" + EndRange +
              ", DivisionSize=" + DivisionSize +
              ", ScaleType=" + ScaleType +
              '}';
   }
}
