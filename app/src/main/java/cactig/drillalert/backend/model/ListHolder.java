package cactig.drillalert.backend.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cjohnson on 3/31/15.
 */
public class ListHolder<T> implements Serializable{
   private List<T> list;

   public ListHolder(List<T> list)
   {
      this.list = list;
   }

   public List<T> getList() {
      return list;
   }

   public void setList(List<T> list) {
      this.list = list;
   }
}
