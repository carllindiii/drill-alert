package cactig.drillalert.backend.model.well;

import java.util.List;

/**
 * CurvePoint
 */

public class CurvePoint {
   private Integer curveId;
   private String wellboreId;
   private List<Data> data;


   public CurvePoint(Integer curveId, String wellboreId, List<Data> data) {
      this.curveId = curveId;
      this.wellboreId = wellboreId;
      this.data = data;
   }

   public Integer getCurveId() {
      return curveId;
   }

   public String getWellboreId() {
      return wellboreId;
   }

   public List<Data> getData() {
      return data;
   }

   public void setCurveId(Integer curveId) {
      this.curveId = curveId;
   }

   public void setWellboreId(String wellboreId) {
      this.wellboreId = wellboreId;
   }

   public void setData(List<Data> data) {
      this.data = data;
   }

   /**
    * Data Class for curvepoint that holds value and iv
    */
   public class Data {
      private Double value;
      private Double iv;

      public Data(Double value, Double iv) {
         this.value = value;
         this.iv = iv;
      }

      public Double getValue() {
         return value;
      }

      public void setValue(Double value) {
         this.value = value;
      }

      public Double getIv() {
         return iv;
      }

      public void setIv(Double iv) {
         this.iv = iv;
      }

      @Override
      public String toString() {
         return "IV: " + getIv() + " Value: " + getValue();
      }
   }
}
