package cactig.drillalert.backend.model.alert;

/**
 * Created by carllindiii on 3/9/15.
 */
public class AlertHistory {
   private String Message;
   private Integer NotificationSeverity; // 0 - information, 1 - warning, 2 - critical
   private String NotificationDate;
   private Integer TriggeringAlert; // ID of the original alert - get from: /alert/{id}
   private Boolean Acknowledged;

   public AlertHistory(String message, Integer notificationSeverity, String notificationDate, Integer triggeringAlert, Boolean acknowledged) {
      Message = message;
      NotificationSeverity = notificationSeverity;
      NotificationDate = notificationDate;
      TriggeringAlert = triggeringAlert;
      Acknowledged = acknowledged;
   }

   public String getMessage() {
      return Message;
   }

   public void setMessage(String message) {
      Message = message;
   }

   public Integer getNotificationSeverity() {
      return NotificationSeverity;
   }

   public void setNotificationSeverity(Integer notificationSeverity) {
      NotificationSeverity = notificationSeverity;
   }

   public String getNotificationDate() {
      return NotificationDate;
   }

   public void setNotificationDate(String notificationDate) {
      NotificationDate = notificationDate;
   }

   public Integer getTriggeringAlert() {
      return TriggeringAlert;
   }

   public void setTriggeringAlert(Integer triggeringAlert) {
      TriggeringAlert = triggeringAlert;
   }

   public Boolean getAcknowledged() {
      return Acknowledged;
   }

   public void setAcknowledged(Boolean acknowledged) {
      Acknowledged = acknowledged;
   }

   @Override
   public String toString() {
      return Message + " ";
   }
}
