package cactig.drillalert.backend.model.well;

/**
 * WellBore class to represent a well bore from the API.
 */
public class WellBore {
   private String Id;
   private String WellId;
   private String Name;

   public WellBore(String id, String wellId, String name) {
      this.Id = id;
      this.WellId = wellId;
      this.Name = name;
   }

   public String getId() {
      return Id;
   }

   public void setId(String id) {
      this.Id = id;
   }

   public String getWellId() {
      return WellId;
   }

   public void setWellId(String wellId) {
      this.WellId = wellId;
   }

   public String getName() {
      return Name;
   }

   public void setName(String name) {
      this.Name = name;
   }

   @Override
   public String toString() {
      return Name;
   }
}
