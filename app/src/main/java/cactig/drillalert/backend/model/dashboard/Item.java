package cactig.drillalert.backend.model.dashboard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Item implements Serializable{
   private String Name;
   private Integer Id;
   private Integer XPos;
   private Integer YPos;
   private String JsFile;
   private Integer PageId;
   private List<ItemSettings> ItemSettings;

   /**
    * Constructor for all fields.
    * @param id
    * @param XPos
    * @param YPos
    * @param jsFile
    * @param pageId
    * @param itemSettings
    */
   public Item(Integer id, Integer XPos, Integer YPos, String jsFile, Integer pageId, List<ItemSettings> itemSettings) {
      Id = id;
      this.XPos = XPos;
      this.YPos = YPos;
      JsFile = jsFile;
      PageId = pageId;
      ItemSettings = itemSettings;
   }

   /**
    * Constructor without ID but all other fields
    * @param XPos
    * @param YPos
    * @param jsFile
    * @param pageId
    * @param itemSettings
    */
   public Item(Integer XPos, Integer YPos, String jsFile, Integer pageId, List<ItemSettings> itemSettings) {
      this.XPos = XPos;
      this.YPos = YPos;
      JsFile = jsFile;
      PageId = pageId;
      ItemSettings = itemSettings;
   }

   public Item() {
   }

   public Integer getId() {
      return Id;
   }

   public void setId(Integer id) {
      Id = id;
   }

   public Integer getXPos() {
      return XPos;
   }

   public void setXPos(Integer XPos) {
      this.XPos = XPos;
   }

   public Integer getYPos() {
      return YPos;
   }

   public void setYPos(Integer YPos) {
      this.YPos = YPos;
   }

   public String getJsFile() {
      return JsFile;
   }

   public void setJsFile(String jsFile) {
      JsFile = jsFile;
   }

   public Integer getPageId() {
      return PageId;
   }

   public void setPageId(Integer pageId) {
      PageId = pageId;
   }

   public List<ItemSettings> getItemSettings() {
      return ItemSettings;
   }

   public void setItemSettings(List<ItemSettings> itemSettings) {
      ItemSettings = itemSettings;
   }

   public String getName() {
      return Name;
   }

   public void setName(String name) {
      Name = name;
   }

   @Override
   public String toString()
   {
      StringBuilder stringBuilder = new StringBuilder();

      if(PageId != null)
      {
         stringBuilder.append(PageId + " ");
      }
      if(JsFile != null)
      {
         stringBuilder.append(JsFile);
      }

      return stringBuilder.toString();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Item item = (Item) o;

      if (Id != null ? !Id.equals(item.Id) : item.Id != null) return false;

      return true;
   }

   @Override
   public int hashCode() {
      return Id != null ? Id.hashCode() : 0;
   }
}
