package cactig.drillalert.backend.model.dashboard;

import android.util.Log;

import java.io.Serializable;
import java.util.List;

public class Page implements Serializable{

   private Integer Id;
   private Integer Pos;
   private Integer XDim;
   private Integer YDim;
   private Integer DashboardId;
   private String Name;
   private Integer Type; /* TYPE: Canvas(0), Track(1), Rose(2) */
   private List<Item> Items;

   /**
    * Constructor including Page ID and all other fields.
    * @param id
    * @param pos
    * @param XDim
    * @param YDim
    * @param dashboardId
    * @param name
    * @param type
    * @param items
    */
   public Page(Integer id, Integer pos, Integer XDim, Integer YDim, Integer dashboardId, String name, Integer type, List<Item> items) {
      Id = id;
      Pos = pos;
      this.XDim = XDim;
      this.YDim = YDim;
      DashboardId = dashboardId;
      Name = name;
      Type = type;
      this.Items = items;
   }

   /**
    * Constructor without Page ID but all other fields.
    * @param pos
    * @param XDim
    * @param YDim
    * @param dashboardId
    * @param name
    * @param type
    * @param items
    */
   public Page(Integer pos, Integer XDim, Integer YDim, Integer dashboardId, String name, Integer type, List<Item> items) {
      Pos = pos;
      this.XDim = XDim;
      this.YDim = YDim;
      DashboardId = dashboardId;
      Name = name;
      Type = type;
      this.Items = items;
   }


   public Page() {
      //TODO: Get this from somewhere...
      Pos = 0;
   }

   public Integer getId() {
      return Id;
   }

   public void setId(Integer id) {
      Id = id;
   }

   public Integer getPos() {
      return Pos;
   }

   public void setPos(Integer pos) {
      Pos = pos;
   }

   public Integer getXDim() {
      return XDim;
   }

   public void setXDim(Integer XDim) {
      this.XDim = XDim;
   }

   public Integer getYDim() {
      return YDim;
   }

   public void setYDim(Integer YDim) {
      this.YDim = YDim;
   }

   public String getName() {
      return Name;
   }

   public void setName(String name) {
      Name = name;
   }

   public Integer getType() {
      return Type;
   }

   public void setType(Integer type) {
      Type = type;
   }

   public List<Item> getItems() {
      return Items;
   }

   public void addItem(Item item)
   {
      if(item != null) {
         Items.add(item);
      }
   }

   public void setItems(List<Item> items)
   {
      this.Items = items;
   }

   public Integer getDashboardId() {
      return DashboardId;
   }

   public void setDashboardId(Integer dashboardId) {
      DashboardId = dashboardId;
   }

   @Override
   public String toString() {
      return Name + ": " + getType(Type);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Page page = (Page) o;

      if (!Id.equals(page.Id)) return false;

      return true;
   }

   @Override
   public int hashCode() {
      return Id.hashCode();
   }

   private String getType(Integer type)
   {
      if(type == null)
      {
         Log.d("PAGE", "Unable to determine the page type");
         return "";
      }
      /* TYPE: Canvas(0), Track(1), Rose(2) */
      if(type == 0)
      {
         return "Canvas";
      }
      else if(type == 1)
      {
         return "Plot";
      }
      else if(type == 2)
      {
         return "Compass";
      }
      else
      {
         Log.e("PAGE", "Unknown type detected");

         return "Plot";
      }
   }
}