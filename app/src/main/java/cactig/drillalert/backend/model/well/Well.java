package cactig.drillalert.backend.model.well;

import java.util.List;

/**
 * Well class to represent a well from the API
 */
public class Well {

   private String Id;
   private String Name;
   private String Location;
   private List<WellBore> WellBores;


   public Well(String id, String name, String location, List<WellBore> wellBores) {
      this.Id = id;
      this.Name = name;
      this.Location = location;
      this.WellBores = wellBores;
   }


   /**
    * Getters and Setters for Id, Name, Location
    */

   public String getId() {
      return Id;
   }

   public void setId(String id) {
      this.Id = id;
   }

   public String getName() {
      return Name;
   }

   public void setName(String name) {
      this.Name = name;
   }

   public String getLocation() {
      return Location;
   }

   public void setLocation(String location) {
      this.Location = location;
   }

   public List<WellBore> getWellBores() {
      return WellBores;
   }

   public void setWellBores(List<WellBore> wellBores) {
      this.WellBores = wellBores;
   }

   @Override
   public String toString() {
      return Name + "\n" + Location;
   }
}
