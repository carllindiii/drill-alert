package cactig.drillalert.backend.model.alert;

import java.io.Serializable;

/**
 * Created by carllindiii on 3/9/15.
 */
public class Alert implements Serializable{
   private Integer Id;
   private Integer UserId;
   private String CurveId;
   private String Name;
   private Boolean Rising;
   private Integer WellBoreId;
   private String Priority; // 0 - information, 1 - warning, 2 - critical
   private Double Threshold;

   public Alert(String curveId, String name, Boolean rising, Integer wellBoreId, String priority, Double threshold) {
      this.Id = null;
      this.CurveId = curveId;
      this.Name = name;
      this.Rising = rising;
      this.WellBoreId = wellBoreId;
      this.Priority = priority;
      this.Threshold = threshold;
   }

   /**
    * Getters and Setters for all
    */

   public Integer getId() {
      return Id;
   }

   public void setId(Integer id) {
      this.Id = id;
   }

   public String getCurveId() {
      return CurveId;
   }

   public void setCurveId(String curveId) {
      this.CurveId = curveId;
   }

   public String getName() {
      return Name;
   }

   public void setName(String name) {
      this.Name = name;
   }

   public Boolean getRising() {
      return Rising;
   }

   public void setRising(Boolean rising) {
      this.Rising = rising;
   }

   public Integer getWellBoreId() {
      return WellBoreId;
   }

   public void setWellBoreId(Integer wellBoreId) {
      this.WellBoreId = wellBoreId;
   }

   public String getPriority() {
      return Priority;
   }

   public void setPriority(String priority) {
      this.Priority = priority;
   }

   public Double getThreshold() {
      return Threshold;
   }

   public void setThreshold(Double threshold) {
      this.Threshold = threshold;
   }

   public Integer getUserId() {
      return UserId;
   }

   public void setUserId(Integer userId) {
      UserId = userId;
   }

   public Boolean isRising() {
      return Rising;
   }

   @Override
   public String toString() {
      return Name;
   }

   @Override
   public boolean equals(Object object) {
      if (this == object) return true;
      if (object == null || getClass() != object.getClass()) return false;

      Alert alert = (Alert) object;

      if (CurveId != null ? !CurveId.equals(alert.CurveId) : alert.CurveId != null) return false;
      if (Id != null ? !Id.equals(alert.Id) : alert.Id != null) return false;
      if (Name != null ? !Name.equals(alert.Name) : alert.Name != null) return false;
      if (Priority != null ? !Priority.equals(alert.Priority) : alert.Priority != null)
         return false;
      if (Rising != null ? !Rising.equals(alert.Rising) : alert.Rising != null) return false;
      if (Threshold != null ? !Threshold.equals(alert.Threshold) : alert.Threshold != null)
         return false;
      if (WellBoreId != null ? !WellBoreId.equals(alert.WellBoreId) : alert.WellBoreId != null)
         return false;

      return true;
   }

   @Override
   public int hashCode() {
      int result = Id != null ? Id.hashCode() : 0;
      result = 31 * result + (CurveId != null ? CurveId.hashCode() : 0);
      result = 31 * result + (Name != null ? Name.hashCode() : 0);
      result = 31 * result + (Rising != null ? Rising.hashCode() : 0);
      result = 31 * result + (WellBoreId != null ? WellBoreId.hashCode() : 0);
      result = 31 * result + (Priority != null ? Priority.hashCode() : 0);
      result = 31 * result + (Threshold != null ? Threshold.hashCode() : 0);
      return result;
   }
}
