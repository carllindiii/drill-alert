package cactig.drillalert.backend.model;

import java.util.List;

import cactig.drillalert.backend.model.well.WellPermission;

public class User {

   Integer id;
   String guid;
   String displayName;
   String email;
   List<WellPermission> wellPermissions;

   public User(Integer id, String guid, String displayName, String email, List<WellPermission> wellPermissions) {
      this.id = id;
      this.guid = guid;
      this.displayName = displayName;
      this.email = email;
      this.wellPermissions = wellPermissions;
   }

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getGuid() {
      return guid;
   }

   public void setGuid(String guid) {
      this.guid = guid;
   }

   public String getDisplayName() {
      return displayName;
   }

   public void setDisplayName(String displayName) {
      this.displayName = displayName;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public List<WellPermission> getWellPermissions() {
      return wellPermissions;
   }

   public void setWellPermissions(List<WellPermission> wellPermissions) {
      this.wellPermissions = wellPermissions;
   }
}
