package cactig.drillalert.backend.model.well;

/**
 * Created by carllindiii on 4/5/15.
 */
public class WellPermission {
   Integer userId;
   Integer wellId;
   Boolean admin;

   public WellPermission(Integer userId, Integer wellId, Boolean admin) {
      this.userId = userId;
      this.wellId = wellId;
      this.admin = admin;
   }

   public Integer getUserId() {
      return userId;
   }

   public void setUserId(Integer userId) {
      this.userId = userId;
   }

   public Integer getWellId() {
      return wellId;
   }

   public void setWellId(Integer wellId) {
      this.wellId = wellId;
   }

   public Boolean isAdmin() {
      return admin;
   }

   public void setAdmin(Boolean admin) {
      this.admin = admin;
   }
}
