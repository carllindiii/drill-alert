package cactig.drillalert.backend.model.well;

import java.io.Serializable;

/**
 * Created by cjohnson on 3/31/15.
 */
public class Curve implements Serializable{
   private String Id;
   private String Name;
   private String Units;
   private String Label;
   private String WellboreId;
   private Integer Ivt;

   public Curve()
   {

   }

   public Curve(String id, String name, String units, String label, String wellboreId, Integer Ivt) {
      Id = id;
      Name = name;
      Units = units;
      this.Label = label;
      this.WellboreId = wellboreId;
      this.Ivt = Ivt;
   }

   public String getId() {
      return Id;
   }

   public void setId(String id) {
      Id = id;
   }

   public String getName() {
      return Name;
   }

   public void setName(String name) {
      Name = name;
   }

   public String getUnits() {
      return Units;
   }

   public void setUnits(String units) {
      Units = units;
   }

   public String getLabel() {
      return Label;
   }

   public void setLabel(String label) {
      Label = label;
   }

   public String getWellboreId() {
      return WellboreId;
   }

   public void setWellboreId(String wellboreId) {
      WellboreId = wellboreId;
   }

   public Integer getIvt() {
      return Ivt;
   }

   public void setIvt(Integer ivt) {
      Ivt = ivt;
   }

   @Override
   public String toString() {
      return getName();
   }
}
