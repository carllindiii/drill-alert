package cactig.drillalert.backend.model.well;

/**
 * Enum to define item type
 */
public enum ItemTypeEnum {
   Canvas (0),
   Plot (1),
   Compass (2);

   private int type;

   ItemTypeEnum(int type) {
      this.type = type;
   }

   @Override
   public String toString() {
      return Integer.toString(type);
   }

   public static ItemTypeEnum fromString(String type) {
      if(type.equals(Canvas.toString())) {
         return ItemTypeEnum.Canvas;
      }
      else if(type.equals(Plot.toString())) {
         return ItemTypeEnum.Plot;
      }
      else if(type.equals(Compass.toString())) {
         return ItemTypeEnum.Compass;
      }
      else return null;
   }

   public int getValue()
   {
      return type;
   }
}
