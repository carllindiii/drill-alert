package cactig.drillalert.backend.model.dashboard;

import java.io.Serializable;
import java.util.List;

/**
 * View object
 *
 * View -> Panel -> Visual
 *
 * Created by carllindiii on 3/3/15.
 */
public class Dashboard implements Serializable{

   private Integer Id;
   private String Name;
   private List<Page> Pages;
   private String WellboreId;
   private Integer UserId;

//   /**
//    * Constructor to fulfill all the fields
//    * @param id
//    * @param name
//    * @param pages
//    * @param wellboreId
//    * @param userId
//    */
//   public Dashboard(Integer id, String name, List<Page> pages, String wellboreId, Integer userId) {
//      Id = id;
//      Name = name;
//      this.Pages = pages;
//      WellboreId = wellboreId;
//      UserId = userId;
//   }
//
//
//   /**
//    * Constructor to fulfill all the necessary fields for updating the dashboard
//    * @param id
//    * @param name
//    * @param pages
//    * @param wellboreId
//    */
//   public Dashboard(Integer id, String name, List<Page> pages, String wellboreId) {
//      Id = id;
//      Name = name;
//      this.Pages = pages;
//      WellboreId = wellboreId;
//      // set the userId to 0 to let the backend handle the logic
//      UserId = 0;
//   }
//
//   /**
//    * Constructor to fulfill only the necessary fields
//    * @param name
//    * @param pages
//    * @param wellboreId
//    */
//   public Dashboard(String name, List<Page> pages, String wellboreId) {
//      Name = name;
//      this.Pages = pages;
//      WellboreId = wellboreId;
//      // set the dashboard id and userId to 0 to let the backend handle the logic
//      Id = 0;
//      UserId = 0;
//   }

   /**
    * Force to use empty constructor and manually set variables
    */
   public Dashboard() {

   }

   public Integer getId() {
      return Id;
   }

   public void setId(Integer id) {
      Id = id;
   }

   public String getName() {
      return Name;
   }

   public void setName(String name) {
      Name = name;
   }

   public List<Page> getPages() {
      return Pages;
   }

   public void setPages(List<Page> pages) {
      this.Pages = pages;
   }

   public String getWellboreId() {
      return WellboreId;
   }

   public void setWellboreId(String wellboreId) {
      WellboreId = wellboreId;
   }

   public Integer getUserId() {
      return UserId;
   }

   public void setUserId(Integer userId) {
      UserId = userId;
   }

   @Override
   public String toString() {
      return Name;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Dashboard dashboard = (Dashboard) o;

      if (Id != null ? !Id.equals(dashboard.Id) : dashboard.Id != null) return false;

      return true;
   }

   @Override
   public int hashCode() {
      return Id != null ? Id.hashCode() : 0;
   }

//   @Override
//   public int describeContents() {
//      return 0;
//   }
//
//   @Override
//   public void writeToParcel(Parcel dest, int flags) {
//      dest.writeInt(Id);
//      dest.writeString(Name);
//      dest.writeTypedList(Panels);
//   }
}
