package cactig.drillalert.backend.parser;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;


/**
 * Parse a well object from a given json string
 */
public class DashboardsParser implements Parser<Dashboard> {
   private static final String TAG = "DashboardsParser";


   private static final DashboardsParser instance = new DashboardsParser();

   private DashboardsParser() {
   }

   public static DashboardsParser getInstance() {
      return instance;
   }


   /**
    * Parses given jsonString using a gson contract.
    *
    * Gson is a Java library that can be used to convert Java Objects into their JSON representation.
    * It can also be used to convert a JSON string to an equivalent Java object.
    * https://code.google.com/p/google-gson/
    *
    * @param jsonString JSON from server
    * @return list of views
    */
   public List<Dashboard> parse(String jsonString) {
      if(jsonString == null)
      {
         return new ArrayList<>();
      }

      Gson gson = new Gson();
      Type listType = new TypeToken<List<Dashboard>>() {}.getType();

      try {
         List<Dashboard> dashboards = gson.fromJson(jsonString, listType);

         Log.d(TAG, "Views: " + dashboards.toString());

         return dashboards;
      }
      catch (Exception exception)
      {
         Log.d(TAG, exception.getMessage());
         return new ArrayList<>();
      }
   }
}