package cactig.drillalert.backend.parser;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.ItemXCurve;

/**
 * Created by cjohnson on 5/23/15.
 */
public class ItemXCurvesParser implements Parser<ItemXCurve> {
   private static final String TAG = "ItemXCurvesParser";


   private static final ItemXCurvesParser instance = new ItemXCurvesParser();

   private ItemXCurvesParser() {
   }

   public static ItemXCurvesParser getInstance() {
      return instance;
   }


   /**
    * Parses given jsonString using a gson contract.
    *
    * Gson is a Java library that can be used to convert Java Objects into their JSON representation.
    * It can also be used to convert a JSON string to an equivalent Java object.
    * https://code.google.com/p/google-gson/
    *
    * @param jsonString JSON from server
    * @return list of ItemXParser
    */
   public List<ItemXCurve> parse(String jsonString) {
      if(jsonString == null)
      {
         return new ArrayList<>();
      }

      Gson gson = new Gson();
      Type listType = new TypeToken<List<ItemXCurve>>() {}.getType();
      List<ItemXCurve> ItemXParser = gson.fromJson(jsonString, listType);

      Log.d(TAG, "ItemXParser: " + ItemXParser.toString());

      return ItemXParser;
   }
}

