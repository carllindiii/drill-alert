package cactig.drillalert.backend.parser;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.well.Curve;
import cactig.drillalert.backend.model.well.CurvePoint;

/**
 * Created by cjohnson on 3/31/15.
 */
public class CurvePointsParser implements Parser<CurvePoint> {
   private static final String TAG = "CurvePointsParser";
   private static final CurvePointsParser instance = new CurvePointsParser();

   private CurvePointsParser() {}

   public static CurvePointsParser getInstance() {
     return instance;
  }

   /**
    * Parses given jsonString using a gson contract.
    *
    * Gson is a Java library that can be used to convert Java Objects into their JSON representation.
    * It can also be used to convert a JSON string to an equivalent Java object.
    * https://code.google.com/p/google-gson/
    *
    * @param jsonString JSON from server
    * @return list of CurvePoints
    */
   public List<CurvePoint> parse(String jsonString) {
      if(jsonString == null)
      {
         return new ArrayList<>();
      }

      Gson gson = new Gson();
      Type objType = new TypeToken<CurvePoint>() {}.getType();
      CurvePoint curvePoint = gson.fromJson(jsonString, objType);

//      Log.d(TAG, "CurvePoint: " + curvePoint.toString());

      ArrayList<CurvePoint> curvePoints = new ArrayList<>();
      curvePoints.add(curvePoint);
      return curvePoints;
   }
}
