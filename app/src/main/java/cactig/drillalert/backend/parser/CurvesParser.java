package cactig.drillalert.backend.parser;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.well.Curve;

/**
 * Created by cjohnson on 3/31/15.
 */
public class CurvesParser implements Parser<Curve> {
   private static final String TAG = "CurvesParser";
   private static final CurvesParser instance = new CurvesParser();

   private CurvesParser() {}

   public static CurvesParser getInstance() {
     return instance;
  }

   /**
    * Parses given jsonString using a gson contract.
    *
    * Gson is a Java library that can be used to convert Java Objects into their JSON representation.
    * It can also be used to convert a JSON string to an equivalent Java object.
    * https://code.google.com/p/google-gson/
    *
    * @param jsonString JSON from server
    * @return list of Curves
    */
   public List<Curve> parse(String jsonString) {
      if(jsonString == null)
      {
         return new ArrayList<>();
      }

      Gson gson = new Gson();
      Type listType = new TypeToken<List<Curve>>() {}.getType();
      List<Curve> Curves = gson.fromJson(jsonString, listType);

      Log.d(TAG, "Curves: " + Curves.toString());

      return Curves;
   }
}
