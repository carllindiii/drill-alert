package cactig.drillalert.backend.parser;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.well.Well;


/**
 * Parse a well object from a given json string
 */
public class WellsParser implements Parser<Well> {
   private static final String TAG = "WellsParser";


   private static final WellsParser instance = new WellsParser();

   private WellsParser() {
   }

   public static WellsParser getInstance() {
      return instance;
   }


   /**
    * Parses given jsonString using a gson contract.
    *
    * Gson is a Java library that can be used to convert Java Objects into their JSON representation.
    * It can also be used to convert a JSON string to an equivalent Java object.
    * https://code.google.com/p/google-gson/
    *
    * @param jsonString JSON from server
    * @return list of wells
    */
   public List<Well> parse(String jsonString) {
      if(jsonString == null)
      {
         return new ArrayList<>();
      }

      Gson gson = new Gson();
      Type listType = new TypeToken<List<Well>>() {}.getType();

      List<Well> wells = gson.fromJson(jsonString, listType);

      Log.d(TAG, "Wells: " + wells.toString());

      return wells;
   }
}
