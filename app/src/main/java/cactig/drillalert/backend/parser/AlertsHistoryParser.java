package cactig.drillalert.backend.parser;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.alert.AlertHistory;


/**
 * Parse a alerts history object from a given json string
 */
public class AlertsHistoryParser implements Parser<AlertHistory> {
   private static final String TAG = "AlertsHistoryParser";


   private static final AlertsHistoryParser instance = new AlertsHistoryParser();

   private AlertsHistoryParser() {
   }

   public static AlertsHistoryParser getInstance() {
      return instance;
   }


   /**
    * Parses given jsonString using a gson contract.
    *
    * Gson is a Java library that can be used to convert Java Objects into their JSON representation.
    * It can also be used to convert a JSON string to an equivalent Java object.
    * https://code.google.com/p/google-gson/
    *
    * @param jsonString JSON from server
    * @return list of alerts history
    */
   public List<AlertHistory> parse(String jsonString) {
      if(jsonString == null)
      {
         return new ArrayList<>();
      }

      Gson gson = new Gson();
      Type listType = new TypeToken<List<AlertHistory>>() {}.getType();
      List<AlertHistory> alertsHistoryList = gson.fromJson(jsonString, listType);

      if(alertsHistoryList == null) {
         Log.d(TAG, "Alerts history is null");
      }
      else {
         Log.d(TAG, "Alerts History: " + alertsHistoryList.toString());
      }

      return alertsHistoryList;
   }
}
