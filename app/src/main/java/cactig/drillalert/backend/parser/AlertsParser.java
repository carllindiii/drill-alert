package cactig.drillalert.backend.parser;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.alert.Alert;


/**
 * Parse a well object from a given json string
 */
public class AlertsParser implements Parser<Alert> {
   private static final String TAG = "AlertsParser";


   private static final AlertsParser instance = new AlertsParser();

   private AlertsParser() {
   }

   public static AlertsParser getInstance() {
      return instance;
   }


   /**
    * Parses given jsonString using a gson contract.
    *
    * Gson is a Java library that can be used to convert Java Objects into their JSON representation.
    * It can also be used to convert a JSON string to an equivalent Java object.
    * https://code.google.com/p/google-gson/
    *
    * @param jsonString JSON from server
    * @return list of alerts
    */
   public List<Alert> parse(String jsonString) {
      if(jsonString == null)
      {
         return new ArrayList<>();
      }

      Gson gson = new Gson();
      Type listType = new TypeToken<List<Alert>>() {}.getType();
      List<Alert> alerts = gson.fromJson(jsonString, listType);

      Log.d(TAG, "Alerts: " + alerts.toString());

      return alerts;
   }
}
