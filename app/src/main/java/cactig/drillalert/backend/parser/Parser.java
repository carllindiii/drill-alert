package cactig.drillalert.backend.parser;

import java.util.List;

public interface Parser<T> {

   public List<T> parse(String jsonString);
}
