package cactig.drillalert.backend.service;

import android.util.Log;

import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.model.dashboard.Item;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.parser.DashboardsParser;
import cactig.drillalert.backend.service.async.GetAsyncService;
import cactig.drillalert.backend.service.async.DeleteAsyncService;
import cactig.drillalert.backend.service.async.PostAsyncService;
import cactig.drillalert.backend.service.async.PutAsyncService;
import resources.NameGenerator;

/**
 * Alert Service
 *
 * Created by clind on 2/9/15
 */
public class DashboardService extends GetAsyncService<Dashboard> {
   private static final String TAG = "DashboardService";
   private static final String END_POINT_URI = "dashboards/";

   public DashboardService(UIRunnable uiRunnable) {
      super(DashboardsParser.getInstance(), uiRunnable);

   }

   public void collectDashboards()
   {
      collectDashboards(null);
   }

   public void collectDashboards(String wellboreId)
   {
      try {
         execute(getDashboardsURI(wellboreId));
      } catch (Exception e) {
         Log.e(TAG, "Failed to fetch Dashboard(s) containing wellboreId:" + wellboreId + "\n" + e.getMessage());
      }
   }

   public void createNewDashboard(Dashboard dashboard) {
      try {
         PostAsyncService postAsyncService = new PostAsyncService(dashboard);
         postAsyncService.execute(END_POINT_URI);
      }
      catch(Exception e){
         Log.e(TAG, "Failed to post Dashboards\n" + e.getMessage());
      }
   }

   public void updateDashboard(Dashboard dashboard) {
      try {
         PutAsyncService putAsyncService = new PutAsyncService(dashboard);
         putAsyncService.execute(END_POINT_URI + dashboard.getId());
      }
      catch(Exception e){
         Log.e(TAG, "Failed to update Dashboard: " + dashboard.getId() + "\n" + e.getMessage());
      }
   }

   public void deleteDashboard(Dashboard dashboard) {
      try {
         DeleteAsyncService deleteAsyncService = new DeleteAsyncService(dashboard.getId());
         deleteAsyncService.execute(END_POINT_URI);
      }
      catch(Exception e){
         Log.e(TAG, "Failed to delete Dashboard: " + dashboard.toString() + "\n" + e.getMessage());
      }
   }

   public String getDashboardsURI(String wellboreId) {
      String endPointURI = END_POINT_URI;

      if(wellboreId != null) {
         endPointURI += wellboreId;
      }

      return endPointURI;
   }

   @Override
   protected void onPostExecute(String json) {
      if (json == null) {
         Log.e(TAG, "Failed to connect to server");
      }
      else {
         try {
            List<Dashboard> dashboards = parser.parse(json);
            NameGenerator nameGenerator = NameGenerator.getInstance();

            for(Dashboard dashboard : dashboards)
            {
               nameGenerator.addName(dashboard.getName());

               if(dashboard.getPages() != null) {
                  for (Page page : dashboard.getPages()) {
                     nameGenerator.addName(page.getName());

                     if(page.getItems() != null)
                     {
                        //TODO: Items don't have names at the moment...
//                        for (Item item : page.getItems())
//                        {
//                           nameGenerator.addName(item.getName());
//                        }
                     }
                  }
               }
            }

            uiRunnable.run(dashboards);
         } catch (Exception e) {
            Log.e(TAG, "Failed to handle data retrieved from server\n" + Log.getStackTraceString(e));
         }
      }
   }
}
