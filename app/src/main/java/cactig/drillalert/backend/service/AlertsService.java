package cactig.drillalert.backend.service;

import android.util.Log;

import java.util.List;

import cactig.drillalert.backend.model.alert.Alert;
import cactig.drillalert.backend.parser.AlertsParser;
import cactig.drillalert.backend.service.async.GetAsyncService;
import cactig.drillalert.backend.service.async.DeleteAsyncService;
import cactig.drillalert.backend.service.async.PostAsyncService;
import cactig.drillalert.backend.service.async.PutAsyncService;
import resources.NameGenerator;

/**
 * Alert Service
 *
 * Created by clind on 2/9/15
 */
public class AlertsService extends GetAsyncService<Alert> {
   private static final String TAG = "AlertsService";
   private static final String END_POINT_URI = "alerts";

   public AlertsService(UIRunnable uiRunnable) {
      super(AlertsParser.getInstance(), uiRunnable);
   }

   /**
    * Get all the wells with users permissions
    */
   public void collectAlerts() {
      try {
         execute(END_POINT_URI);
      } catch (Exception e) {
         Log.e(TAG, "Failed to fetch Alerts\n" + e.getMessage());
      }
   }

   public void createNewAlert(Alert alert) {
      try {
         PostAsyncService postAsyncService = new PostAsyncService(alert);
         postAsyncService.execute(END_POINT_URI);
      }
      catch(Exception e){
         Log.e(TAG, "Failed to post Alerts\n" + e.getMessage());
      }
   }

   public void updateAlert(Alert alert) {
      try {
         PutAsyncService putAsyncService = new PutAsyncService(alert);
         putAsyncService.execute(END_POINT_URI + "/" + alert.getId());
      }
      catch(Exception e){
         Log.e(TAG, "Failed to post Alerts\n" + e.getMessage());
      }
   }

   public void deleteAlert(Alert alert) {
      try {
         DeleteAsyncService deleteAsyncService = new DeleteAsyncService(alert.getId());
         deleteAsyncService.execute(END_POINT_URI);
      }
      catch(Exception e){
         Log.e(TAG, "Failed to delete alert: " + alert.toString() + "\n" + e.getMessage());
      }
   }

   @Override
   protected void onPostExecute(String json) {
      if (json == null) {
         Log.e(TAG, "Failed to connect to server");
      }
      else {
         try {
            List<Alert> alerts = parser.parse(json);

            for(Alert alert : alerts) {
               NameGenerator.getInstance().addName(alert.getName());
            }

            uiRunnable.run(alerts);
         } catch (Exception e) {
            Log.e(TAG, "Failed to handle data retrieved from server\n" + Log.getStackTraceString(e));
         }
      }
   }
}
