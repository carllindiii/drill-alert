package cactig.drillalert.backend.service.async;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

import cactig.drillalert.backend.LoginWebViewClient;
import cactig.drillalert.backend.parser.Parser;
import cactig.drillalert.backend.service.UIRunnable;

public abstract class GetAsyncService<T> extends AsyncTask<String, Void, String> {
   private static final String TAG = "GetAsyncService";

   private static final int TIMEOUT_MILLISECONDS = 30000; /* milliseconds = 30 seconds */
   private static final String urlString = "https://drillalert.azurewebsites.net/api/";
   protected Parser<T> parser;
   protected LoginWebViewClient loginWebViewClient = LoginWebViewClient.getInstance();
   protected UIRunnable<T> uiRunnable;

   public GetAsyncService(Parser<T> parser, UIRunnable<T> uiRunnable) {
      this.parser = parser;
      this.uiRunnable = uiRunnable;
   }

   @Override
   protected String doInBackground(String... params) {

      String responseBody = null;
      String uriLoc = params[0];

      if(uriLoc == null) {
         Log.d(TAG, "Empty URI Location");
      }
      else {
         try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(urlString + uriLoc);

            String sessionCookie = loginWebViewClient.getSessionCookie();
//            Log.d(TAG, "Setting Cookie: " + sessionCookie);

            // Set the cookie inside the header for all requests
            httpGet.setHeader("Cookie", sessionCookie);

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            responseBody = httpClient.execute(httpGet, responseHandler);
//            Log.d(TAG, "Response body: " + responseBody);
         } catch (Exception e) {
//            Toast.makeText(context, "A problem with our internal servers has occurred. Please try again later.", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Failed to fetch data from server: " +  urlString + uriLoc + "\n" + Log.getStackTraceString(e));
         }
      }

      return responseBody;
   }

   /**
    * Override this method to gather desired data from post execute
    * @param s JSON retrieved from server
    */
   protected abstract void onPostExecute(String s);

}
