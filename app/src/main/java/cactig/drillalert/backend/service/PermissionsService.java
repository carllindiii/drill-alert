package cactig.drillalert.backend.service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import cactig.drillalert.backend.service.async.GetAsyncService;

/**
 * Alert Service
 *
 * Created by clind on 2/9/15
 */
public class PermissionsService extends GetAsyncService<Object> {
   private static final String TAG = "PermissionsService";
   private static final String END_POINT_URI = "permissions/android/";
   final String SENDER_ID = "936255009248"; /* Sender ID for push requests -- Given by backend */

   protected Context context;

   public PermissionsService() {
      super(null, null);
   }

   public void getPermissions(Context context) {
      this.context = context;
      try {
         execute(END_POINT_URI);
      }
      catch(Exception e){
         Log.e(TAG, "Failed to get permissions\n" + e.getMessage());
      }
   }

   /**
    * Logic after the asynchronous task has completed
    * Gives the json from the backend if successful, null otherwise
    * @param json this currently returns a list of wells. We are using the wells/ endpoint for this
    */
   @Override
   protected void onPostExecute(String json) {
      if (json != null) {
         Log.d(TAG, json);
      }
      else {
         Log.e(TAG, "Failed to retrieve data from server\n");
      }
   }

   /**
    * The google cloud messaging must be run asynchronously. Do this before executing the request
    * to the server.
    * @param params the request endpoint
    * @return string of the json retrieved from the backend
    */
   @Override
   protected String doInBackground(String... params) {
      String uriLoc = params[0];

      try {
         GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context.getApplicationContext());
         String registrationId = gcm.register(SENDER_ID);
         uriLoc += registrationId;
         Log.d(TAG, "Unique Registration id: " + registrationId);
      }
      catch(IOException e) {
         Log.e(TAG, "Failed to get permissions from backend:\n" + Log.getStackTraceString(e));
      }

      return super.doInBackground(uriLoc);
   }
}
