package cactig.drillalert.backend.service;

import java.util.List;

/**
 * Created by cjohnson on 3/9/15.
 */
public interface UIRunnable<T> {
   public void run(List<T> data);
}
