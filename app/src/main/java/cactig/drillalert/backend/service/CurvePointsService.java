package cactig.drillalert.backend.service;

import android.util.Log;

import java.util.List;

import cactig.drillalert.backend.model.well.CurvePoint;
import cactig.drillalert.backend.parser.CurvePointsParser;
import cactig.drillalert.backend.service.async.GetAsyncService;

/**
 * Created by cjohnson on 3/31/15.
 */
public class CurvePointsService extends GetAsyncService<CurvePoint> {
   private static final String TAG = "CurvePointsService";
   private static final String END_POINT_URI = "curvepoints/";

   public CurvePointsService(UIRunnable uiRunnable) {
      super(CurvePointsParser.getInstance(), uiRunnable);
   }

   /**
    * Get all the curves with users permissions
    */
   public void collectCurvePoints(Integer curveId, String startIV, String endIV) {
      try {
         execute(END_POINT_URI + curveId + "/" + startIV + "/" + endIV);
      } catch (Exception e) {
         Log.e(TAG, "Failed to fetch CurvePoints\n" + e.getMessage());
      }
   }


   @Override
   protected void onPostExecute(String json) {
      if (json == null) {
         Log.e(TAG, "Failed to connect to server");
      }
      else {
         try {
            List<CurvePoint> curvePoints = parser.parse(json);

            uiRunnable.run(curvePoints);
         } catch (Exception e) {
            Log.e(TAG, "Failed to handle data retrieved from server\n" + Log.getStackTraceString(e));
         }
      }
   }
}
