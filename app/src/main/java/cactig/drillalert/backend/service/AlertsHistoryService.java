package cactig.drillalert.backend.service;

import android.util.Log;

import java.util.List;

import cactig.drillalert.backend.model.alert.AlertHistory;
import cactig.drillalert.backend.parser.AlertsHistoryParser;
import cactig.drillalert.backend.service.async.GetAsyncService;

/**
 * Alert History Service
 *
 * Created by clind on 2/9/15
 */
public class AlertsHistoryService extends GetAsyncService<AlertHistory> {
   private static final String TAG = "AlertsHistoryService";
   private static final String END_POINT_URI = "alertshistory/";

   public AlertsHistoryService(UIRunnable uiRunnable) {
      super(AlertsHistoryParser.getInstance(), uiRunnable);
   }

   /**
    * Get all the alerts history
    */
   public void displayAlertsHistory() {
      try {
         execute(END_POINT_URI);
      } catch (Exception e) {
         Log.e(TAG, "Failed to fetch Alerts History\n" + e.getMessage());
      }
   }

   @Override
   protected void onPostExecute(String json) {
      if (json == null) {
         Log.e(TAG, "Failed to connect to server");
      }
      else {
         try {
            List<AlertHistory> alertsHistory = parser.parse(json);

            uiRunnable.run(alertsHistory);
         } catch (Exception e) {
            Log.e(TAG, "Failed to handle data retrieved from server\n" + Log.getStackTraceString(e));
         }
      }
   }
}
