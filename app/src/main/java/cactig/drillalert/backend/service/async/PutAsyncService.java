package cactig.drillalert.backend.service.async;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

import cactig.drillalert.backend.LoginWebViewClient;

/**
 * PUT -- used for updating
 */
public class PutAsyncService extends AsyncTask<String, Void, String> {
   private static final String TAG = "PutAsyncService";

   private static final int TIMEOUT_MILLISECONDS = 30000; /* milliseconds = 30 seconds */
   private static final String urlString = "https://drillalert.azurewebsites.net/api/";

   private LoginWebViewClient loginWebViewClient = LoginWebViewClient.getInstance();
   private Object object;

   public PutAsyncService(Object object) {
      this.object = object;
   }

   @Override
   protected String doInBackground(String... params) {

      String responseBody = null;
      String uriLoc = params[0];

      if(uriLoc == null) {
         Log.e(TAG, "Empty URI Location");
      }
      else {
         try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPut httpPut = new HttpPut(urlString + uriLoc);

            String sessionCookie = loginWebViewClient.getSessionCookie();
//            Log.d(TAG, "Setting Cookie: " + sessionCookie);

            // Set the cookie inside the header for all requests
            httpPut.setHeader("Cookie", sessionCookie);
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader("Content-Type", "application/json");

            Gson gson = new Gson();
            String serializedList = gson.toJson(object);
            Log.d(TAG, urlString + uriLoc);
            Log.d(TAG, serializedList);

            httpPut.setEntity(new StringEntity(serializedList));

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            responseBody = httpClient.execute(httpPut, responseHandler);
            Log.d(TAG, "Response body: " + responseBody);
         } catch (IOException e) {
            Log.e(TAG, "Failed to post data to server\n" + Log.getStackTraceString(e));
         }
      }

      return responseBody;
   }

   /**
    * Override this method to gather desired data from post execute
    * @param s Response retrieved from server
    */
   protected void onPostExecute(String s) {
      Log.d(TAG, "onPostExecute: " + s);
   }
}
