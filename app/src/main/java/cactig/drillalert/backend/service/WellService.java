package cactig.drillalert.backend.service;

import android.util.Log;

import java.util.List;

import cactig.drillalert.backend.parser.WellsParser;
import cactig.drillalert.backend.service.async.GetAsyncService;
import cactig.drillalert.backend.model.well.Well;

/**
 * Well Service
 *
 * Created by clind on 2/9/15
 */
public class WellService extends GetAsyncService<Well> {
   private static final String TAG = "WellService";
   private static final String END_POINT_URI = "wells/";

   public WellService(UIRunnable uiRunnable) {
      super(WellsParser.getInstance(), uiRunnable);
   }

   /**
    * Get all the wells with users permissions
    */
   public void collectWells() {
      try {
         execute(END_POINT_URI);
      } catch (Exception e) {
         Log.e(TAG, "Failed to fetch Wells\n" + e.getMessage());
      }
   }

   @Override
   protected void onPostExecute(String json) {
      if (json == null) {
         Log.e(TAG, "Failed to connect to server");
      }
      else {
         try {
            List<Well> wells = parser.parse(json);

            uiRunnable.run(wells);
         } catch (Exception e) {
            Log.e(TAG, "Failed to handle data retrieved from server\n" + Log.getStackTraceString(e));
         }
      }
   }

}
