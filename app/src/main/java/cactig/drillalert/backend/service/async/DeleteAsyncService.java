package cactig.drillalert.backend.service.async;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;

import java.io.IOException;

import cactig.drillalert.backend.LoginWebViewClient;

public class DeleteAsyncService extends AsyncTask<String, Void, String> {
   private static final String TAG = "PostAsyncService";

   private static final int TIMEOUT_MILLISECONDS = 30000; /* milliseconds = 30 seconds */
   private static final String urlString = "https://drillalert.azurewebsites.net/api/";

   private LoginWebViewClient loginWebViewClient = LoginWebViewClient.getInstance();
   private int id;

   /**
    * Specify the URI deletion location
    * Usually this will be an ID appended to the URI
    * @param id id of object to delete
    * @return
    */
   public DeleteAsyncService(int id) {
      this.id = id;
   }

   @Override
   protected String doInBackground(String... params) {

      String responseBody = null;
      String uriLoc = params[0];

      if(uriLoc == null) {
         Log.e(TAG, "Empty URI Location");
      }
      else {
         try {
            Log.d("TAG", "Deleting: " + urlString + uriLoc + id);
            HttpClient httpClient = new DefaultHttpClient();
            HttpDelete httpDelete = new HttpDelete(urlString + uriLoc + id);

            String sessionCookie = loginWebViewClient.getSessionCookie();
//            Log.d(TAG, "Setting Cookie: " + sessionCookie);

            // Set the cookie inside the header for all requests
            httpDelete.setHeader("Cookie", sessionCookie);
            httpDelete.setHeader("Accept", "application/json");
//            httpDelete.setHeader("Content-Type", "application/json");

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            responseBody = httpClient.execute(httpDelete, responseHandler);
//            Log.d(TAG, "Response body: " + responseBody);
         } catch (IOException e) {
            Log.e(TAG, "Failed to delete data from the server\n" + Log.getStackTraceString(e));
         }
      }

      return responseBody;
   }

   /**
    * Override this method to gather desired data from post execute
    * @param s Response retrieved from server
    */
   protected void onPostExecute(String s) {
      Log.d(TAG, "onPostExecute: " + s);
   }
}
