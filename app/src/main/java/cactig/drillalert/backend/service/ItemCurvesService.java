package cactig.drillalert.backend.service;

import android.util.Log;

import java.util.List;

import cactig.drillalert.backend.model.ItemXCurve;
import cactig.drillalert.backend.parser.ItemXCurvesParser;
import cactig.drillalert.backend.service.async.GetAsyncService;
import cactig.drillalert.backend.service.async.PutAsyncService;

/**
 * Item Curves Service
 */
public class ItemCurvesService extends GetAsyncService<ItemXCurve> {
   private static final String TAG = "ItemCurveService";
   private static final String END_POINT_URI = "ItemCurves/";

   public ItemCurvesService(UIRunnable uiRunnable) {
      super(ItemXCurvesParser.getInstance(), uiRunnable);
   }

   /**
    * Get all the wells with users permissions
    */
   public void collectItemCurves(Integer itemID) {
      try {
         if(itemID == null) {
            itemID = 1;
         }
         execute(END_POINT_URI + itemID);
      } catch (Exception e) {
         Log.e(TAG, "Failed to fetch ItemXCurves\n" + e.getMessage());
      }
   }

   public void updateItemCurve(ItemXCurve itemXCurve) {
      try {
         PutAsyncService putAsyncService = new PutAsyncService(itemXCurve);
         putAsyncService.execute(END_POINT_URI + itemXCurve.getId());
      }
      catch(Exception e){
         Log.e(TAG, "Failed to update ItemCurve\n" + e.getMessage());
      }
   }

   @Override
   protected void onPostExecute(String json) {
      if (json == null) {
         Log.e(TAG, "Failed to connect to server");
      }
      else {
         try {
            List<ItemXCurve> itemXCurves = parser.parse(json);

            uiRunnable.run(itemXCurves);
         } catch (Exception e) {
            Log.e(TAG, "Failed to handle data retrieved from server\n" + Log.getStackTraceString(e));
         }
      }
   }
}
