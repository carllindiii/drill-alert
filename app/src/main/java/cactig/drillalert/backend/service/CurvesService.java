package cactig.drillalert.backend.service;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cactig.drillalert.backend.model.well.Curve;
import cactig.drillalert.backend.parser.CurvesParser;
import cactig.drillalert.backend.service.async.GetAsyncService;

/**
 * Curves service to retrieve curves from server
 */
public class CurvesService extends GetAsyncService<Curve> {
   private static final String TAG = "CurvesService";
   private static final String END_POINT_URI = "Curves/";

   private static final int TRUE_VERTICAL_DEPTH = 0;
   private static final int TIME = 1;
   private static final int DEPTH = 2;
   private static final int VERTICAL_SECTION = 3;
   private int type;

   public CurvesService(UIRunnable uiRunnable) {
      super(CurvesParser.getInstance(), uiRunnable);
   }

   public void collectCurves(String wellboreId)
   {
      collectCurves(wellboreId, -1);
   }

   public void collectCurves(String wellboreId, String type) {
      try {
         if(wellboreId == null) {
            execute(END_POINT_URI);
         }
         else {
            execute(END_POINT_URI + wellboreId);
         }

         this.type = getTypeValue(type);
      } catch (Exception e) {
         Log.e(TAG, "Failed to fetch Curves\n" + e.getMessage());
      }
   }

   public void collectCurves(String wellboreId, int type) {
      try {
         if(wellboreId == null) {
            execute(END_POINT_URI);
         }
         else {
            execute(END_POINT_URI + wellboreId);
         }

         this.type = type;
      } catch (Exception e) {
         Log.e(TAG, "Failed to fetch Curves\n" + e.getMessage());
      }
   }

   @Override
   protected void onPostExecute(String json) {
      if (json == null) {
         Log.e(TAG, "Failed to connect to server");
      }
      else {
         try {
            List<Curve> curves = parser.parse(json);
            List<Curve> updatedCurves = new ArrayList<>();

            if(type == -1)
            {
               updatedCurves.addAll(curves);
            }
            else {
               for (Curve curve : curves) {
                  if (curve.getIvt() == type) {
                     updatedCurves.add(curve);
                  }
               }
            }

            Collections.sort(updatedCurves, new Comparator<Curve>() {
               @Override
               public int compare(Curve lhs, Curve rhs) {
                  return lhs.getName().compareTo(rhs.getName());
               }
            });

              uiRunnable.run(updatedCurves);
         } catch (Exception e) {
            Log.e(TAG, "Failed to handle data retrieved from server\n" + Log.getStackTraceString(e));
         }
      }
   }

   private int getTypeValue(String type)
   {
      if(type.equals("Depth"))
      {
         return DEPTH;
      }
      else if(type.equals("True Vertical Depth"))
      {
         return TRUE_VERTICAL_DEPTH;
      }
      else if(type.equals("Time"))
      {
         return TIME;
      }
      else
      {
         return VERTICAL_SECTION;
      }
   }
}
