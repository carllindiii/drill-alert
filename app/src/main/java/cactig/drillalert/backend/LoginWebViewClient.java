package cactig.drillalert.backend;

import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import cactig.drillalert.backend.model.User;
import cactig.drillalert.LoginActivity;

/**
 * Singleton web client to track requests to the server from the login web view
 */
public class LoginWebViewClient extends WebViewClient {
   private static final String TAG ="LoginWebViewClient";
   private static final String urlString = "https://drillalert.azurewebsites.net/";

   private static final LoginWebViewClient instance = new LoginWebViewClient();
   private LoginActivity loginActivity;

   private LoginWebViewClient() {
      super();
   }

   public static LoginWebViewClient getInstance() {
      instance.loginActivity = null;
      return instance;
   }

   public static LoginWebViewClient getInstance(LoginActivity activity) {
      instance.loginActivity = activity;
      return instance;
   }

   /**
    * Get the session cookie for this login
    * @return the entire session cookie string
    */
   public String getSessionCookie() {
      CookieManager cookieManager = CookieManager.getInstance();
      return cookieManager.getCookie(urlString);
   }

   /**
    * Logout of the application
    *
    * This removes the authenticated login session cookies
    */
   public void logout() {
      CookieManager cookieManager = CookieManager.getInstance();
      cookieManager.removeAllCookie();
   }

   /**
    * Need to override this method to prevent the default browser opening on redirect.
    */
   @Override
   public boolean shouldOverrideUrlLoading(WebView view, String url) {
      super.shouldOverrideUrlLoading(view, url);
      return false;
   }

   /**
    * After the page has finished loading there will be two options
    *
    * 1) The user has sent a valid username/password to start the next activity. We will isValidDouble that
    *    the proper cookies are instantiated here, FedAuth and FedAuth1.
    *
    * 2) The user has not been authorized, so we shall load the login screen.
    *
    * Note: Any invalid username/password will not result in this method being called. Only performing
    *       javascript on the page.
    *
    * @param view login web view
    * @param url url being posted
    */
   @Override
   public void onPageFinished(WebView view, String url) {
      String cookieHeader = getSessionCookie();

      if(isUserAuthorized(cookieHeader)) {
         // don't allow the web view to go to the next page (the admin panel)
         view.stopLoading();
         view.loadUrl("about:blank");

         Log.d(TAG, "user is authorized!");

         if(loginActivity != null) {
            // load the next activity
            loginActivity.login();
            view.destroy();
         }
      }
      else {
//         view.loadUrl(insertTestAccount());
      }
   }

   /**
    * If the user cookies contain a "FedAuth" or "FedAuth1" cookie, then the user is authorized.
    * @param cookieHeader cookies stored in the header
    * @return true if user is authorized, false if user is not authorized
    */
   private boolean isUserAuthorized(String cookieHeader) {
      return cookieHeader.contains("FedAuth") || cookieHeader.contains("FedAuth1");
   }

   /**
    * Javascript to load test user account onto the webview page
    * @return javascript function as a string
    */
   private String insertTestAccount() {
      // load test user onto page
      String testUsername = "CAPSTONE2015" + "\\\\" + "testuser";

      // for testing purposes, load test username
      String testUserJavascript = "javascript:(function () {" +
              "document.getElementById('userNameInput').value = " + "'" + testUsername + "'" + ";" +
              "})()";

      return testUserJavascript;
   }
}
