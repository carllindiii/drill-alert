package resources.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import resources.CheckboxElementListener;

/**
 * Created by cjohnson on 3/4/15.
 */
public class CheckboxElementListAdapter<T> extends ArrayAdapter<T>
{
   private int layoutResourceId;
   private List<T> data;
   private SparseBooleanArray mSelectedItemsIds;
   private ArrayList<CheckboxElementListener> listeners;

   public CheckboxElementListAdapter(Context context, int layoutResourceId, List<T> data) {
      super(context, layoutResourceId, data);
      super.notifyDataSetChanged();

      listeners = new ArrayList<>();

      mSelectedItemsIds = new SparseBooleanArray();

      this.layoutResourceId = layoutResourceId;

      this.data = data;
   }

   public void remove(int index)
   {
      data.remove(index);
      notifyDataSetChanged();
   }

   @Override
   public void remove(T t) {
      data.remove(t);
      notifyDataSetChanged();
   }

   public void removeSelection() {
      mSelectedItemsIds = new SparseBooleanArray();
      notifyDataSetChanged();

      for(CheckboxElementListener checkboxElementListener : listeners)
      {
         checkboxElementListener.fireElementsRemoved();
      }
   }

   public void toggleSelection(int position) {
      selectView(position, !mSelectedItemsIds.get(position));
   }

   public void selectView(int position, boolean value) {
      if (value)
         mSelectedItemsIds.put(position, value);
      else
         mSelectedItemsIds.delete(position);
      notifyDataSetChanged();
   }

   public int getSelectedCount() {
      return mSelectedItemsIds.size();
   }

   public SparseBooleanArray getSelectedIds() {
      return mSelectedItemsIds;
   }

   public List<T> getAllData() {
      return data;
   }

   public int getLayoutResourceId() {
      return layoutResourceId;
   }

   public boolean isSelected(int index)
   {
      Boolean selected = mSelectedItemsIds.get(index);

      return selected != null && selected;
   }

   public void add(T t)
   {
      if(t != null)
      {
         data.add(t);
      }

      notifyDataSetChanged();
   }

   public void set(int index, T t)
   {
      if(t != null)
      {
         data.set(index, t);
      }

      notifyDataSetChanged();
   }

   public void addCheckboxElementListener(CheckboxElementListener checkboxElementListener)
   {
      if(checkboxElementListener != null)
      {
         listeners.add(checkboxElementListener);
      }
   }

   public void removeCheckboxElementListener(CheckboxElementListener checkboxElementListener)
   {
      listeners.remove(checkboxElementListener);
   }

   public void setData(List<T> newData)
   {
      while(!data.isEmpty())
      {
         this.data.remove(0);
      }

      data.addAll(newData);

      notifyDataSetChanged();
   }
}
