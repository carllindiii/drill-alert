package resources.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.R;
import resources.CheckboxListItem;

/**
 * Created by cjohnson on 1/31/15.
 */
public class CheckboxArrayAdapter<T> extends CheckboxElementListAdapter<CheckboxListItem<T>> {
   protected List<CheckboxListItem<T>> checkboxListItems;

   public CheckboxArrayAdapter(Context context, int layoutId,
                               List<CheckboxListItem<T>> checkboxListItems) {
      super(context, layoutId, checkboxListItems);

      this.checkboxListItems = checkboxListItems;
   }

   @Override
   public View getView(final int position, View view, ViewGroup parent) {
      ViewHolder holder = null;

      Log.v("ConvertView", String.valueOf(position));

      if (view == null) {
         LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                 Context.LAYOUT_INFLATER_SERVICE);
         view = vi.inflate(R.layout.checkbox_list_item, null);

         holder = new ViewHolder();
         holder.text = (TextView) view.findViewById(R.id.checkboxText);
         holder.checkbox = (CheckBox) view.findViewById(R.id.checkBox1);
         view.setTag(holder);

         holder.checkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               CheckBox cb = (CheckBox) v;

               CheckboxListItem checkboxListItem = getAllData().get(position);
               checkboxListItem.setChecked(cb.isChecked());
            }
         });
      } else {
         holder = (ViewHolder) view.getTag();
      }


      CheckboxListItem checkboxListItem = getAllData().get(position);
      checkboxListItem.setChecked(checkboxListItem.isSelected());
      holder.checkbox.setText(checkboxListItem.getText());
      holder.checkbox.setChecked(checkboxListItem.isSelected());

      return view;
   }

   public List<T> getAllItems()
   {
      List<T> items = new ArrayList<>();

      for(CheckboxListItem<T> checkboxListItem : checkboxListItems)
      {
         items.add(checkboxListItem.getItem());
      }

      return items;
   }

   private class ViewHolder {
      TextView text;
      CheckBox checkbox;
   }
}