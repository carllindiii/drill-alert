package resources.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cactig.drillalert.R;
import cactig.drillalert.backend.model.dashboard.Item;

/**
 * Created by cjohnson on 3/4/15.
 */
public class CanvasListAdapter extends CheckboxElementListAdapter<Item> {
   private List<Item> data;

   public CanvasListAdapter(Context context, int layoutResourceId, List<Item> data) {
      super(context, layoutResourceId, data);

      this.data = data;
   }

   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
      View row = convertView;
      StringHolder holder = null;

      if (row == null) {
         LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
         row = inflater.inflate(getLayoutResourceId(), parent, false);

         holder = new StringHolder();
         holder.txtTitle = (TextView) row.findViewById(R.id.sortable_row_text);

         row.setTag(holder);
      } else {
         holder = (StringHolder) row.getTag();
      }


      holder.txtTitle.setText(getAllData().get(position).toString());
      if (isSelected(position)) {
         TypedValue typedValue = new TypedValue();
         getContext().getTheme().resolveAttribute(android.R.attr.activatedBackgroundIndicator, typedValue, true);
         row.setBackgroundResource(typedValue.resourceId);
      }
      else if (data != null && data.get(position).getJsFile() != null && data.get(position).getJsFile().equalsIgnoreCase("Gauge")) {
         row.setBackgroundResource(R.drawable.grablines_light_blue);
      }
      else {
         row.setBackgroundResource(R.drawable.grablines_dark_blue);
      }

      return row;
   }

   private static class StringHolder
   {
      TextView txtTitle;
   }
}
