package resources.adapter.utility;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.model.dashboard.Dashboard;
import cactig.drillalert.backend.model.dashboard.Page;
import cactig.drillalert.backend.service.DashboardService;
import cactig.drillalert.backend.service.UIRunnable;

/**
 * Created by cjohnson on 4/22/15.
 */
public class AdapterFactory {
   public static ArrayAdapter<Page> getTemplatePanelAdapter(final int filter, String wellboreId, Context context)
   {
      final ArrayList<Page> existingPages = new ArrayList<>();
      final ArrayAdapter arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, existingPages);

      new DashboardService(new UIRunnable<Dashboard>() {
         @Override
         public void run(List<Dashboard> data) {
            for(Dashboard dashboard : data) {
               for (Page page : dashboard.getPages()) {
                  if (page.getType().equals(filter)) {
                     existingPages.add(page);
                  }
               }
            }

            arrayAdapter.notifyDataSetChanged();
         }
      }).collectDashboards(wellboreId);

      return arrayAdapter;
   }
}
