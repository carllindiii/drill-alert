package resources.adapter.utility;

import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cactig.drillalert.backend.service.UIRunnable;
import cactig.drillalert.R;
import resources.CheckboxListItem;
import resources.adapter.CheckboxElementListAdapter;

/**
 * Created by cjohnson on 3/4/15.
 */
public class CheckboxPopupUtility {
   public static void prepareRemoveListView(final ListView listView, final CheckboxElementListAdapter dataAdapter) {
      listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
      // Capture ListView item click
      listView.setMultiChoiceModeListener(new ListView.MultiChoiceModeListener() {
         @Override
         public void onItemCheckedStateChanged(ActionMode mode,
                                               int position, long id, boolean checked) {
            // Capture total checked items
            final int checkedCount = listView.getCheckedItemCount();
            // Set the CAB menu_delete according to total checked items
            mode.setTitle(checkedCount + " Selected");
            // Calls toggleSelection method from ListViewAdapter Class
            dataAdapter.toggleSelection(position);
         }


         @Override
         public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
               case R.id.delete:
                  // Calls getSelectedIds method from ListViewAdapter Class
                  SparseBooleanArray selected = dataAdapter.getSelectedIds();
                  // Captures all selected ids with a loop
                  for (int selectedIndex = (dataAdapter.getAllData().size() - 1); selectedIndex >= 0; selectedIndex--) {
                     if (selected.get(selectedIndex)) {
                        // Remove selected items following the ids
                        dataAdapter.remove(selectedIndex);
                     }
                  }
                  // Close CAB
                  mode.finish();
                  return true;
               default:
                  return false;
            }
         }

         @Override
         public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_delete, menu);

            return true;
         }

         @Override
         public void onDestroyActionMode(ActionMode mode) {
            dataAdapter.removeSelection();
         }

         @Override
         public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
         }
      });
   }

   public static void prepareSelectionListView(final ListView listView, final CheckboxElementListAdapter dataAdapter, final UIRunnable uiRunnable) {
      listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
      // Capture ListView item click
      listView.setMultiChoiceModeListener(new ListView.MultiChoiceModeListener() {
         @Override
         public void onItemCheckedStateChanged(ActionMode mode,
                                               int position, long id, boolean checked) {
            // Capture total checked items
            final int checkedCount = listView.getCheckedItemCount();
            // Set the CAB menu_delete according to total checked items
            mode.setTitle(checkedCount + " Selected");
            // Calls toggleSelection method from ListViewAdapter Class
            dataAdapter.toggleSelection(position);
         }

         @Override
         public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
               case R.id.select:
                  uiRunnable.run(getCheckedItems(dataAdapter));
                  // Close CAB
                  mode.finish();
                  return true;
               default:
                  return false;
            }
         }

         @Override
         public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.select, menu);

            return true;
         }

         @Override
         public void onDestroyActionMode(ActionMode mode) {
            dataAdapter.removeSelection();
         }

         @Override
         public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
         }
      });
   }

   private static List<CheckboxListItem> getCheckedItems(CheckboxElementListAdapter<CheckboxListItem> checkboxAdapter)
   {
      List<CheckboxListItem> checkedItems = new ArrayList<>();

      for(CheckboxListItem checkboxListItem : checkboxAdapter.getAllData())
      {
         if(checkboxListItem.isChecked()) {
            checkedItems.add(checkboxListItem);
         }
      }

      return checkedItems;
   }
}
