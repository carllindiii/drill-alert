package resources.adapter.utility;

/**
 * Created by cjohnson on 3/30/15.
 */
public abstract class IntentKeyManager {
   private static String ALERT = "ALERT";
   private static String CREATE_FROM_EXISTING = "CREATE_FROM_EXISTING";
   private static String VISUAL = "VISUAL";
   private static String VIEW = "VIEW";
   private static String WELL_BOREID = "WELLBORE_ID";
   private static String ALL_VIEW = "VIEW";
   private static String PANEL = "PANEL";
   private static String PANELS = "PANELS";
   private static String CURVES = "CURVES";
   private static String IVT_KEY = "IVT_KEY";
   private static String ALL_CURVES = "ALL_CURVES";

   public static String getAlertKey()
   {
      return ALERT;
   }

   public static String getUseAsTemplateKey()
   {
      return CREATE_FROM_EXISTING;
   }

   public static String getSelectedVisualKey()
   {
      return VISUAL;
   }

   public static String getDashboardsKey()
   {
      return VIEW;
   }

   public static String getWellboreIdKey()
   {
      return WELL_BOREID;
   }

   public static String getAllViewKey()
   {
      return ALL_VIEW;
   }

   public static String getPanelKey()
   {
      return PANEL;
   }

   public static String getPanelsKey()
   {
      return PANELS;
   }

   public static String getSelectedCurvesKey()
   {
      return CURVES;
   }

   public static String getAllCurvesKey()
   {
      return ALL_CURVES;
   }

   public static String getIVTKey()
   {
      return IVT_KEY;
   }
}
