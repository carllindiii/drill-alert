package resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by cjohnson on 4/19/15.
 */
public class NameGenerator implements Serializable{
   private static final NameGenerator instance = new NameGenerator();

   private Set<String> usedNames = new HashSet<>();

   private NameGenerator()
   {
      //Needed for singleton
   }

   public static NameGenerator getInstance()
   {
      return instance;
   }

   public String generateName(String seed)
   {
      int index = 0;

      while(isNameInUse(seed + " " + index))
      {
         index++;
      }

      addName(seed + " " + index);

      return seed + " " + index;
   }

   public boolean isNameInUse(String name)
   {
      return usedNames.contains(name);
   }

   public void addName(String name)
   {
      usedNames.add(name);
   }

   //TODO: When the user deletes an item, free up any names under it
   public void removeName(String name)
   {
      usedNames.remove(name);
   }
}
