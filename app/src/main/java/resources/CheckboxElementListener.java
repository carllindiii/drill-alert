package resources;

/**
 * Created by cjohnson on 4/22/15.
 */
public interface CheckboxElementListener {
   public void fireElementsRemoved();
}
