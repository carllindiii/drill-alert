package resources;

import android.util.Log;

/**
 * Created by cjohnson on 1/31/15.
 */
public class CheckboxListItem<T> {

   private boolean selected = false;
   private boolean checked = false;
   private T item;

   private static final String TAG = "CheckboxListItem";

   public CheckboxListItem(T item, boolean selected, boolean checked) {
      super();
      this.item = item;
      this.selected = selected;
      this.checked = checked;
   }

   public String getText() {
      if(item == null)
      {
         Log.d(TAG, "Unable to get text for item");
         return "";
      }
      else
      {
         return item.toString();
      }
   }

   public boolean isSelected() {
      return selected;
   }

   public void setSelected(boolean selected) {
      this.selected = selected;
   }

   public boolean isChecked() {
      return checked;
   }

   public void setChecked(boolean checked) {
      this.checked = checked;
   }

   public T getItem() {
      return item;
   }

   public void setItem(T item)
   {
      this.item = item;
   }
}
